<?php
include('../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'user') {
	header("location:../../login-user");
	exit;
}
$id = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/cetak-user.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../assets/image/logo.jpeg">
	<title>Cetak Catatan Keluarga-User</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../">Home</a>
		<a href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a href="../penduduk/data-warga/">Input Data Warga</a>
			<a href="../penduduk/data-keluarga/">Input Data Keluarga</a>
			<a href="../penduduk/catatan-keluarga/">Input Catatan Keluarga</a>
		</div>
		<a class="active" href="./">Cetak</a>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../">Home</a>
				<a href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a href="../penduduk/data-warga/">Input Data Warga</a>
					<a href="../penduduk/data-keluarga/">Input Data Keluarga</a>
					<a href="../penduduk/catatan-keluarga/">Input Catatan Keluarga</a>
				</div>
				<a class="active" href="./">Cetak</a>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul text-center">
			<p>REKAPITULASI<br>CATATAN KELUARGA</p>
		</div>

		<div class="form mb-4">
			<div class="card">
				<!-- <div class="card-header">
					Featured
				</div> -->
				<?php
				$sqlCheckData = "SELECT * FROM tbl_catatan_keluarga WHERE idCatatanKeluarga='$id'";
				$queryCheckData = mysqli_query($db, $sqlCheckData);

				if (mysqli_num_rows($queryCheckData) < 1) {
					?>
					<div class="card-body">
						<div class="tabel d-flex flex-sm-row flex-column">
							<table border="0" width="40%">
								<?php
								$sqlDataWarga = "SELECT * FROM tbl_data_Warga WHERE idUser = '$id'";
								$queryDataWarga = mysqli_query($db, $sqlDataWarga);
								$dataWarga = mysqli_fetch_array($queryDataWarga);
								$sqlDataKeluarga = "SELECT * FROM tbl_data_keluarga WHERE idKeluarga = '$id'";
								$queryDataKeluarga = mysqli_query($db, $sqlDataKeluarga);
								$dataKeluarga = mysqli_fetch_array($queryDataKeluarga);
								?>
								<form action="confCatatanKeluarga.php" method="post">

									<tr>
										<td width="60%">Catatan Keluarga Dari</td>
										<td>:</td>
										<td><?php echo $dataWarga['kepalaRumahTangga'] ?></td>
										<input type="text" name="kepalaRumahTangga" value="<?php echo $dataWarga['kepalaRumahTangga'] ?>" hidden>
									</tr>
									<tr>
										<td width="60%">Anggota Kelompok Dasawisma</td>
										<td>:</td>
										<td><?php echo $dataWarga['dasaWisma'] ?></td>
										<input type="text" name="dasaWisma" value="<?php echo $dataWarga['dasaWisma'] ?>" hidden>
									</tr>
									<tr>
										<td width="60%">Tahun</td>
										<td>:</td>
										<td><?php echo date('Y') ?></td>
										<input type="text" name="tahun" value="<?php echo date('Y') ?>" hidden>
									</tr>
								</table>

								<table border="0" width="40%">
									<tr>
										<td width="50%">Kriteria Rumah</td>
										<td>:</td>
										<td><?php echo $dataKeluarga['kriteriaRumah'] ?></td>
										<input type="text" name="kriteriaRumah" value="<?php echo $dataKeluarga['kriteriaRumah'] ?>" hidden>
									</tr>
									<tr>
										<td width="50%">Jamban Keluarga</td>
										<td>:</td>
										<td><?php echo $dataKeluarga['jambanKeluarga'] ?></td>
										<input type="text" name="jambanKeluarga" value="<?php echo $dataKeluarga['jambanKeluarga'] ?>" hidden>
									</tr>
									<tr>
										<td width="50%">Tempat Sampah</td>
										<td>:</td>
										<td><?php echo $dataKeluarga['memilikiTPS'] ?></td>
										<input type="text" name="memilikiTPS" value="<?php echo $dataKeluarga['memilikiTPS'] ?>" hidden>
									</tr>
								</table>
							</div>

							<div class="tabel table-responsive mb-4 mt-4">
								<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
									<thead class="table-warning">
										<tr>
											<th scope="col" class="text-center" rowspan="2">NO</th>
											<th scope="col" class="text-center" rowspan="2">NAMA ANGGOTA KELUARGA</th>
											<th scope="col" class="text-center" rowspan="2">STATUS PERKAWINAN</th>
											<th scope="col" class="text-center" rowspan="2">L/P</th>
											<th scope="col" class="text-center" rowspan="2">TEMPAT LAHIR</th>
											<th scope="col" class="text-center" rowspan="2">TGL/BL/TH LAHIR/UMUR</th>
											<th scope="col" class="text-center" rowspan="2">AGAMA</th>
											<th scope="col" class="text-center" rowspan="2">PENDIDIKAN</th>
											<th scope="col" class="text-center" rowspan="2">PEKERJAAN</th>
											<th scope="col" class="text-center" rowspan="2">BERKEBUTUHAN KHUSUS</th>
											<th scope="col" class="text-center" colspan="8">KEGIATAN YANG DIIKUTI</th>
											<th scope="col" class="text-center" rowspan="2">KET</th>
										</tr>
										<tr>
											<th>PENGHAYATAN DAN PENGAMALAN PANCASILA</th>
											<th>GOTONG ROYONG</th>
											<th>PENDIDIKAN DAN KETRAMPILAN</th>
											<th>PENGEMBANGAN KEHIDUPAN BERKOPERASI</th>
											<th>PANGAN</th>
											<th>SANDANG</th>
											<th>KESEHATAN</th>
											<th>PERENCANAAN SEHAT</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>2</td>
											<td>3</td>
											<td>4</td>
											<td>5</td>
											<td>6</td>
											<td>7</td>
											<td>8</td>
											<td>9</td>
											<td>10</td>
											<td>11</td>
											<td>12</td>
											<td>13</td>
											<td>14</td>
											<td>15</td>
											<td>16</td>
											<td>17</td>
											<td>18</td>
											<td>19</td>
										</tr>
										<?php
										$sqlListCatatan = "SELECT * FROM tbl_list_catatan_keluarga WHERE idCatatanKeluarga = '$id'";
										$queryListCatatan = mysqli_query($db, $sqlListCatatan);
										$no = 1;
										if (mysqli_num_rows($queryListCatatan) < 1) {
											?>
											<tr>
												<td colspan="19" class="text-center">TIDAK ADA CATATAN KELUARGA</td>
											</tr>
											<?php
										} else {
											while ($dataList = mysqli_fetch_array($queryListCatatan)) {
												?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $dataList['anggotaKeluarga'] ?></td>
													<td><?php echo $dataList['statusKawinAnggota'] ?></td>
													<td><?php echo $dataList['jenisKelaminAnggota'] ?></td>
													<td><?php echo $dataList['tempatLahirAnggota'] ?></td>
													<td><?php echo $dataList['tanggalLahirAnggota'] ?></td>
													<td><?php echo $dataList['agamaAnggota'] ?></td>
													<td><?php echo $dataList['pendidikanAnggota'] ?></td>
													<td><?php echo $dataList['pekerjaanAnggota'] ?></td>
													<td><?php echo $dataList['berkebutuhanKhususAnggota'] ?></td>
													<td><?php echo $dataList['pengamalanPancasila'] ?></td>
													<td><?php echo $dataList['gotongRoyong'] ?></td>
													<td><?php echo $dataList['pendidikanKeterampilan'] ?></td>
													<td><?php echo $dataList['kehidupanBerkoperasi'] ?></td>
													<td><?php echo $dataList['pangan'] ?></td>
													<td><?php echo $dataList['sandang'] ?></td>
													<td><?php echo $dataList['kesehatan'] ?></td>
													<td><?php echo $dataList['perencanaanKesehatan'] ?></td>
													<td><?php echo $dataList['keterangan'] ?></td>
												</tr>
												<?php
											}
										}
										?>
									</tbody>
								</table>
							</div>

							<div class="tombol text-end mt-2">
								<!-- <button class="btn btn-danger" type="submit" role="button" name="submitAll" value="submitAll">Print</button> -->
								<a class="btn btn-danger" href="view/" role="button">Print</a>
							</div>
						</form>

					</div>
					<?php
				}else{
					?>
					<div class="card-body">
						<div class="tabel d-flex flex-sm-row flex-column">
							<table border="0" width="40%">
								<?php
								$sqlDataWarga = "SELECT * FROM tbl_data_Warga WHERE idUser = '$id'";
								$queryDataWarga = mysqli_query($db, $sqlDataWarga);
								$dataWarga = mysqli_fetch_array($queryDataWarga);
								$sqlDataKeluarga = "SELECT * FROM tbl_data_keluarga WHERE idKeluarga = '$id'";
								$queryDataKeluarga = mysqli_query($db, $sqlDataKeluarga);
								$dataKeluarga = mysqli_fetch_array($queryDataKeluarga);
								?>
								<form action="confCatatanKeluarga.php" method="post">

									<tr>
										<td width="60%">Catatan Keluarga Dari</td>
										<td>:</td>
										<td><?php echo $dataWarga['kepalaRumahTangga'] ?></td>
										<input type="text" name="kepalaRumahTangga" value="<?php echo $dataWarga['kepalaRumahTangga'] ?>" hidden>
									</tr>
									<tr>
										<td width="60%">Anggota Kelompok Dasawisma</td>
										<td>:</td>
										<td><?php echo $dataWarga['dasaWisma'] ?></td>
										<input type="text" name="dasaWisma" value="<?php echo $dataWarga['dasaWisma'] ?>" hidden>
									</tr>
									<tr>
										<td width="60%">Tahun</td>
										<td>:</td>
										<td><?php echo date('Y') ?></td>
										<input type="text" name="tahun" value="<?php echo date('Y') ?>" hidden>
									</tr>
								</table>

								<table border="0" width="40%">
									<tr>
										<td width="50%">Kriteria Rumah</td>
										<td>:</td>
										<td><?php echo $dataKeluarga['kriteriaRumah'] ?></td>
										<input type="text" name="kriteriaRumah" value="<?php echo $dataKeluarga['kriteriaRumah'] ?>" hidden>
									</tr>
									<tr>
										<td width="50%">Jamban Keluarga</td>
										<td>:</td>
										<td><?php echo $dataKeluarga['jambanKeluarga'] ?></td>
										<input type="text" name="jambanKeluarga" value="<?php echo $dataKeluarga['jambanKeluarga'] ?>" hidden>
									</tr>
									<tr>
										<td width="50%">Tempat Sampah</td>
										<td>:</td>
										<td><?php echo $dataKeluarga['memilikiTPS'] ?></td>
										<input type="text" name="memilikiTPS" value="<?php echo $dataKeluarga['memilikiTPS'] ?>" hidden>
									</tr>
								</table>
							</div>

							<div class="tabel table-responsive mb-4 mt-4">
								<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
									<thead class="table-warning">
										<tr>
											<th scope="col" class="text-center" rowspan="2">NO</th>
											<th scope="col" class="text-center" rowspan="2">NAMA ANGGOTA KELUARGA</th>
											<th scope="col" class="text-center" rowspan="2">STATUS PERKAWINAN</th>
											<th scope="col" class="text-center" rowspan="2">L/P</th>
											<th scope="col" class="text-center" rowspan="2">TEMPAT LAHIR</th>
											<th scope="col" class="text-center" rowspan="2">TGL/BL/TH LAHIR/UMUR</th>
											<th scope="col" class="text-center" rowspan="2">AGAMA</th>
											<th scope="col" class="text-center" rowspan="2">PENDIDIKAN</th>
											<th scope="col" class="text-center" rowspan="2">PEKERJAAN</th>
											<th scope="col" class="text-center" rowspan="2">BERKEBUTUHAN KHUSUS</th>
											<th scope="col" class="text-center" colspan="8">KEGIATAN YANG DIIKUTI</th>
											<th scope="col" class="text-center" rowspan="2">KET</th>
										</tr>
										<tr>
											<th>PENGHAYATAN DAN PENGAMALAN PANCASILA</th>
											<th>GOTONG ROYONG</th>
											<th>PENDIDIKAN DAN KETRAMPILAN</th>
											<th>PENGEMBANGAN KEHIDUPAN BERKOPERASI</th>
											<th>PANGAN</th>
											<th>SANDANG</th>
											<th>KESEHATAN</th>
											<th>PERENCANAAN SEHAT</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>2</td>
											<td>3</td>
											<td>4</td>
											<td>5</td>
											<td>6</td>
											<td>7</td>
											<td>8</td>
											<td>9</td>
											<td>10</td>
											<td>11</td>
											<td>12</td>
											<td>13</td>
											<td>14</td>
											<td>15</td>
											<td>16</td>
											<td>17</td>
											<td>18</td>
											<td>19</td>
										</tr>
										<?php
										$sqlListCatatan = "SELECT * FROM tbl_list_catatan_keluarga WHERE idCatatanKeluarga = '$id'";
										$queryListCatatan = mysqli_query($db, $sqlListCatatan);
										$no = 1;
										if (mysqli_num_rows($queryListCatatan) < 1) {
											?>
											<tr>
												<td colspan="19" class="text-center">TIDAK ADA CATATAN KELUARGA</td>
											</tr>
											<?php
										} else {
											while ($dataList = mysqli_fetch_array($queryListCatatan)) {
												?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $dataList['anggotaKeluarga'] ?></td>
													<td><?php echo $dataList['statusKawinAnggota'] ?></td>
													<td><?php echo $dataList['jenisKelaminAnggota'] ?></td>
													<td><?php echo $dataList['tempatLahirAnggota'] ?></td>
													<td><?php echo $dataList['tanggalLahirAnggota'] ?></td>
													<td><?php echo $dataList['agamaAnggota'] ?></td>
													<td><?php echo $dataList['pendidikanAnggota'] ?></td>
													<td><?php echo $dataList['pekerjaanAnggota'] ?></td>
													<td><?php echo $dataList['berkebutuhanKhususAnggota'] ?></td>
													<td><?php echo $dataList['pengamalanPancasila'] ?></td>
													<td><?php echo $dataList['gotongRoyong'] ?></td>
													<td><?php echo $dataList['pendidikanKeterampilan'] ?></td>
													<td><?php echo $dataList['kehidupanBerkoperasi'] ?></td>
													<td><?php echo $dataList['pangan'] ?></td>
													<td><?php echo $dataList['sandang'] ?></td>
													<td><?php echo $dataList['kesehatan'] ?></td>
													<td><?php echo $dataList['perencanaanKesehatan'] ?></td>
													<td><?php echo $dataList['keterangan'] ?></td>
												</tr>
												<?php
											}
										}
										?>
									</tbody>
								</table>
							</div>

							<div class="tombol text-end mt-2">
								<!-- <button class="btn btn-danger" type="submit" role="button" name="update" value="update">Print</button> -->
								<a class="btn btn-danger" href="view/" target="_BLANK" role="button">Print</a>
							</div>
						</form>
					</div>
					<?php
				}
				?>

			</div>
		</div>

	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	// $(document).ready(function(){
	// 	$('[data-bs-toggle="popover"]').popover();   
	// });
	// const popover = new bootstrap.Popover('.popover-dismiss', {
	// 	trigger: 'focus'
	// })
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

</html>