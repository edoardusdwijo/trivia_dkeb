<?php
include("../../../config.php");
session_start();

if(isset($_POST['saveAnggota'])){
    $idUser = $_SESSION['id'];
    $idKeluarga = $idUser;
    $noRegAnggota = $_POST['noRegAnggota'];
    $namaAnggota = $_POST['namaAnggota'];
    $statusKeluargaAnggota = $_POST['statusKeluargaAnggota'];
    $statusPerkawinanAnggota = $_POST['statusPerkawinanAnggota'];
    $jenisKelaminAnggota = $_POST['jenisKelaminAnggota'];
    $tglLahirAnggota = $_POST['tglLahirAnggota'];
    $pendidikanAnggota = $_POST['pendidikanAnggota'];
    $pekerjaanAnggota = $_POST['pekerjaanAnggota'];

    $sql = "INSERT INTO tbl_anggota_kel (idKeluarga, idUser, noRegAnggota, namaAnggota, statusKeluargaAnggota, 
                                        statusPerkawinanAnggota, jenisKelaminAnggota, tglLahirAnggota, 
                                        pendidikanAnggota, pekerjaanAnggota)
                                        VALUE ('$idKeluarga', '$idUser', '$noRegAnggota', '$namaAnggota', '$statusKeluargaAnggota',
                                        '$statusPerkawinanAnggota', '$jenisKelaminAnggota', '$tglLahirAnggota', 
                                        '$pendidikanAnggota', '$pekerjaanAnggota')";
    $query = mysqli_query($db, $sql);

    if($query){
        echo "
            <script>
                alert('DATA BERHASIL DI SIMPAN');
                document.location.href = '../data-keluarga';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI SIMPAN');
                document.location.href = '../data-keluarga';
            </script>
        ";
    }
}

if(isset($_POST['save'])){
    $idUser = $_SESSION['id'];
    $idKeluarga = $idUser;
    $dasaWisma = $_POST['dasaWisma'];
    $rt = $_POST['rt'];
    $rw = $_POST['rw'];
    $dusun = $_POST['dusun'];
    $desa = $_POST['desa'];
    $kec = $_POST['kec'];
    $kab = $_POST['kab'];
    $prov = $_POST['prov'];
    $namaKepalaK = $_POST['namaKepalaK'];
    $jmlhAnggota = $_POST['jmlhAnggota'];
    $jumlahKK = $_POST['jumlahKK'];
    $jumlahBalita = $_POST['jumlahBalita'];
    $jumlahPus = $_POST['jumlahPus'];
    $jumlahWus = $_POST['jumlahWus'];
    $jumlahButa = $_POST['jumlahButa'];
    $jumlahHamil = $_POST['jumlahHamil'];
    $jumlahMenyusui = $_POST['jumlahMenyusui'];
    $jumlahLansia = $_POST['jumlahLansia'];
    $berkebutuhanKhusus = $_POST['berkebutuhanKhusus'];
    $makananPokok = $_POST['makananPokok'];
    $jambanKeluarga = $_POST['jambanKeluarga'];
    $sumberAir = $_POST['sumberAir'];
    $memilikiTPS = $_POST['memilikiTPS'];
    $saluranLimbah = $_POST['saluranLimbah'];
    $stikerP4K = $_POST['stikerP4K'];
    $kriteriaRumah = $_POST['kriteriaRumah'];
    $up2k = $_POST['up2k'];
    $jenisUsaha = $_POST['jenisUsaha'];

    $sql = "INSERT INTO tbl_data_keluarga (idKeluarga, idUser, dasaWisma, rt, rw, dusun, desa, kec, kab, prov,
                                        namaKepalaK, jmlhAnggota, jumlahKK, jumlahBalita, jumlahPus, jumlahWus,
                                        jumlahButa, jumlahHamil, jumlahMenyusui, jumlahLansia, berkebutuhanKhusus,
                                        makananPokok, jambanKeluarga, sumberAir, memilikiTPS, saluranLimbah, stikerP4K, 
                                        kriteriaRumah, up2k, jenisUsaha) 
                                        VALUE 
                                        ('$idKeluarga', '$idUser', '$dasaWisma', '$rt', '$rw', '$dusun', '$desa', '$kec', '$kab', '$prov',
                                        '$namaKepalaK', '$jmlhAnggota', '$jumlahKK', '$jumlahBalita', '$jumlahPus', '$jumlahWus',
                                        '$jumlahButa', '$jumlahHamil', '$jumlahMenyusui', '$jumlahLansia', '$berkebutuhanKhusus',
                                        '$makananPokok', '$jambanKeluarga', '$sumberAir', '$memilikiTPS', '$saluranLimbah', '$stikerP4K', 
                                        '$kriteriaRumah', '$up2k', '$jenisUsaha')";

    $query = mysqli_query($db, $sql);

    if($query){
        echo "
            <script>
                alert('DATA BERHASIL DI UPDATE');
                document.location.href = 'selanjutnya/';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI UPDATE');
                document.location.href = '../data-keluarga';
            </script>
        ";
    }
}

if(isset($_POST['update'])){
    $idUser = $_SESSION['id'];
    $idKeluarga = $idUser;
    $dasaWisma = $_POST['dasaWisma'];
    $rt = $_POST['rt'];
    $rw = $_POST['rw'];
    $dusun = $_POST['dusun'];
    $desa = $_POST['desa'];
    $kec = $_POST['kec'];
    $kab = $_POST['kab'];
    $prov = $_POST['prov'];
    $namaKepalaK = $_POST['namaKepalaK'];
    $jmlhAnggota = $_POST['jmlhAnggota'];
    $jumlahKK = $_POST['jumlahKK'];
    $jumlahBalita = $_POST['jumlahBalita'];
    $jumlahPus = $_POST['jumlahPus'];
    $jumlahWus = $_POST['jumlahWus'];
    $jumlahButa = $_POST['jumlahButa'];
    $jumlahHamil = $_POST['jumlahHamil'];
    $jumlahMenyusui = $_POST['jumlahMenyusui'];
    $jumlahLansia = $_POST['jumlahLansia'];
    $berkebutuhanKhusus = $_POST['berkebutuhanKhusus'];
    $makananPokok = $_POST['makananPokok'];
    $jambanKeluarga = $_POST['jambanKeluarga'];
    $sumberAir = $_POST['sumberAir'];
    $memilikiTPS = $_POST['memilikiTPS'];
    $saluranLimbah = $_POST['saluranLimbah'];
    $stikerP4K = $_POST['stikerP4K'];
    $kriteriaRumah = $_POST['kriteriaRumah'];
    $up2k = $_POST['up2k'];
    $jenisUsaha = $_POST['jenisUsaha'];

    $sql = "UPDATE tbl_data_keluarga SET dasaWisma='$dasaWisma', rt='$rt', rw='$rw', dusun='$dusun', desa='$desa',
    kec='$kec', kab='$kab', prov='$prov', namaKepalaK='$namaKepalaK', jmlhAnggota='$jmlhAnggota', jumlahKK='$jumlahKK',
    jumlahBalita='$jumlahBalita', jumlahPus='$jumlahPus', jumlahWus='$jumlahWus', jumlahButa='$jumlahButa',
    jumlahHamil='$jumlahHamil', jumlahMenyusui='$jumlahMenyusui', jumlahLansia='$jumlahLansia', berkebutuhanKhusus='$berkebutuhanKhusus',
    makananPokok='$makananPokok', jambanKeluarga='$jambanKeluarga', sumberAir='$sumberAir', memilikiTPS='$memilikiTPS',
    saluranLimbah='$saluranLimbah', stikerP4K='$stikerP4K', kriteriaRumah='$kriteriaRumah', up2k='$up2k', jenisUsaha='$jenisUsaha'
    WHERE idKeluarga='$idKeluarga'";

    $query = mysqli_query($db, $sql);

    if($query){
        echo "
            <script>
                alert('DATA BERHASIL DI UPDATE');
                document.location.href = 'selanjutnya/';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI UPDATE');
                document.location.href = '../data-keluarga';
            </script>
        ";
    }
}