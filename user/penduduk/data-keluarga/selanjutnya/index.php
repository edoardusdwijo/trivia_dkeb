<?php
include('../../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'user') {
	header("location:../../../../login-user");
	exit;
}
$id = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../../css/selanjutnya-user.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../../assets/image/logo.jpeg">
	<title>Input Data Keluarga-User</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../../../">Home</a>
		<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a href="../../data-warga/">Input Data Warga</a>
			<a class="active" href="../">Input Data Keluarga</a>
			<a href="../../catatan-keluarga/">Input Catatan Keluarga</a>
		</div>
		<a href="../../../cetak/">Cetak</a>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../../../">Home</a>
				<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a href="../../data-warga/">Input Data Warga</a>
					<a class="active" href="../">Input Data Keluarga</a>
					<a href="../../catatan-keluarga/">Input Catatan Keluarga</a>
				</div>
				<a href="../../../cetak/">Cetak</a>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul text-center">
			<p>DATA KELUARGA</p>
			<p>(DI ISI OLEH KADER, SUMBER KEPALA KELUARGA/KADER)</p>
		</div>

		<div class="form mb-4">
			<div class="card">
				<div class="card-body">
					<div class="tabel d-flex flex-sm-row flex-column mb-1">
						<?php
						$sqlNamaKK = "SELECT * FROM tbl_data_keluarga WHERE idUser = '$id'";
						$queryNamaKK = mysqli_query($db, $sqlNamaKK);
						$dataNama = mysqli_fetch_array($queryNamaKK);
						?>
						<div class="pemanfaatan">
							<p class="subjudul text-center">Pemanfaatan Tanah Pekarangan (PTP) Hatinya PKK</p>
							<P class="subjudul1">Nama KRT : <?php echo $dataNama['namaKepalaK'] ?></P>

							<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary mb-4 btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">
								Tambah Data
							</button>

							<!-- Modal -->
							<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data</h1>
											<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
										</div>
										<form action="confSelanjutnya.php" method="post">
											<div class="modal-body">
												<input type="text" name="namaKRT" value="<?php echo $dataNama['namaKepalaK'] ?>" hidden>
												<input type="text" name="dasaWisma" value="<?php echo $dataNama['dasaWisma'] ?>" hidden>
												<div class="mb-3">
													<label for="keterangan" class="form-label">Keterangan</label>
													<select class="form-select" aria-label="Default select example" name="keterangan">
														<option selected disabled>Pilih Keterangan</option>
														<option value="Peternakan">Peternakan</option>
														<option value="Perikanan">Perikanan</option>
														<option value="Warung Hidup">Warung Hidup</option>
														<option value="Toga">Toga</option>
														<option value="Lumbung Hidup">Lumbung Hidup</option>
														<option value="Tanaman Keras">Tanaman Keras</option>
													</select>
												</div>
												<div class="mb-3">
													<label for="komoditi" class="form-label">Komoditi</label>
													<input type="text" class="form-control" id="komoditi" placeholder="Masukkan Komoditi" name="komoditi">
												</div>
												<div class="mb-3">
													<label for="volume" class="form-label">Volume</label>
													<input type="number" class="form-control" id="volume" placeholder="Masukkan Volume" name="volume">
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
												<button type="submit" class="btn btn-primary" name="submitPTP" value="submitPTP">Simpan</button>
											</div>
										</form>
									</div>
								</div>
							</div>

							<div class="table-responsive mb-4">
								<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
									<thead class="table-warning">
										<tr>
											<th>NO</th>
											<th>KETERANGAN</th>
											<th>KOMODITI</th>
											<th>VOLUME</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$sqlPTP = "SELECT * FROM tbl_ptp WHERE idKeluarga = '$id'";
										$queryPTP = mysqli_query($db, $sqlPTP);
										$no = 1;
										$totalVolPTP = 0;
										if (mysqli_num_rows($queryPTP) > 0) {
											while ($dataPTP = mysqli_fetch_array($queryPTP)) {
										?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $dataPTP['keterangan'] ?></td>
													<td><?php echo $dataPTP['komoditi'] ?></td>
													<td><?php echo $dataPTP['volume'] ?></td>
												</tr>
											<?php
												$totalVolPTP += $dataPTP['volume'];
												$no++;
											}
										} else {
											?>
											<tr>
												<td class="text-center" colspan="4">TIDAK ADA DATA PTP</td>
											</tr>
										<?php
										}
										?>

										<tr>
											<td></td>
											<td>Jumlah</td>
											<td></td>
											<td><?php echo $totalVolPTP ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="industri">
							<p class="subjudul text-center">Industri Rumah Tangga</p>
							<P class="subjudul2">Nama KRT: <?php echo $dataNama['namaKepalaK'] ?></P>

							<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary mb-4 btn-sm" data-bs-toggle="modal" data-bs-target="#Modal">
								Tambah Data
							</button>

							<!-- Modal -->
							<div class="modal fade" id="Modal" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h1 class="modal-title fs-5" id="ModalLabel">Tambah Data</h1>
											<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
										</div>
										<div class="modal-body">
											<form action="confSelanjutnya.php" method="post">
												<input type="text" name="namaKRT" value="<?php echo $dataNama['namaKepalaK'] ?>" hidden>
												<input type="text" name="dasaWisma" value="<?php echo $dataNama['dasaWisma'] ?>" hidden>
												<div class="mb-3">
													<label for="keterangan" class="form-label">Keterangan</label>
													<select class="form-select" aria-label="Default select example" name="keterangan">
														<option selected>Pilih Keterangan</option>
														<option value="Pangan">Pangan</option>
														<option value="Sandang">Sandang</option>
														<option value="Jasa">Jasa</option>
														<option value="Lain-lain">Lain-lain</option>
													</select>
												</div>
												<div class="mb-3">
													<label for="komoditi" class="form-label">Komoditi</label>
													<input type="text" class="form-control" id="komoditi" placeholder="Masukkan Komoditi" name="komoditi">
												</div>
												<div class="mb-3">
													<label for="volume" class="form-label">Volume</label>
													<input type="number" class="form-control" id="volume" placeholder="Masukkan Volume" name="volume">
												</div>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
											<button type="submit" class="btn btn-primary" name="submitIRT" value="submitIRT">Simpan</button>
										</div>
										</form>
									</div>
								</div>
							</div>

							<div class="table-responsive mb-4">
								<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
									<thead class="table-warning">
										<tr>
											<th>NO</th>
											<th>KETERANGAN</th>
											<th>KOMODITI</th>
											<th>VOLUME</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$sqlIRT = "SELECT * FROM tbl_irt WHERE idKeluarga = '$id'";
										$queryIRT = mysqli_query($db, $sqlIRT);
										$no = 1;
										$totalVolIRT = 0;
										if (mysqli_num_rows($queryIRT) > 0) {
											while ($dataIRT = mysqli_fetch_array($queryIRT)) {
										?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $dataIRT['keterangan'] ?></td>
													<td><?php echo $dataIRT['komoditi'] ?></td>
													<td><?php echo $dataIRT['volume'] ?></td>
												</tr>
											<?php
												$totalVolIRT += $dataIRT['volume'];
												$no++;
											}
										} else {
											?>
											<tr>
												<td class="text-center" colspan="4">TIDAK ADA DATA IRT</td>
											</tr>
										<?php
										}
										?>

										<tr>
											<td></td>
											<td>Jumlah</td>
											<td></td>
											<td><?php echo $totalVolIRT ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	// $(document).ready(function(){
	// 	$('[data-bs-toggle="popover"]').popover();   
	// });
	// const popover = new bootstrap.Popover('.popover-dismiss', {
	// 	trigger: 'focus'
	// })
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

</html>