<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'user') {
	header("location:../../../login-user");
	exit;
}
$id = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/datakeluarga-user.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../assets/image/logo.jpeg">
	<title>Input Data Keluarga-User</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../../">Home</a>
		<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a href="../data-warga/">Input Data Warga</a>
			<a class="active" href="./">Input Data Keluarga</a>
			<a href="../catatan-keluarga/">Input Catatan Keluarga</a>
		</div>
		<a href="../../cetak/">Cetak</a>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../../">Home</a>
				<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a href="../data-warga/">Input Data Warga</a>
					<a class="active" href="./">Input Data Keluarga</a>
					<a href="../catatan-keluarga/">Input Catatan Keluarga</a>
				</div>
				<a href="../../cetak/">Cetak</a>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul text-center">
			<p>DATA KELUARGA</p>
			<p>(DI ISI OLEH KADER, SUMBER KEPALA KELUARGA/KADER)</p>
		</div>

		<div class="form mb-4">
			<div class="card">
				<div class="card-body">
					<?php
					$sql = "SELECT * FROM tbl_data_keluarga WHERE idUser = $id";
					$query = mysqli_query($db, $sql);
					$data = mysqli_fetch_array($query);

					if (mysqli_num_rows($query) < 1) {
					?>
						<form action="confDataKeluarga.php" method="post">
							<div class="mb-3">
								<label for="dasawisma" class="form-label">Dasa Wisma</label>
								<input type="text" class="form-control" id="dasawisma" aria-describedby="emailHelp" placeholder="Masukkan Dasa Wisma" name="dasaWisma">
							</div>
							<div class="mb-3">
								<label for="rt" class="form-label">RT</label>
								<input type="text" class="form-control" id="rt" aria-describedby="emailHelp" placeholder="Masukkan RT" name="rt">
							</div>
							<div class="mb-3">
								<label for="rw" class="form-label">RW</label>
								<input type="text" class="form-control" id="rw" aria-describedby="emailHelp" placeholder="Masukkan RW" name="rw">
							</div>
							<div class="mb-3">
								<label for="dusun" class="form-label">Dusun/Ling</label>
								<input type="text" class="form-control" id="dusun" aria-describedby="emailHelp" placeholder="Masukkan Dusun/Ling" name="dusun">
							</div>
							<div class="mb-3">
								<label for="desa" class="form-label">Desa/Kel</label>
								<input type="text" class="form-control" id="desa" aria-describedby="emailHelp" placeholder="Masukkan Desa/Kel" name="desa">
							</div>
							<div class="mb-3">
								<label for="kec" class="form-label">Kec</label>
								<input type="text" class="form-control" id="kec" aria-describedby="emailHelp" placeholder="Masukkan kec" name="kec">
							</div>
							<div class="mb-3">
								<label for="kota" class="form-label">Kab/Kota</label>
								<input type="text" class="form-control" id="kota" aria-describedby="emailHelp" placeholder="Masukkan Kab/Kota" name="kab">
							</div>
							<div class="mb-3">
								<label for="prov" class="form-label">Prov</label>
								<input type="text" class="form-control" id="prov" aria-describedby="emailHelp" placeholder="Masukkan Prov" name="prov">
							</div>
							<div class="mb-3">
								<label for="kepalakeluarga" class="form-label">Nama Kepala Keluarga</label>
								<input type="text" class="form-control" id="kepalakeluarga" aria-describedby="emailHelp" placeholder="Masukkan Nama Kepala Keluarga" name="namaKepalaK">
							</div>
							<div class="mb-3">
								<label for="jml" class="form-label">Jumlah Anggota Keluarga</label>
								<input type="text" class="form-control" id="jml" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Anggota Keluarga" name="jmlhAnggota">
							</div>
							<div class="mb-3">
								<label for="kk" class="form-label">Jumlah KK</label>
								<input type="text" class="form-control" id="kk" aria-describedby="emailHelp" placeholder="Masukkan Jumlah KK" name="jumlahKK">
							</div>
							<div class="mb-3">
								<label for="balita" class="form-label">Jumlah Balita (Anak)</label>
								<input type="number" class="form-control" id="balita" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Balita" name="jumlahBalita">
							</div>
							<div class="mb-3">
								<label for="pus" class="form-label">Jumlah PUS (Pasang)</label>
								<input type="number" class="form-control" id="pus" aria-describedby="emailHelp" placeholder="Masukkan Jumlah PUS" name="jumlahPus">
							</div>
							<div class="mb-3">
								<label for="wus" class="form-label">Jumlah WUS (Orang)</label>
								<input type="number" class="form-control" id="wus" aria-describedby="emailHelp" placeholder="Masukkan Jumlah WUS" name="jumlahWus">
							</div>
							<div class="mb-3">
								<label for="buta" class="form-label">Jumlah Buta (Orang)</label>
								<input type="number" class="form-control" id="buta" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Buta" name="jumlahButa">
							</div>
							<div class="mb-3">
								<label for="hamil" class="form-label">Jumlah Ibu Hamil (Orang)</label>
								<input type="number" class="form-control" id="hamil" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Ibu Hamil" name="jumlahHamil">
							</div>
							<div class="mb-3">
								<label for="menyusui" class="form-label">Jumlah Ibu Menyusui (Orang)</label>
								<input type="number" class="form-control" id="menyusui" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Menyusui" name="jumlahMenyusui">
							</div>
							<div class="mb-3">
								<label for="lansia" class="form-label">Jumlah Lansia (Orang)</label>
								<input type="number" class="form-control" id="lansia" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Lansia" name="jumlahLansia">
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Berkebutuhan Khusus:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="berkebutuhanKhusus" id="fisik" value="Fisik">
									<label class="form-check-label" for="fisik">Fisik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="berkebutuhanKhusus" id="non-fisik" value="Non Fisik">
									<label class="form-check-label" for="non-fisik">Non Fisik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="berkebutuhanKhusus" id="tidakBerkebutuhan" value="Tidak Berkebutuhan Khusus">
									<label class="form-check-label" for="tidakBerkebutuhan">Tidak Berkebutuhan Khusus</label>
								</div>
							</div>

							<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
								Tambah Data
							</button>

							<div class="tabel table-responsive mb-4 mt-4">
								<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
									<thead class="table-warning">
										<tr>
											<th>NO</th>
											<th>NO. REG</th>
											<th>NAMA ANGGOTA</th>
											<th>STATUS DLM KELUARGA</th>
											<th>STATUS DLM PERKAWINAN</th>
											<th>JENIS KELAMIN</th>
											<th>TGL. LAHIR / UMUR</th>
											<th>PENDIDIKAN</th>
											<th>PEKERJAAN</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$sqlAnggota = "SELECT * FROM tbl_anggota_kel WHERE idUser = '$id'";
										$queryAnggota = mysqli_query($db, $sqlAnggota);
										$no = 1;
										if (mysqli_num_rows($queryAnggota) < 1) {
										?>
											<td colspan="9" class="text-center">TIDAK ADA DATA ANGGOTA</td>
											<?php
										} else {
											while ($dataAnggota = mysqli_fetch_array($queryAnggota)) {
											?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $dataAnggota['noRegAnggota'] ?></td>
													<td><?php echo $dataAnggota['namaAnggota'] ?></td>
													<td><?php echo $dataAnggota['statusKeluargaAnggota'] ?></td>
													<td><?php echo $dataAnggota['statusPerkawinanAnggota'] ?></td>
													<td><?php echo $dataAnggota['jenisKelaminAnggota'] ?></td>
													<td><?php echo $dataAnggota['tglLahirAnggota'] ?></td>
													<td><?php echo $dataAnggota['pendidikanAnggota'] ?></td>
													<td><?php echo $dataAnggota['pekerjaanAnggota'] ?></td>
												</tr>

										<?php
												$no++;
											}
										}
										?>
									</tbody>
								</table>
							</div>

							<div class="col mb-3">
								<label for="pokok" class="form-label">Makanan Pokok Sehari hari: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="makananPokok" id="beras" value="Beras">
									<label class="form-check-label" for="beras">Beras</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="makananPokok" id="non-beras" value="Non Beras">
									<label class="form-check-label" for="non-beras">Non Beras</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="jamban" class="form-label">Memiliki Jamban Keluarga: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jambanKeluarga" id="yaJamban" value="Ya">
									<label class="form-check-label" for="yaJamban">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jambanKeluarga" id="tidakJamban" value="Tidak">
									<label class="form-check-label" for="tidakJamban">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="air" class="form-label">Sumber Air Keluarga: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="sumberAir" id="pdam" value="PDAM">
									<label class="form-check-label" for="pdam">PDAM</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="sumberAir" id="sumur" value="Sumur">
									<label class="form-check-label" for="sumur">Sumur</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="sumberAir" id="lainnya" value="Lainnya">
									<label class="form-check-label" for="lainnya">Lainnya</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="sampah" class="form-label">Memiliki Tempat Pembuangan Sampah: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="memilikiTPS" id="yatps" value="Ya">
									<label class="form-check-label" for="yatps">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="memilikiTPS" id="tidaktps" value="Tidak">
									<label class="form-check-label" for="tidaktps">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="limbah" class="form-label">Mempunyai Saluran Pembuangan Air Limbah: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="saluranLimbah" id="yalimbah" value="Ya">
									<label class="form-check-label" for="yalimbah">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="saluranLimbah" id="tidaklimbah" value="Tidak">
									<label class="form-check-label" for="tidaklimbah">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="p4k" class="form-label">Menempel Stiker P4K: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="stikerP4K" id="yap4k" value="Ya">
									<label class="form-check-label" for="yap4k">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="stikerP4K" id="tidakp4k" value="Tidak">
									<label class="form-check-label" for="tidakp4k">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="rumah" class="form-label">Kriteria Rumah: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kriteriaRumah" id="sehat" value="Sehat">
									<label class="form-check-label" for="sehat">Sehat</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kriteriaRumah" id="tidakSehat" value="Tidak Sehat">
									<label class="form-check-label" for="tidakSehat">Tidak Sehat</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="up2k" class="form-label">Aktivitas UP2K: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="up2k" id="yaup2k" value="Ya">
									<label class="form-check-label" for="yaup2k">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="up2k" id="tidakup2k" value="Tidak">
									<label class="form-check-label" for="tidakup2k">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="up2k" class="form-label">Jenis Usaha (Jika tidak ada aktivitas UP2K): </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jenisUsaha" id="warung" value="Warung">
									<label class="form-check-label" for="warung">Warung</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jenisUsaha" id="koperasi" value="Kegiatan Koperasi">
									<label class="form-check-label" for="koperasi">Kegiatan Koperasi</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jenisUsaha" id="tidakAda" value="Tidak Ada" checked>
									<label class="form-check-label" for="TidakAda">Tidak Ada</label>
								</div>
							</div>

							<div class="tombol text-end">
								<button type="submit" class="btn btn-primary" name="save" value="save">Save</button>
							</div>
						</form>
						<!-- Modal -->
						<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<form action="confDataKeluarga.php" method="post">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data</h1>
											<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
										</div>
										<div class="modal-body">
											<div class="mb-3">
												<label for="reg" class="form-label">No. Reg</label>
												<input type="text" class="form-control" id="reg" aria-describedby="emailHelp" placeholder="Masukkan No Reg" name="noRegAnggota">
											</div>
											<div class="mb-3">
												<label for="anggota" class="form-label">Nama Anggota</label>
												<input type="text" class="form-control" id="anggota" aria-describedby="emailHelp" placeholder="Masukkan Nama Anggota" name="namaAnggota">
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Status Dalam Keluarga</label>
												<select class="form-select" aria-label="Default select example" name="statusKeluargaAnggota">
													<option selected disabled>Pilih Status Dalam Keluarga</option>
													<option value="Suami">Suami</option>
													<option value="Istri">Istri</option>
													<option value="Anak">Anak</option>
													<option value="Menantu">Menantu</option>
													<option value="Keluarga">Keluarga</option>
													<option value="Dll">Dll</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Status Perkawinan</label>
												<select class="form-select" aria-label="Default select example" name="statusPerkawinanAnggota">
													<option selected disabled>Pilih Status Perkawinan</option>
													<option value="Kawin">Kawin</option>
													<option value="Tidak Kawin">Tidak Kawin</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Jenis Kelamin</label>
												<select class="form-select" aria-label="Default select example" name="jenisKelaminAnggota">
													<option selected disabled>Pilih Jenis Kelamin</option>
													<option value="Laki-laki">Laki-laki</option>
													<option value="Perempuan">Perempuan</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="tanggal" class="form-label">Tanggal Lahir</label>
												<input type="date" class="form-control" id="tanggal" aria-describedby="emailHelp" placeholder="Masukkan Tanggal Lahir" name="tglLahirAnggota">
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Pendidikan</label>
												<select class="form-select" aria-label="Default select example" name="pendidikanAnggota">
													<option selected disabled>Pilih Pendidikan</option>
													<option value="Tamat SD">Tamat SD</option>
													<option value="SD/MI">SD/MI</option>
													<option value="SMP/Sederajat">SMP/Sederajat</option>
													<option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
													<option value="Diploma">Diploma</option>
													<option value="S1">S1</option>
													<option value="S2">S2</option>
													<option value="S3">S3</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Pekerjaan</label>
												<select class="form-select" aria-label="Default select example" name="pekerjaanAnggota" required>
													<option selected disabled>Pilih Pekerjaan</option>
													<option value="Pelajar">Pelajar</option>
													<option value="Petani">Petani</option>
													<option value="Pedagang">Pedagang</option>
													<option value="Swasta">Swasta</option>
													<option value="Wirausaha">Wirausaha</option>
													<option value="PNS">PNS</option>
													<option value="TNI/Polri">TNI/Polri</option>
													<option value="Dll">Dll</option>
												</select>
											</div>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
											<button type="submit" class="btn btn-primary" name="saveAnggota" value="saveAnggota">Simpan</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					<?php
					} else {
					?>
						<form action="confDataKeluarga.php" method="post">
							<div class="mb-3">
								<label for="dasawisma" class="form-label">Dasa Wisma</label>
								<input type="text" class="form-control" id="dasawisma" aria-describedby="emailHelp" placeholder="Masukkan Dasa Wisma" name="dasaWisma" value="<?php echo $data['dasaWisma'] ?>">
							</div>
							<div class="mb-3">
								<label for="rt" class="form-label">RT</label>
								<input type="text" class="form-control" id="rt" aria-describedby="emailHelp" placeholder="Masukkan RT" name="rt" value="<?php echo $data['rt'] ?>">
							</div>
							<div class=" mb-3">
								<label for="rw" class="form-label">RW</label>
								<input type="text" class="form-control" id="rw" aria-describedby="emailHelp" placeholder="Masukkan RW" name="rw" value="<?php echo $data['rw'] ?>">
							</div>
							<div class=" mb-3">
								<label for="dusun" class="form-label">Dusun/Ling</label>
								<input type="text" class="form-control" id="dusun" aria-describedby="emailHelp" placeholder="Masukkan Dusun/Ling" name="dusun" value="<?php echo $data['dusun'] ?>">
							</div>
							<div class=" mb-3">
								<label for="desa" class="form-label">Desa/Kel</label>
								<input type="text" class="form-control" id="desa" aria-describedby="emailHelp" placeholder="Masukkan Desa/Kel" name="desa" value="<?php echo $data['desa'] ?>">
							</div>
							<div class=" mb-3">
								<label for="kec" class="form-label">Kec</label>
								<input type="text" class="form-control" id="kec" aria-describedby="emailHelp" placeholder="Masukkan kec" name="kec" value="<?php echo $data['kec'] ?>">
							</div>
							<div class=" mb-3">
								<label for="kota" class="form-label">Kab/Kota</label>
								<input type="text" class="form-control" id="kota" aria-describedby="emailHelp" placeholder="Masukkan Kab/Kota" name="kab" value="<?php echo $data['kab'] ?>">
							</div>
							<div class=" mb-3">
								<label for="prov" class="form-label">Prov</label>
								<input type="text" class="form-control" id="prov" aria-describedby="emailHelp" placeholder="Masukkan Prov" name="prov" value="<?php echo $data['prov'] ?>">
							</div>
							<div class=" mb-3">
								<label for="kepalakeluarga" class="form-label">Nama Kepala Keluarga</label>
								<input type="text" class="form-control" id="kepalakeluarga" aria-describedby="emailHelp" placeholder="Masukkan Nama Kepala Keluarga" name="namaKepalaK" value="<?php echo $data['namaKepalaK'] ?>">
							</div>
							<div class=" mb-3">
								<label for="jml" class="form-label">Jumlah Anggota Keluarga</label>
								<input type="text" class="form-control" id="jml" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Anggota Keluarga" name="jmlhAnggota" value="<?php echo $data['jmlhAnggota'] ?>">
							</div>
							<div class=" mb-3">
								<label for="kk" class="form-label">Jumlah KK</label>
								<input type="text" class="form-control" id="kk" aria-describedby="emailHelp" placeholder="Masukkan Jumlah KK" name="jumlahKK" value="<?php echo $data['jumlahKK'] ?>">
							</div>
							<div class=" mb-3">
								<label for="balita" class="form-label">Jumlah Balita (Anak)</label>
								<input type="number" class="form-control" id="balita" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Balita" name="jumlahBalita" value="<?php echo $data['jumlahBalita'] ?>">
							</div>
							<div class=" mb-3">
								<label for="pus" class="form-label">Jumlah PUS (Pasang)</label>
								<input type="number" class="form-control" id="pus" aria-describedby="emailHelp" placeholder="Masukkan Jumlah PUS" name="jumlahPus" value="<?php echo $data['jumlahPus'] ?>">
							</div>
							<div class=" mb-3">
								<label for="wus" class="form-label">Jumlah WUS (Orang)</label>
								<input type="number" class="form-control" id="wus" aria-describedby="emailHelp" placeholder="Masukkan Jumlah WUS" name="jumlahWus" value="<?php echo $data['jumlahWus'] ?>">
							</div>
							<div class=" mb-3">
								<label for="buta" class="form-label">Jumlah Buta (Orang)</label>
								<input type="number" class="form-control" id="buta" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Buta" name="jumlahButa" value="<?php echo $data['jumlahButa'] ?>">
							</div>
							<div class=" mb-3">
								<label for="hamil" class="form-label">Jumlah Ibu Hamil (Orang)</label>
								<input type="number" class="form-control" id="hamil" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Ibu Hamil" name="jumlahHamil" value="<?php echo $data['jumlahHamil'] ?>">
							</div>
							<div class=" mb-3">
								<label for="menyusui" class="form-label">Jumlah Ibu Menyusui (Orang)</label>
								<input type="number" class="form-control" id="menyusui" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Menyusui" name="jumlahMenyusui" value="<?php echo $data['jumlahMenyusui'] ?>">
							</div>
							<div class=" mb-3">
								<label for="lansia" class="form-label">Jumlah Lansia (Orang)</label>
								<input type="number" class="form-control" id="lansia" aria-describedby="emailHelp" placeholder="Masukkan Jumlah Lansia" name="jumlahLansia" value="<?php echo $data['jumlahLansia'] ?>">
							</div>
							<div class=" mb-3">
								<label for="exampleFormControlInput1" class="form-label">Berkebutuhan Khusus:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="berkebutuhanKhusus" id="fisik" value="Fisik" <?php if ($data['berkebutuhanKhusus'] == 'Fisik') { ?> checked <?php } ?>>
									<label class="form-check-label" for="fisik">Fisik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="berkebutuhanKhusus" id="non-fisik" value="Non Fisik" <?php if ($data['berkebutuhanKhusus'] == 'Non Fisik') { ?> checked <?php } ?>>
									<label class="form-check-label" for="non-fisik">Non Fisik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="berkebutuhanKhusus" id="tidakBerkebutuhan" value="Tidak Berkebutuhan Khusus" <?php if ($data['berkebutuhanKhusus'] == 'Tidak Berkebutuhan Khusus') { ?> checked <?php } ?>>
									<label class="form-check-label" for="tidakBerkebutuhan">Tidak Berkebutuhan Khusus</label>
								</div>
							</div>

							<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
								Tambah Data
							</button>

							<div class="tabel table-responsive mb-4 mt-4">
								<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
									<thead class="table-warning">
										<tr>
											<th>NO</th>
											<th>NO. REG</th>
											<th>NAMA ANGGOTA</th>
											<th>STATUS DLM KELUARGA</th>
											<th>STATUS DLM PERKAWINAN</th>
											<th>JENIS KELAMIN</th>
											<th>TGL. LAHIR / UMUR</th>
											<th>PENDIDIKAN</th>
											<th>PEKERJAAN</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$sqlAnggota = "SELECT * FROM tbl_anggota_kel WHERE idUser = '$id'";
										$queryAnggota = mysqli_query($db, $sqlAnggota);
										$no = 1;
										if (mysqli_num_rows($queryAnggota) < 1) {
										?>
											<td colspan="9" class="text-center">TIDAK ADA DATA ANGGOTA</td>
											<?php
										} else {
											while ($dataAnggota = mysqli_fetch_array($queryAnggota)) {
											?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $dataAnggota['noRegAnggota'] ?></td>
													<td><?php echo $dataAnggota['namaAnggota'] ?></td>
													<td><?php echo $dataAnggota['statusKeluargaAnggota'] ?></td>
													<td><?php echo $dataAnggota['statusPerkawinanAnggota'] ?></td>
													<td><?php echo $dataAnggota['jenisKelaminAnggota'] ?></td>
													<td><?php echo $dataAnggota['tglLahirAnggota'] ?></td>
													<td><?php echo $dataAnggota['pendidikanAnggota'] ?></td>
													<td><?php echo $dataAnggota['pekerjaanAnggota'] ?></td>
												</tr>

										<?php
												$no++;
											}
										}
										?>
									</tbody>
								</table>
							</div>

							<div class="col mb-3">
								<label for="pokok" class="form-label">Makanan Pokok Sehari hari: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="makananPokok" id="beras" value="Beras" <?php if ($data['makananPokok'] == 'Beras') { ?> checked <?php } ?>>
									<label class="form-check-label" for="beras">Beras</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="makananPokok" id="non-beras" value="Non Beras" <?php if ($data['makananPokok'] == 'Non Beras') { ?> checked <?php } ?>>
									<label class="form-check-label" for="non-beras">Non Beras</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="jamban" class="form-label">Memiliki Jamban Keluarga: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jambanKeluarga" id="yaJamban" value="Ya" <?php if ($data['jambanKeluarga'] == 'Ya') { ?> checked <?php } ?>>
									<label class="form-check-label" for="yaJamban">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jambanKeluarga" id="tidakJamban" value="Tidak" <?php if ($data['jambanKeluarga'] == 'Tidak') { ?> checked <?php } ?>>
									<label class="form-check-label" for="tidakJamban">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="air" class="form-label">Sumber Air Keluarga: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="sumberAir" id="pdam" value="PDAM" <?php if ($data['sumberAir'] == 'PDAM') { ?> checked <?php } ?>>
									<label class="form-check-label" for="pdam">PDAM</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="sumberAir" id="sumur" value="Sumur" <?php if ($data['sumberAir'] == 'Sumur') { ?> checked <?php } ?>>
									<label class="form-check-label" for="sumur">Sumur</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="sumberAir" id="lainnya" value="Lainnya" <?php if ($data['sumberAir'] == 'Lainnya') { ?> checked <?php } ?>>
									<label class="form-check-label" for="lainnya">Lainnya</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="sampah" class="form-label">Memiliki Tempat Pembuangan Sampah: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="memilikiTPS" id="yatps" value="Ya" <?php if ($data['memilikiTPS'] == 'Ya') { ?> checked <?php } ?>>
									<label class="form-check-label" for="yatps">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="memilikiTPS" id="tidaktps" value="Tidak" <?php if ($data['memilikiTPS'] == 'Tidak') { ?> checked <?php } ?>>
									<label class="form-check-label" for="tidaktps">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="limbah" class="form-label">Mempunyai Saluran Pembuangan Air Limbah: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="saluranLimbah" id="yalimbah" value="Ya" <?php if ($data['saluranLimbah'] == 'Ya') { ?> checked <?php } ?>>
									<label class="form-check-label" for="yalimbah">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="saluranLimbah" id="tidaklimbah" value="Tidak" <?php if ($data['saluranLimbah'] == 'Tidak') { ?> checked <?php } ?>>
									<label class="form-check-label" for="tidaklimbah">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="p4k" class="form-label">Menempel Stiker P4K: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="stikerP4K" id="yap4k" value="Ya" <?php if ($data['stikerP4K'] == 'Ya') { ?> checked <?php } ?>>
									<label class="form-check-label" for="yap4k">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="stikerP4K" id="tidakp4k" value="Tidak" <?php if ($data['stikerP4K'] == 'Tidak') { ?> checked <?php } ?>>
									<label class="form-check-label" for="tidakp4k">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="rumah" class="form-label">Kriteria Rumah: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kriteriaRumah" id="sehat" value="Sehat" <?php if ($data['kriteriaRumah'] == 'Sehat') { ?> checked <?php } ?>>
									<label class="form-check-label" for="sehat">Sehat</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kriteriaRumah" id="tidakSehat" value="Tidak Sehat" <?php if ($data['kriteriaRumah'] == 'Tidak Sehat') { ?> checked <?php } ?>>
									<label class="form-check-label" for="tidakSehat">Tidak Sehat</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="up2k" class="form-label">Aktivitas UP2K: </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="up2k" id="yaup2k" value="Ya" <?php if ($data['up2k'] == 'Ya') { ?> checked <?php } ?>>
									<label class="form-check-label" for="yaup2k">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="up2k" id="tidakup2k" value="Tidak" <?php if ($data['up2k'] == 'Tidak') { ?> checked <?php } ?>>
									<label class="form-check-label" for="tidakup2k">Tidak</label>
								</div>
							</div>
							<div class="col mb-3">
								<label for="up2k" class="form-label">Jenis Usaha (Jika tidak ada aktivitas UP2K): </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jenisUsaha" id="warung" value="Warung" <?php if ($data['jenisUsaha'] == 'Warung') { ?> checked <?php } ?>>
									<label class="form-check-label" for="warung">Warung</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jenisUsaha" id="koperasi" value="Kegiatan Koperasi" <?php if ($data['jenisUsaha'] == 'Kegiatan Koperasi') { ?> checked <?php } ?>>
									<label class="form-check-label" for="koperasi">Kegiatan Koperasi</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jenisUsaha" id="tidakAda" value="Tidak Ada" <?php if ($data['jenisUsaha'] == 'Tidak Ada') { ?> checked <?php } ?>>
									<label class="form-check-label" for="TidakAda">Tidak Ada</label>
								</div>
							</div>

							<div class="tombol text-end">
								<button type="submit" class="btn btn-primary" name="update" value="update">Selanjutnya</button>
							</div>
						</form>
						<!-- Modal -->
						<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<form action="confDataKeluarga.php" method="post">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data</h1>
											<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
										</div>
										<div class="modal-body">
											<div class="mb-3">
												<label for="reg" class="form-label">No. Reg</label>
												<input type="text" class="form-control" id="reg" aria-describedby="emailHelp" placeholder="Masukkan No Reg" name="noRegAnggota">
											</div>
											<div class="mb-3">
												<label for="anggota" class="form-label">Nama Anggota</label>
												<input type="text" class="form-control" id="anggota" aria-describedby="emailHelp" placeholder="Masukkan Nama Anggota" name="namaAnggota">
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Status Dalam Keluarga</label>
												<select class="form-select" aria-label="Default select example" name="statusKeluargaAnggota">
													<option selected disabled>Pilih Status Dalam Keluarga</option>
													<option value="Suami">Suami</option>
													<option value="Istri">Istri</option>
													<option value="Anak">Anak</option>
													<option value="Menantu">Menantu</option>
													<option value="Keluarga">Keluarga</option>
													<option value="Dll">Dll</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Status Perkawinan</label>
												<select class="form-select" aria-label="Default select example" name="statusPerkawinanAnggota">
													<option selected disabled>Pilih Status Perkawinan</option>
													<option value="Kawin">Kawin</option>
													<option value="Tidak Kawin">Tidak Kawin</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Jenis Kelamin</label>
												<select class="form-select" aria-label="Default select example" name="jenisKelaminAnggota">
													<option selected disabled>Pilih Jenis Kelamin</option>
													<option value="Laki-laki">Laki-laki</option>
													<option value="Perempuan">Perempuan</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="tanggal" class="form-label">Tanggal Lahir</label>
												<input type="date" class="form-control" id="tanggal" aria-describedby="emailHelp" placeholder="Masukkan Tanggal Lahir" name="tglLahirAnggota">
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Pendidikan</label>
												<select class="form-select" aria-label="Default select example" name="pendidikanAnggota">
													<option selected disabled>Pilih Pendidikan</option>
													<option value="Tamat SD">Tamat SD</option>
													<option value="SD/MI">SD/MI</option>
													<option value="SMP/Sederajat">SMP/Sederajat</option>
													<option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
													<option value="Diploma">Diploma</option>
													<option value="S1">S1</option>
													<option value="S2">S2</option>
													<option value="S3">S3</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Pekerjaan</label>
												<select class="form-select" aria-label="Default select example" name="pekerjaanAnggota" required>
													<option selected disabled>Pilih Pekerjaan</option>
													<option value="Pelajar">Pelajar</option>
													<option value="Petani">Petani</option>
													<option value="Pedagang">Pedagang</option>
													<option value="Swasta">Swasta</option>
													<option value="Wirausaha">Wirausaha</option>
													<option value="PNS">PNS</option>
													<option value="TNI/Polri">TNI/Polri</option>
													<option value="Dll">Dll</option>
												</select>
											</div>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
											<button type="submit" class="btn btn-primary" name="saveAnggota" value="saveAnggota">Simpan</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					<?php
					}
					?>

				</div>
			</div>
		</div>

	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	// $(document).ready(function(){
	// 	$('[data-bs-toggle="popover"]').popover();   
	// });
	// const popover = new bootstrap.Popover('.popover-dismiss', {
	// 	trigger: 'focus'
	// })
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

</html>