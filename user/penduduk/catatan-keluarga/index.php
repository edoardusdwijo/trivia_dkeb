<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'user') {
	header("location:../../../login-user");
	exit;
}
$id = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/catatankeluarga-user.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../assets/image/logo.jpeg">
	<title>Input Catatan Keluarga-User</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../../">Home</a>
		<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a href="../data-warga/">Input Data Warga</a>
			<a href="../data-keluarga/">Input Data Keluarga</a>
			<a class="active" href="./">Input Catatan Keluarga</a>
		</div>
		<a href="../../cetak/">Cetak</a>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../../">Home</a>
				<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a href="../data-warga/">Input Data Warga</a>
					<a href="../data-keluarga/">Input Data Keluarga</a>
					<a class="active" href="./">Input Catatan Keluarga</a>
				</div>
				<a href="../../cetak/">Cetak</a>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul text-center">
			<p>CATATAN KELUARGA</p>
		</div>

		<div class="form mb-4">
			<div class="card">
				<!-- <div class="card-header">
					Featured
				</div> -->
				<?php
				$sqlCheckData = "SELECT * FROM tbl_catatan_keluarga WHERE idCatatanKeluarga='$id'";
				$queryCheckData = mysqli_query($db, $sqlCheckData);

				if (mysqli_num_rows($queryCheckData) < 1) {
				?>
					<div class="card-body">
						<div class="tabel d-flex flex-sm-row flex-column">
							<table border="0" width="40%">
								<?php
								$sqlDataWarga = "SELECT * FROM tbl_data_Warga WHERE idUser = '$id'";
								$queryDataWarga = mysqli_query($db, $sqlDataWarga);
								$dataWarga = mysqli_fetch_array($queryDataWarga);
								$sqlDataKeluarga = "SELECT * FROM tbl_data_keluarga WHERE idKeluarga = '$id'";
								$queryDataKeluarga = mysqli_query($db, $sqlDataKeluarga);
								$dataKeluarga = mysqli_fetch_array($queryDataKeluarga);
								?>
								<form action="confCatatanKeluarga.php" method="post">

									<tr>
										<td width="60%">Catatan Keluarga Dari</td>
										<td>:</td>
										<td><?php echo $dataWarga['kepalaRumahTangga'] ?></td>
										<input type="text" name="kepalaRumahTangga" value="<?php echo $dataWarga['kepalaRumahTangga'] ?>" hidden>
									</tr>
									<tr>
										<td width="60%">Anggota Kelompok Dasawisma</td>
										<td>:</td>
										<td><?php echo $dataWarga['dasaWisma'] ?></td>
										<input type="text" name="dasaWisma" value="<?php echo $dataWarga['dasaWisma'] ?>" hidden>
									</tr>
									<tr>
										<td width="60%">Tahun</td>
										<td>:</td>
										<td><?php echo date('Y') ?></td>
										<input type="text" name="tahun" value="<?php echo date('Y') ?>" hidden>
									</tr>
							</table>

							<table border="0" width="40%">
								<tr>
									<td width="50%">Kriteria Rumah</td>
									<td>:</td>
									<td><?php echo $dataKeluarga['kriteriaRumah'] ?></td>
									<input type="text" name="kriteriaRumah" value="<?php echo $dataKeluarga['kriteriaRumah'] ?>" hidden>
								</tr>
								<tr>
									<td width="50%">Jamban Keluarga</td>
									<td>:</td>
									<td><?php echo $dataKeluarga['jambanKeluarga'] ?></td>
									<input type="text" name="jambanKeluarga" value="<?php echo $dataKeluarga['jambanKeluarga'] ?>" hidden>
								</tr>
								<tr>
									<td width="50%">Tempat Sampah</td>
									<td>:</td>
									<td><?php echo $dataKeluarga['memilikiTPS'] ?></td>
									<input type="text" name="memilikiTPS" value="<?php echo $dataKeluarga['memilikiTPS'] ?>" hidden>
								</tr>
							</table>
						</div>

						<!-- Button trigger modal -->
						<button type="button" class="btn btn-primary mt-4" data-bs-toggle="modal" data-bs-target="#exampleModal">
							Tambah Data
						</button>

						<div class="tabel table-responsive mb-4 mt-4">
							<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
								<thead class="table-warning">
									<tr>
										<th scope="col" class="text-center" rowspan="2">NO</th>
										<th scope="col" class="text-center" rowspan="2">NAMA ANGGOTA KELUARGA</th>
										<th scope="col" class="text-center" rowspan="2">STATUS PERKAWINAN</th>
										<th scope="col" class="text-center" rowspan="2">L/P</th>
										<th scope="col" class="text-center" rowspan="2">TEMPAT LAHIR</th>
										<th scope="col" class="text-center" rowspan="2">TGL/BL/TH LAHIR/UMUR</th>
										<th scope="col" class="text-center" rowspan="2">AGAMA</th>
										<th scope="col" class="text-center" rowspan="2">PENDIDIKAN</th>
										<th scope="col" class="text-center" rowspan="2">PEKERJAAN</th>
										<th scope="col" class="text-center" rowspan="2">BERKEBUTUHAN KHUSUS</th>
										<th scope="col" class="text-center" colspan="8">KEGIATAN YANG DIIKUTI</th>
										<th scope="col" class="text-center" rowspan="2">KET</th>
									</tr>
									<tr>
										<th>PENGHAYATAN DAN PENGAMALAN PANCASILA</th>
										<th>GOTONG ROYONG</th>
										<th>PENDIDIKAN DAN KETRAMPILAN</th>
										<th>PENGEMBANGAN KEHIDUPAN BERKOPERASI</th>
										<th>PANGAN</th>
										<th>SANDANG</th>
										<th>KESEHATAN</th>
										<th>PERENCANAAN SEHAT</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>2</td>
										<td>3</td>
										<td>4</td>
										<td>5</td>
										<td>6</td>
										<td>7</td>
										<td>8</td>
										<td>9</td>
										<td>10</td>
										<td>11</td>
										<td>12</td>
										<td>13</td>
										<td>14</td>
										<td>15</td>
										<td>16</td>
										<td>17</td>
										<td>18</td>
										<td>19</td>
									</tr>
									<?php
									$sqlListCatatan = "SELECT * FROM tbl_list_catatan_keluarga WHERE idCatatanKeluarga = '$id'";
									$queryListCatatan = mysqli_query($db, $sqlListCatatan);
									$no = 1;
									if (mysqli_num_rows($queryListCatatan) < 1) {
									?>
										<tr>
											<td colspan="19" class="text-center">TIDAK ADA CATATAN KELUARGA</td>
										</tr>
										<?php
									} else {
										while ($dataList = mysqli_fetch_array($queryListCatatan)) {
										?>
											<tr>
												<td><?php echo $no ?></td>
												<td><?php echo $dataList['anggotaKeluarga'] ?></td>
												<td><?php echo $dataList['statusKawinAnggota'] ?></td>
												<td><?php echo $dataList['jenisKelaminAnggota'] ?></td>
												<td><?php echo $dataList['tempatLahirAnggota'] ?></td>
												<td><?php echo $dataList['tanggalLahirAnggota'] ?></td>
												<td><?php echo $dataList['agamaAnggota'] ?></td>
												<td><?php echo $dataList['pendidikanAnggota'] ?></td>
												<td><?php echo $dataList['pekerjaanAnggota'] ?></td>
												<td><?php echo $dataList['berkebutuhanKhususAnggota'] ?></td>
												<td><?php echo $dataList['pengamalanPancasila'] ?></td>
												<td><?php echo $dataList['gotongRoyong'] ?></td>
												<td><?php echo $dataList['pendidikanKeterampilan'] ?></td>
												<td><?php echo $dataList['kehidupanBerkoperasi'] ?></td>
												<td><?php echo $dataList['pangan'] ?></td>
												<td><?php echo $dataList['sandang'] ?></td>
												<td><?php echo $dataList['kesehatan'] ?></td>
												<td><?php echo $dataList['perencanaanKesehatan'] ?></td>
												<td><?php echo $dataList['keterangan'] ?></td>
											</tr>
									<?php
										}
									}
									?>
								</tbody>
							</table>
						</div>

						<div class="tombol text-end mt-2">
							<button class="btn btn-primary" type="submit" role="button" name="submitAll" value="submitAll"><img src="../../../assets/icon/icon-upload.png"> Simpan</button>
						</div>
						</form>

						<!-- Modal -->
						<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>
									<div class="modal-body">
										<form action="confCatatanKeluarga.php" method="post">
											<div class="mb-3">
												<label for="nama" class="form-label">Nama Anggota Keluarga</label>
												<input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Anggota Keluarga" name="anggotaKeluarga">
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Status Perkawinan</label>
												<select class="form-select" aria-label="Default select example" name="statusKawinAnggota">
													<option selected disabled>Pilih Status Perkawinan</option>
													<option value="Menikah">Menikah</option>
													<option value="Lajang">Lajang</option>
													<option value="Cerai Mati">Cerai Mati</option>
													<option value="Cerai Hidup">Cerai Hidup</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Jenis Kelamin</label>
												<select class="form-select" aria-label="Default select example" name="jenisKelaminAnggota">
													<option selected disabled>Pilih Jenis Kelamin</option>
													<option value="Laki-laki">Laki-laki</option>
													<option value="Perempuan">Perempuan</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="tempat" class="form-label">Tempat Lahir</label>
												<input type="text" class="form-control" id="tempat" placeholder="Masukkan Tempat Lahir" name="tempatLahirAnggota">
											</div>
											<div class="mb-3">
												<label for="tanggal" class="form-label">Tanggal Lahir</label>
												<input type="date" class="form-control" id="tanggal" placeholder="Masukkan Tanggal Lahir" name="tanggalLahirAnggota">
											</div>
											<div class="mb-3">
												<label for="agama" class="form-label">Agama</label>
												<input type="text" class="form-control" id="agama" placeholder="Masukkan Agama" name="agamaAnggota">
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Pendidikan</label>
												<select class="form-select" aria-label="Default select example" name="pendidikanAnggota">
													<option selected disabled>Pilih Pendidikan</option>
													<option value="Tamat SD">Tamat SD</option>
													<option value="SD/MI">SD/MI</option>
													<option value="SMP/Sederajat">SMP/Sederajat</option>
													<option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
													<option value="Diploma">Diploma</option>
													<option value="S1">S1</option>
													<option value="S2">S2</option>
													<option value="S3">S3</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="status" class="form-label">Pekerjaan</label>
												<select class="form-select" aria-label="Default select example" name="pekerjaanAnggota">
													<option selected disabled>Pilih Pekerjaan</option>
													<option value="Petani">Petani</option>
													<option value="Pedagang">Pedagang</option>
													<option value="Swasta">Swasta</option>
													<option value="Wirausaha">Wirausaha</option>
													<option value="PNS">PNS</option>
													<option value="TNI/Polri">TNI/Polri</option>
													<option value="Dll">Dll</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="kebutuhan" class="form-label">Berkebutuhuan Khusus</label>
												<select class="form-select" aria-label="Default select example" name="berkebutuhanKhususAnggota">
													<option selected disabled>Apakah Berkebutuhan Khusus</option>
													<option value="Ya">Ya</option>
													<option value="Tidak">Tidak</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="pancasila" class="form-label">Penghayatan dan Pengamalan Pancasila</label>
												<select class="form-select" aria-label="Default select example" name="pengamalanPancasila">
													<option selected disabled>Penghayatan dan Pemgalan Pancasila</option>
													<option value="Ya">Ya</option>
													<option value="Tidak">Tidak</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="gotong" class="form-label">Gotong Royong</label>
												<select class="form-select" aria-label="Default select example" name="gotongRoyong">
													<option selected disabled>Gotong Royong</option>
													<option value="Ya">Ya</option>
													<option value="Tidak">Tidak</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="ketrampilan" class="form-label">Pendidikan dan Ketrampilan</label>
												<select class="form-select" aria-label="Default select example" name="pendidikanKeterampilan">
													<option selected disabled>Pendidikan dan Ketrampilan</option>
													<option value="Ya">Ya</option>
													<option value="Tidak">Tidak</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="Pengembangan" class="form-label">Pengembangan Kehidupan Berkoperasi</label>
												<input type="text" class="form-control" id="tempat" placeholder="Masukkan Kegiatan yang diikuti Anggota Keluarga" name="kehidupanBerkoperasi">
											</div>
											<div class="mb-3">
												<label for="pangan" class="form-label">Pangan</label>
												<select class="form-select" aria-label="Default select example" name="pangan">
													<option selected disabled>Pangan</option>
													<option value="Ya">Ya</option>
													<option value="Tidak">Tidak</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="sandang" class="form-label">Sandang</label>
												<select class="form-select" aria-label="Default select example" name="sandang">
													<option selected disabled>Sandang</option>
													<option value="Ya">Ya</option>
													<option value="Tidak">Tidak</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="kesehatan" class="form-label">Kesehatan</label>
												<select class="form-select" aria-label="Default select example" name="kesehatan">
													<option selected disabled>Kesehatan</option>
													<option value="Kegiatan Posyandu Balita/Lansia">Kegiatan Posyandu Balita/Lansia</option>
													<option value="PHBS">PHBS</option>
													<option value="Kegiatan Lainnya">Kegiatan Lainnya</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="perencanaan" class="form-label">Perencanaan Kesehatan</label>
												<select class="form-select" aria-label="Default select example" name="perencanaanKesehatan">
													<option selected disabled>Perencanaan Kesehatan</option>
													<option value="Ya">Ya</option>
													<option value="Tidak">Tidak</option>
												</select>
											</div>
											<div class="mb-3">
												<label for="ket" class="form-label">Keterangan</label>
												<textarea class="form-control" id="ket" rows="3" name="keterangan"></textarea>
											</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
										<button type="submit" class="btn btn-primary" name="submit" value="submit">Simpan</button>
									</div>
									</form>
								</div>
							</div>
						</div>
						<div class="keterangan">
							<p class="ket">KETERANGAN:</p>

							<div class="isi">
								<p>Kolom 2 : Diisi dengan nama seluruh anggota keluarga yang ada dalam rumah tangga </p>
								<p>Kolom 3 : Diisi Menikah, Lajang, Cerai, Mati, Cerai Hidup</p>
								<p>Kolom 4 : Diisi Dengan Jenis Kelamin Laki-laki/Perempuan</p>
								<p>Kolom 10 : Diisi Dengan kondisi anggota keluarga (catat mental, cacat fisik, dsb)</p>
								<p>Kolom 11-18 : di isi dengan jenis kegiatan yang diikuti oleh masing-masing anggota keluarga</p>
								<p>Kolom 11 : di isi dengan kegiatan keagamaan, PKBN, Pola Asuh, Pencegahan KDRT, Pencegahan Trafficking, Narkoba, Pencegahan kejahatan seksual yang ikuti oleh anggota keluarga</p>
								<p>Kolom 12 : Di isi dengan kegiatan kerjabakti, jimpitan, arisan, rukun kematian, bakti sosial, dsb yang diikuti oleh anggota keluarga</p>
								<p>Kolom 13 : Di isi dengan kegiatan BKB, PAUD Sejenis, Paket ABC, KF yang diikuti oleh anggota keluarga</p>
								<p>Kolom 14 : Di isi dengan kegiatan UP2K, KOPERASI, yang diikuti oleh anggota keluarga</p>
								<p>Kolom 15 : di isi dengan jenis makanan pokok ( beras dan non beras/pangan lokal) anggota keluarga dan pemanfaatan halaman pekarangan yang dilakukan oleh anggota keluarga</p>
								<p>Kolom 16 : Di isi dengan kegiatan usaha yang berkaitan dengan usaha sandang</p>
								<p>Kolom 17 : Di isi dengan anggota keluarga yang mengikuti kegiatan posyandu Balita/Lansia dan PHBS, dan kegiatan kesehatan lainnya.</p>
								<p>Kolom 18 : Di isi dengan anggota keluarga yang mengikuti Program KB menjadi Peserta BPJS Kesehatan, menabung untuk masa depan keluarga</p>
								<p>Kolom 19 : Di isi dengan hal-hal yang belum tercantum dalam kolom-kolom sebelumnya</p>
							</div>
						</div>
					</div>
				<?php
				}else{
					?>
					<div class="card-body">
					<div class="tabel d-flex flex-sm-row flex-column">
						<table border="0" width="40%">
							<?php
							$sqlDataWarga = "SELECT * FROM tbl_data_Warga WHERE idUser = '$id'";
							$queryDataWarga = mysqli_query($db, $sqlDataWarga);
							$dataWarga = mysqli_fetch_array($queryDataWarga);
							$sqlDataKeluarga = "SELECT * FROM tbl_data_keluarga WHERE idKeluarga = '$id'";
							$queryDataKeluarga = mysqli_query($db, $sqlDataKeluarga);
							$dataKeluarga = mysqli_fetch_array($queryDataKeluarga);
							?>
							<form action="confCatatanKeluarga.php" method="post">

								<tr>
									<td width="60%">Catatan Keluarga Dari</td>
									<td>:</td>
									<td><?php echo $dataWarga['kepalaRumahTangga'] ?></td>
									<input type="text" name="kepalaRumahTangga" value="<?php echo $dataWarga['kepalaRumahTangga'] ?>" hidden>
								</tr>
								<tr>
									<td width="60%">Anggota Kelompok Dasawisma</td>
									<td>:</td>
									<td><?php echo $dataWarga['dasaWisma'] ?></td>
									<input type="text" name="dasaWisma" value="<?php echo $dataWarga['dasaWisma'] ?>" hidden>
								</tr>
								<tr>
									<td width="60%">Tahun</td>
									<td>:</td>
									<td><?php echo date('Y') ?></td>
									<input type="text" name="tahun" value="<?php echo date('Y') ?>" hidden>
								</tr>
						</table>

						<table border="0" width="40%">
							<tr>
								<td width="50%">Kriteria Rumah</td>
								<td>:</td>
								<td><?php echo $dataKeluarga['kriteriaRumah'] ?></td>
								<input type="text" name="kriteriaRumah" value="<?php echo $dataKeluarga['kriteriaRumah'] ?>" hidden>
							</tr>
							<tr>
								<td width="50%">Jamban Keluarga</td>
								<td>:</td>
								<td><?php echo $dataKeluarga['jambanKeluarga'] ?></td>
								<input type="text" name="jambanKeluarga" value="<?php echo $dataKeluarga['jambanKeluarga'] ?>" hidden>
							</tr>
							<tr>
								<td width="50%">Tempat Sampah</td>
								<td>:</td>
								<td><?php echo $dataKeluarga['memilikiTPS'] ?></td>
								<input type="text" name="memilikiTPS" value="<?php echo $dataKeluarga['memilikiTPS'] ?>" hidden>
							</tr>
						</table>
					</div>

					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary mt-4" data-bs-toggle="modal" data-bs-target="#exampleModal">
						Tambah Data
					</button>

					<div class="tabel table-responsive mb-4 mt-4">
						<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
							<thead class="table-warning">
								<tr>
									<th scope="col" class="text-center" rowspan="2">NO</th>
									<th scope="col" class="text-center" rowspan="2">NAMA ANGGOTA KELUARGA</th>
									<th scope="col" class="text-center" rowspan="2">STATUS PERKAWINAN</th>
									<th scope="col" class="text-center" rowspan="2">L/P</th>
									<th scope="col" class="text-center" rowspan="2">TEMPAT LAHIR</th>
									<th scope="col" class="text-center" rowspan="2">TGL/BL/TH LAHIR/UMUR</th>
									<th scope="col" class="text-center" rowspan="2">AGAMA</th>
									<th scope="col" class="text-center" rowspan="2">PENDIDIKAN</th>
									<th scope="col" class="text-center" rowspan="2">PEKERJAAN</th>
									<th scope="col" class="text-center" rowspan="2">BERKEBUTUHAN KHUSUS</th>
									<th scope="col" class="text-center" colspan="8">KEGIATAN YANG DIIKUTI</th>
									<th scope="col" class="text-center" rowspan="2">KET</th>
								</tr>
								<tr>
									<th>PENGHAYATAN DAN PENGAMALAN PANCASILA</th>
									<th>GOTONG ROYONG</th>
									<th>PENDIDIKAN DAN KETRAMPILAN</th>
									<th>PENGEMBANGAN KEHIDUPAN BERKOPERASI</th>
									<th>PANGAN</th>
									<th>SANDANG</th>
									<th>KESEHATAN</th>
									<th>PERENCANAAN SEHAT</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>2</td>
									<td>3</td>
									<td>4</td>
									<td>5</td>
									<td>6</td>
									<td>7</td>
									<td>8</td>
									<td>9</td>
									<td>10</td>
									<td>11</td>
									<td>12</td>
									<td>13</td>
									<td>14</td>
									<td>15</td>
									<td>16</td>
									<td>17</td>
									<td>18</td>
									<td>19</td>
								</tr>
								<?php
								$sqlListCatatan = "SELECT * FROM tbl_list_catatan_keluarga WHERE idCatatanKeluarga = '$id'";
								$queryListCatatan = mysqli_query($db, $sqlListCatatan);
								$no = 1;
								if (mysqli_num_rows($queryListCatatan) < 1) {
								?>
									<tr>
										<td colspan="19" class="text-center">TIDAK ADA CATATAN KELUARGA</td>
									</tr>
									<?php
								} else {
									while ($dataList = mysqli_fetch_array($queryListCatatan)) {
									?>
										<tr>
											<td><?php echo $no ?></td>
											<td><?php echo $dataList['anggotaKeluarga'] ?></td>
											<td><?php echo $dataList['statusKawinAnggota'] ?></td>
											<td><?php echo $dataList['jenisKelaminAnggota'] ?></td>
											<td><?php echo $dataList['tempatLahirAnggota'] ?></td>
											<td><?php echo $dataList['tanggalLahirAnggota'] ?></td>
											<td><?php echo $dataList['agamaAnggota'] ?></td>
											<td><?php echo $dataList['pendidikanAnggota'] ?></td>
											<td><?php echo $dataList['pekerjaanAnggota'] ?></td>
											<td><?php echo $dataList['berkebutuhanKhususAnggota'] ?></td>
											<td><?php echo $dataList['pengamalanPancasila'] ?></td>
											<td><?php echo $dataList['gotongRoyong'] ?></td>
											<td><?php echo $dataList['pendidikanKeterampilan'] ?></td>
											<td><?php echo $dataList['kehidupanBerkoperasi'] ?></td>
											<td><?php echo $dataList['pangan'] ?></td>
											<td><?php echo $dataList['sandang'] ?></td>
											<td><?php echo $dataList['kesehatan'] ?></td>
											<td><?php echo $dataList['perencanaanKesehatan'] ?></td>
											<td><?php echo $dataList['keterangan'] ?></td>
										</tr>
								<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>

					<div class="tombol text-end mt-2">
						<button class="btn btn-primary" type="submit" role="button" name="update" value="update"><img src="../../../assets/icon/icon-upload.png"> Simpan</button>
					</div>
					</form>

					<!-- Modal -->
					<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
									<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
								</div>
								<div class="modal-body">
									<form action="confCatatanKeluarga.php" method="post">
										<div class="mb-3">
											<label for="nama" class="form-label">Nama Anggota Keluarga</label>
											<input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Anggota Keluarga" name="anggotaKeluarga">
										</div>
										<div class="mb-3">
											<label for="status" class="form-label">Status Perkawinan</label>
											<select class="form-select" aria-label="Default select example" name="statusKawinAnggota">
												<option selected disabled>Pilih Status Perkawinan</option>
												<option value="Menikah">Menikah</option>
												<option value="Lajang">Lajang</option>
												<option value="Cerai Mati">Cerai Mati</option>
												<option value="Cerai Hidup">Cerai Hidup</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="status" class="form-label">Jenis Kelamin</label>
											<select class="form-select" aria-label="Default select example" name="jenisKelaminAnggota">
												<option selected disabled>Pilih Jenis Kelamin</option>
												<option value="Laki-laki">Laki-laki</option>
												<option value="Perempuan">Perempuan</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="tempat" class="form-label">Tempat Lahir</label>
											<input type="text" class="form-control" id="tempat" placeholder="Masukkan Tempat Lahir" name="tempatLahirAnggota">
										</div>
										<div class="mb-3">
											<label for="tanggal" class="form-label">Tanggal Lahir</label>
											<input type="date" class="form-control" id="tanggal" placeholder="Masukkan Tanggal Lahir" name="tanggalLahirAnggota">
										</div>
										<div class="mb-3">
											<label for="agama" class="form-label">Agama</label>
											<input type="text" class="form-control" id="agama" placeholder="Masukkan Agama" name="agamaAnggota">
										</div>
										<div class="mb-3">
											<label for="status" class="form-label">Pendidikan</label>
											<select class="form-select" aria-label="Default select example" name="pendidikanAnggota">
												<option selected disabled>Pilih Pendidikan</option>
												<option value="Tamat SD">Tamat SD</option>
												<option value="SD/MI">SD/MI</option>
												<option value="SMP/Sederajat">SMP/Sederajat</option>
												<option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
												<option value="Diploma">Diploma</option>
												<option value="S1">S1</option>
												<option value="S2">S2</option>
												<option value="S3">S3</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="status" class="form-label">Pekerjaan</label>
											<select class="form-select" aria-label="Default select example" name="pekerjaanAnggota">
												<option selected disabled>Pilih Pekerjaan</option>
												<option value="Petani">Petani</option>
												<option value="Pedagang">Pedagang</option>
												<option value="Swasta">Swasta</option>
												<option value="Wirausaha">Wirausaha</option>
												<option value="PNS">PNS</option>
												<option value="TNI/Polri">TNI/Polri</option>
												<option value="Dll">Dll</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="kebutuhan" class="form-label">Berkebutuhuan Khusus</label>
											<select class="form-select" aria-label="Default select example" name="berkebutuhanKhususAnggota">
												<option selected disabled>Apakah Berkebutuhan Khusus</option>
												<option value="Ya">Ya</option>
												<option value="Tidak">Tidak</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="pancasila" class="form-label">Penghayatan dan Pengamalan Pancasila</label>
											<select class="form-select" aria-label="Default select example" name="pengamalanPancasila">
												<option selected disabled>Penghayatan dan Pemgalan Pancasila</option>
												<option value="Ya">Ya</option>
												<option value="Tidak">Tidak</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="gotong" class="form-label">Gotong Royong</label>
											<select class="form-select" aria-label="Default select example" name="gotongRoyong">
												<option selected disabled>Gotong Royong</option>
												<option value="Ya">Ya</option>
												<option value="Tidak">Tidak</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="ketrampilan" class="form-label">Pendidikan dan Ketrampilan</label>
											<select class="form-select" aria-label="Default select example" name="pendidikanKeterampilan">
												<option selected disabled>Pendidikan dan Ketrampilan</option>
												<option value="Ya">Ya</option>
												<option value="Tidak">Tidak</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="Pengembangan" class="form-label">Pengembangan Kehidupan Berkoperasi</label>
											<input type="text" class="form-control" id="tempat" placeholder="Masukkan Kegiatan yang diikuti Anggota Keluarga" name="kehidupanBerkoperasi">
										</div>
										<div class="mb-3">
											<label for="pangan" class="form-label">Pangan</label>
											<select class="form-select" aria-label="Default select example" name="pangan">
												<option selected disabled>Pangan</option>
												<option value="Ya">Ya</option>
												<option value="Tidak">Tidak</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="sandang" class="form-label">Sandang</label>
											<select class="form-select" aria-label="Default select example" name="sandang">
												<option selected disabled>Sandang</option>
												<option value="Ya">Ya</option>
												<option value="Tidak">Tidak</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="kesehatan" class="form-label">Kesehatan</label>
											<select class="form-select" aria-label="Default select example" name="kesehatan">
												<option selected disabled>Kesehatan</option>
												<option value="Kegiatan Posyandu Balita/Lansia">Kegiatan Posyandu Balita/Lansia</option>
												<option value="PHBS">PHBS</option>
												<option value="Kegiatan Lainnya">Kegiatan Lainnya</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="perencanaan" class="form-label">Perencanaan Kesehatan</label>
											<select class="form-select" aria-label="Default select example" name="perencanaanKesehatan">
												<option selected disabled>Perencanaan Kesehatan</option>
												<option value="Ya">Ya</option>
												<option value="Tidak">Tidak</option>
											</select>
										</div>
										<div class="mb-3">
											<label for="ket" class="form-label">Keterangan</label>
											<textarea class="form-control" id="ket" rows="3" name="keterangan"></textarea>
										</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
									<button type="submit" class="btn btn-primary" name="submit" value="submit">Simpan</button>
								</div>
								</form>
							</div>
						</div>
					</div>
					<div class="keterangan">
						<p class="ket">KETERANGAN:</p>

						<div class="isi">
							<p>Kolom 2 : Diisi dengan nama seluruh anggota keluarga yang ada dalam rumah tangga </p>
							<p>Kolom 3 : Diisi Menikah, Lajang, Cerai, Mati, Cerai Hidup</p>
							<p>Kolom 4 : Diisi Dengan Jenis Kelamin Laki-laki/Perempuan</p>
							<p>Kolom 10 : Diisi Dengan kondisi anggota keluarga (catat mental, cacat fisik, dsb)</p>
							<p>Kolom 11-18 : di isi dengan jenis kegiatan yang diikuti oleh masing-masing anggota keluarga</p>
							<p>Kolom 11 : di isi dengan kegiatan keagamaan, PKBN, Pola Asuh, Pencegahan KDRT, Pencegahan Trafficking, Narkoba, Pencegahan kejahatan seksual yang ikuti oleh anggota keluarga</p>
							<p>Kolom 12 : Di isi dengan kegiatan kerjabakti, jimpitan, arisan, rukun kematian, bakti sosial, dsb yang diikuti oleh anggota keluarga</p>
							<p>Kolom 13 : Di isi dengan kegiatan BKB, PAUD Sejenis, Paket ABC, KF yang diikuti oleh anggota keluarga</p>
							<p>Kolom 14 : Di isi dengan kegiatan UP2K, KOPERASI, yang diikuti oleh anggota keluarga</p>
							<p>Kolom 15 : di isi dengan jenis makanan pokok ( beras dan non beras/pangan lokal) anggota keluarga dan pemanfaatan halaman pekarangan yang dilakukan oleh anggota keluarga</p>
							<p>Kolom 16 : Di isi dengan kegiatan usaha yang berkaitan dengan usaha sandang</p>
							<p>Kolom 17 : Di isi dengan anggota keluarga yang mengikuti kegiatan posyandu Balita/Lansia dan PHBS, dan kegiatan kesehatan lainnya.</p>
							<p>Kolom 18 : Di isi dengan anggota keluarga yang mengikuti Program KB menjadi Peserta BPJS Kesehatan, menabung untuk masa depan keluarga</p>
							<p>Kolom 19 : Di isi dengan hal-hal yang belum tercantum dalam kolom-kolom sebelumnya</p>
						</div>
					</div>
				</div>
					<?php
				}
				?>

			</div>
		</div>

	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	// $(document).ready(function(){
	// 	$('[data-bs-toggle="popover"]').popover();   
	// });
	// const popover = new bootstrap.Popover('.popover-dismiss', {
	// 	trigger: 'focus'
	// })
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

</html>