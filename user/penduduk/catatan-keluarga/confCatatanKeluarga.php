<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'user') {
	header("location:../../../login-user");
	exit;
}
$id = $_SESSION['id'];

if(isset($_POST['submit'])){
    $idCatatanKeluarga = $_SESSION['id'];
    $anggotaKeluarga = $_POST['anggotaKeluarga'];
    $statusKawinAnggota = $_POST['statusKawinAnggota'];
    $jenisKelaminAnggota = $_POST['jenisKelaminAnggota'];
    $tempatLahirAnggota = $_POST['tempatLahirAnggota'];
    $tanggalLahirAnggota = $_POST['tanggalLahirAnggota'];
    $agamaAnggota = $_POST['agamaAnggota'];
    $pendidikanAnggota = $_POST['pendidikanAnggota'];
    $pekerjaanAnggota = $_POST['pekerjaanAnggota'];
    $berkebutuhanKhususAnggota = $_POST['berkebutuhanKhususAnggota'];
    $pengamalanPancasila = $_POST['pengamalanPancasila'];
    $gotongRoyong = $_POST['gotongRoyong'];
    $pendidikanKeterampilan = $_POST['pendidikanKeterampilan'];
    $kehidupanBerkoperasi = $_POST['kehidupanBerkoperasi'];
    $pangan = $_POST['pangan'];
    $sandang = $_POST['sandang'];
    $kesehatan = $_POST['kesehatan'];
    $perencanaanKesehatan = $_POST['perencanaanKesehatan'];
    $keterangan = $_POST['keterangan'];

    $sql = "INSERT INTO tbl_list_catatan_keluarga (idCatatanKeluarga, anggotaKeluarga, statusKawinAnggota,
    jenisKelaminAnggota, tempatLahirAnggota, tanggalLahirAnggota, agamaAnggota, pendidikanAnggota, pekerjaanAnggota,
    berkebutuhanKhususAnggota, pengamalanPancasila, gotongRoyong, pendidikanKeterampilan, kehidupanBerkoperasi,
    pangan, sandang, kesehatan, perencanaanKesehatan, keterangan) VALUE ('$idCatatanKeluarga', '$anggotaKeluarga', '$statusKawinAnggota',
    '$jenisKelaminAnggota', '$tempatLahirAnggota', '$tanggalLahirAnggota', '$agamaAnggota', '$pendidikanAnggota', '$pekerjaanAnggota',
    '$berkebutuhanKhususAnggota', '$pengamalanPancasila', '$gotongRoyong', '$pendidikanKeterampilan', '$kehidupanBerkoperasi',
    '$pangan', '$sandang', '$kesehatan', '$perencanaanKesehatan', '$keterangan')";
    $query = mysqli_query($db, $sql);

    if($query){
        echo "
            <script>
                alert('DATA BERHASIL DI SIMPAN');
                document.location.href = '../catatan-keluarga';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI SIMPAN');
                document.location.href = '../catatan-keluarga';
            </script>
        ";
    }
}

if(isset($_POST['submitAll'])){
    $idCatatanKeluarga = $_SESSION['id'];
    $kepalaRumahTangga = $_POST['kepalaRumahTangga'];
    $dasaWisma = $_POST['dasaWisma'];
    $tahun = $_POST['tahun'];
    $kriteriaRumah = $_POST['kriteriaRumah'];
    $jambanKeluarga = $_POST['jambanKeluarga'];
    $memilikiTPS = $_POST['memilikiTPS'];

    $sql = "INSERT INTO tbl_catatan_keluarga (idCatatanKeluarga, kepalaRumahTangga, dasaWisma, tahun, kriteriaRumah, jambanKeluarga, memilikiTPS)
    VALUE ('$idCatatanKeluarga', '$kepalaRumahTangga', '$dasaWisma', '$tahun', '$kriteriaRumah', '$jambanKeluarga', '$memilikiTPS')";

    $query = mysqli_query($db,$sql);

    if($query){
        echo "
            <script>
                alert('DATA BERHASIL DI SIMPAN');
                document.location.href = '../catatan-keluarga';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI SIMPAN');
                document.location.href = '../catatan-keluarga';
            </script>
        ";
    }
}

if(isset($_POST['update'])){
    $idCatatanKeluarga = $_SESSION['id'];
    $kepalaRumahTangga = $_POST['kepalaRumahTangga'];
    $dasaWisma = $_POST['dasaWisma'];
    $tahun = $_POST['tahun'];
    $kriteriaRumah = $_POST['kriteriaRumah'];
    $jambanKeluarga = $_POST['jambanKeluarga'];
    $memilikiTPS = $_POST['memilikiTPS'];

    $sql = "UPDATE tbl_catatan_keluarga SET kepalaRumahTangga='$kepalaRumahTangga', dasaWisma='$dasaWisma', tahun='$tahun', kriteriaRumah='$kriteriaRumah', jambanKeluarga='$jambanKeluarga', memilikiTPS='$memilikiTPS' WHERE idCatatanKeluarga = '$idCatatanKeluarga'";

    $query = mysqli_query($db,$sql);

    if($query){
        echo "
            <script>
                alert('DATA BERHASIL DI UPDATE');
                document.location.href = '../catatan-keluarga';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI UPDATE');
                document.location.href = '../catatan-keluarga';
            </script>
        ";
    }
}
?>