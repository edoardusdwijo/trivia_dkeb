<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'user') {
	header("location:../../../login-user");
	exit;
}
$id = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/datawarga-user.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../assets/image/logo.jpeg">
	<title>Input Data Warga-User</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../../">Home</a>
		<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a class="active" href="./">Input Data Warga</a>
			<a href="../data-keluarga/">Input Data Keluarga</a>
			<a href="../catatan-keluarga/">Input Catatan Keluarga</a>
		</div>
		<a href="../../cetak/">Cetak</a>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../../">Home</a>
				<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a class="active" href="./">Input Data Warga</a>
					<a href="../data-keluarga/">Input Data Keluarga</a>
					<a href="../catatan-keluarga/">Input Catatan Keluarga</a>
				</div>
				<a href="../../cetak/">Cetak</a>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul text-center">
			<p>DATA WARGA TP-PKK</p>
		</div>

		<div class="form mb-4">
			<div class="card">
				<!-- <div class="card-header">
					Featured
				</div> -->
				<?php
				$sqlData = "SELECT * FROM tbl_data_warga WHERE idUser='$id'";
				$queryData = mysqli_query($db, $sqlData);
				$jumlahData = mysqli_num_rows($queryData);
				$dataUser = mysqli_fetch_array($queryData);
				if ($jumlahData < 1) {
					?>
					<div class="card-body">
						<form action="confDataWarga.php" method="post">
							<div class="mb-3">
								<label for="dasaWisma" class="form-label">Dasa Wisma</label>
								<input type="text" class="form-control" id="dasaWisma" name="dasaWisma" placeholder="Masukkan Dasa Wisma" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Nama Kepala Rumah Tangga</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="kepalaRumahTangga" placeholder="Masukkan Nama Kepala Rumah Tangga" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">No. Registrasi</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="noRegist" placeholder="Masukkan No Registrasi" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">No. KTP/NIK</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="nik" placeholder="Masukkan No KTP/NIK" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Nama</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="nama" placeholder="Masukkan Nama" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Jabatan</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="jabatan" placeholder="Masukkan Jabatan">
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">RT</label>
								<input type="number" class="form-control" id="exampleFormControlInput1" name="rt" placeholder="Masukkan No RT" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">RW</label>
								<input type="number" class="form-control" id="exampleFormControlInput1" name="rw" placeholder="Masukkan No RW" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Jenis Kelamin:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jk" id="jk" value="Laki-laki" checked>
									<label class="form-check-label" for="inlineRadio1">Laki-laki</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jk" id="jk" value="Perempuan">
									<label class="form-check-label" for="inlineRadio1">Perempuan</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Tempat Lahir</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="tempatLahir" placeholder="Masukkan Tempat Lahir" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Tgl Lahir / Umur</label>
								<div class="row">
									<div class="col">
										<input type="date" class="form-control" placeholder="Masukkan Tanggal Lahir" name="tglLahir" aria-label="First name" required>
									</div>
									<div class="col">
										<input type="text" class="form-control" placeholder="Masukkan Umur" name="umur" aria-label="Last name" required>
									</div>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Status Perkawinan:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKawin" id="statusKawin" value="Menikah" checked>
									<label class="form-check-label" for="inlineRadio1">Menikah</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKawin" id="statusKawin" value="Lajang">
									<label class="form-check-label" for="inlineRadio1">Lajang</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKawin" id="statusKawin" value="Janda">
									<label class="form-check-label" for="inlineRadio1">Janda</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKawin" id="statusKawin" value="Duda">
									<label class="form-check-label" for="inlineRadio1">Duda</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Status Dalam Keluarga:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKeluarga" id="statusKeluarga" value="Kepala Rumah Tangga" checked>
									<label class="form-check-label" for="inlineRadio1">Kepala Rumah Tangga</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKeluarga" id="statusKeluarga" value="KK atau anggota keluarga">
									<label class="form-check-label" for="inlineRadio1">KK atau anggota keluarga</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Agama:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Islam" checked>
									<label class="form-check-label" for="inlineRadio1">Islam</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Kristen">
									<label class="form-check-label" for="inlineRadio1">Kristen</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Katholik">
									<label class="form-check-label" for="inlineRadio1">Katholik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Hindu">
									<label class="form-check-label" for="inlineRadio1">Hindu</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Budha">
									<label class="form-check-label" for="inlineRadio1">Budha</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Konghucu">
									<label class="form-check-label" for="inlineRadio1">Konghucu</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Lain-lain">
									<label class="form-check-label" for="inlineRadio1">Lain-lain</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Alamat</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="alamat" placeholder="Masukkan Alamat" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Pendidikan:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="Tidak tamat SD">
									<label class="form-check-label" for="inlineRadio1">Tidak tamat SD</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="SD/MI">
									<label class="form-check-label" for="inlineRadio1">SD/MI</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="SMP/Sederajat">
									<label class="form-check-label" for="inlineRadio1">SMP/Sederajat</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="SMU/SMK/Sederajat">
									<label class="form-check-label" for="inlineRadio1">SMU/SMK/Sederajat</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="Diploma">
									<label class="form-check-label" for="inlineRadio1">Diploma</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="S1">
									<label class="form-check-label" for="inlineRadio1">S1</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="S2">
									<label class="form-check-label" for="inlineRadio1">S2</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="S3">
									<label class="form-check-label" for="inlineRadio1">S3</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Pekerjaan:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Petani">
									<label class="form-check-label" for="inlineRadio1">Petani</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Pedagang">
									<label class="form-check-label" for="inlineRadio1">Pedagang</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Swasta">
									<label class="form-check-label" for="inlineRadio1">Swasta</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Wirausaha">
									<label class="form-check-label" for="inlineRadio1">Wirausaha</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="PNS">
									<label class="form-check-label" for="inlineRadio1">PNS</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="TNI/Polri">
									<label class="form-check-label" for="inlineRadio1">TNI/Polri</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Lain-lain">
									<label class="form-check-label" for="inlineRadio1">Lain-lain</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Akseptor KB</label>
								<div class="row">
									<div class="col-2">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="akseptorKB" id="akseptorKB" value="Ya">
											<label class="form-check-label" for="inlineRadio1">Ya</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="akseptorKB" id="akseptorKB" value="Tidak">
											<label class="form-check-label" for="inlineRadio1">Tidak</label>
										</div>
									</div>
									<div class="col-10">
										<input type="text" class="form-control" placeholder="Jenis Akseptor KB" name="jenisAkseptor" aria-label="Last name">
									</div>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Aktif dalam kegiatan Posyandu</label>
								<div class="row">
									<div class="col-2">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="aktifPosyandu" id="aktifPosyandu" value="Ya">
											<label class="form-check-label" for="inlineRadio1">Ya</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="aktifPosyandu" id="aktifPosyandu" value="Tidak">
											<label class="form-check-label" for="inlineRadio1">Tidak</label>
										</div>
									</div>
									<div class="col-10">
										<input type="number" class="form-control" placeholder="Frekuensi/Volume" name="frekVolume" aria-label="Last name">
									</div>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Mengikuti Program Bina Keluarga Balita:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="binaKeluargaBalita" id="binaKeluargaBalita" value="Ya">
									<label class="form-check-label" for="inlineRadio1">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="binaKeluargaBalita" id="binaKeluargaBalita" value="Tidak">
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Memiliki Tabungan (diisi khusus KK):</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="memilikiTabungan" id="memilikiTabungan" value="Ya">
									<label class="form-check-label" for="inlineRadio1">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="memilikiTabungan" id="memilikiTabungan" value="Tidak">
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Mengikuti Kelompok Belajar:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kelompokBelajar" id="kelompokBelajar" value="Paket A">
									<label class="form-check-label" for="inlineRadio1">Paket A</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kelompokBelajar" id="kelompokBelajar" value="Paket B">
									<label class="form-check-label" for="inlineRadio1">Paket B</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kelompokBelajar" id="kelompokBelajar" value="Paket C">
									<label class="form-check-label" for="inlineRadio1">Paket C</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kelompokBelajar" id="kelompokBelajar" value="Tidak Mengikuti Kelompok Belajar">
									<label class="form-check-label" for="inlineRadio1">Tidak Mengikuti Kelompok Belajar</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Mengikuti PAUD:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="mengikutiPaud" id="mengikutiPaud" value="Ya">
									<label class="form-check-label" for="inlineRadio1">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="mengikutiPaud" id="mengikutiPaud" value="Tidak">
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Ikut dalam kegiatan koperasi</label>
								<div class="row">
									<div class="col-2">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="kegiatanKoperasi" id="kegiatanKoperasi" value="Ya">
											<label class="form-check-label" for="inlineRadio1">Ya</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="kegiatanKoperasi" id="kegiatanKoperasi" value="Tidak">
											<label class="form-check-label" for="inlineRadio1">Tidak</label>
										</div>
									</div>
									<div class="col-10">
										<input type="text" class="form-control" placeholder="Jenis Koperasi" name="jenisKoperasi" aria-label="Last name">
									</div>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Berkebutuhan Khusus:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kebutuhanKhusus" id="kebutuhanKhusus" value="Fisik">
									<label class="form-check-label" for="inlineRadio1">Fisik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kebutuhanKhusus" id="kebutuhanKhusus" value="Non Fisik">
									<label class="form-check-label" for="inlineRadio1">Non Fisik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kebutuhanKhusus" id="kebutuhanKhusus" value="Tidak">
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</div>

							<div class="tombol text-end">
								<button class="btn btn-primary" type="submit" role="button" name="submit" value="submit"><img src="../../../assets/icon/icon-upload.png"> Simpan</button>
							</div>
						</form>
					</div>
					<?php
				} else {
					?>
					<div class="card-body">
						<form action="confDataWarga.php" method="post">
							<div class="mb-3">
								<label for="dasaWisma" class="form-label">Dasa Wisma</label>
								<input type="text" class="form-control" id="dasaWisma" name="dasaWisma" placeholder="Masukkan Dasa Wisma" value="<?php echo $dataUser['dasaWisma'] ?>" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Nama Kepala Rumah Tangga</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="kepalaRumahTangga" placeholder="Masukkan Nama Kepala Rumah Tangga" value="<?php echo $dataUser['kepalaRumahTangga'] ?>" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">No. Registrasi</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="noRegist" placeholder="Masukkan No Registrasi" value="<?php echo $dataUser['noRegist'] ?>" readonly>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">No. KTP/NIK</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="nik" placeholder="Masukkan No KTP/NIK" value="<?php echo $dataUser['nik'] ?>" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Nama</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="nama" placeholder="Masukkan Nama" value="<?php echo $dataUser['nama'] ?>" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Jabatan</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="jabatan" placeholder="Masukkan Nama" value="<?php echo $dataUser['jabatan'] ?>">
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">RT</label>
								<input type="number" class="form-control" id="exampleFormControlInput1" name="rt" placeholder="Masukkan No RT" value="<?php echo $dataUser['rt'] ?>">
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">RW</label>
								<input type="number" class="form-control" id="exampleFormControlInput1" name="rw" placeholder="Masukkan No RW" value="<?php echo $dataUser['rw'] ?>">
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Jenis Kelamin:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jk" id="jk" value="Laki-laki" <?php if($dataUser['jk']=='Laki-laki'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Laki-laki</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="jk" id="jk" value="Perempuan" <?php if($dataUser['jk']=='Perempuan'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Perempuan</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Tempat Lahir</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="tempatLahir" placeholder="Masukkan Tempat Lahir" value="<?php echo $dataUser['tempatLahir'] ?>" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Tgl Lahir / Umur</label>
								<div class="row">
									<div class="col">
										<input type="date" class="form-control" placeholder="Masukkan Tanggal Lahir" name="tglLahir" aria-label="First name" value="<?php echo $dataUser['tglLahir'] ?>" required>
									</div>
									<div class="col">
										<input type="text" class="form-control" placeholder="Masukkan Umur" name="umur" aria-label="Last name" value="<?php echo $dataUser['umur'] ?>" required>
									</div>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Status Perkawinan:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKawin" id="statusKawin" value="Menikah" <?php if($dataUser['statusKawin']=='Menikah'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Menikah</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKawin" id="statusKawin" value="Lajang" <?php if($dataUser['statusKawin']=='Lajang'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Lajang</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKawin" id="statusKawin" value="Janda" <?php if($dataUser['statusKawin']=='Janda'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Janda</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKawin" id="statusKawin" value="Duda" <?php if($dataUser['statusKawin']=='Duda'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Duda</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Status Dalam Keluarga:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKeluarga" id="statusKeluarga" value="Kepala Rumah Tangga" <?php if($dataUser['statusKeluarga']=='Kepala Rumah Tangga'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Kepala Rumah Tangga</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="statusKeluarga" id="statusKeluarga" value="KK atau anggota keluarga" <?php if($dataUser['statusKeluarga']=='KK atau anggota keluarga'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">KK atau anggota keluarga</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Agama:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Islam" <?php if($dataUser['agama']=='Islam'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Islam</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Kristen" <?php if($dataUser['agama']=='Kristen'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Kristen</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Katholik" <?php if($dataUser['agama']=='Katholik'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Katholik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Hindu" <?php if($dataUser['agama']=='Hindu'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Hindu</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Budha" <?php if($dataUser['agama']=='Budha'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Budha</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Konghucu" <?php if($dataUser['agama']=='Konghucu'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Konghucu</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agama" id="agama" value="Lain-lain" <?php if($dataUser['agama']=='Lain-lain'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Lain-lain</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Alamat</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" name="alamat" placeholder="Masukkan Alamat" value="<?php echo $dataUser['alamat'] ?>" required>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Pendidikan:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="Tidak tamat SD" <?php if($dataUser['pendidikan']=='Tidak tamat SD'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Tidak tamat SD</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="SD/MI" <?php if($dataUser['pendidikan']=='SD/MI'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">SD/MI</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="SMP/Sederajat" <?php if($dataUser['pendidikan']=='SMP/Sederajat'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">SMP/Sederajat</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="SMU/SMK/Sederajat" <?php if($dataUser['pendidikan']=='SMU/SMK/Sederajat'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">SMU/SMK/Sederajat</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="Diploma" <?php if($dataUser['pendidikan']=='Diploma'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Diploma</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="S1" <?php if($dataUser['pendidikan']=='S1'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">S1</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="S2" <?php if($dataUser['pendidikan']=='S2'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">S2</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pendidikan" id="pendidikan" value="S3" <?php if($dataUser['pendidikan']=='S3'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">S3</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Pekerjaan:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Petani" <?php if($dataUser['pekerjaan']=='Petani'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Petani</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Pedagang" <?php if($dataUser['pekerjaan']=='Pedagang'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Pedagang</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Swasta" <?php if($dataUser['pekerjaan']=='Swasta'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Swasta</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Wirausaha" <?php if($dataUser['pekerjaan']=='Wirausaha'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Wirausaha</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="PNS" <?php if($dataUser['pekerjaan']=='PNS'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">PNS</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="TNI/Polri" <?php if($dataUser['pekerjaan']=='TNI/Polri'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">TNI/Polri</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="pekerjaan" id="pekerjaan" value="Lain-lain" <?php if($dataUser['pekerjaan']=='Lain-lain'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Lain-lain</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Akseptor KB</label>
								<div class="row">
									<div class="col-2">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="akseptorKB" id="akseptorKB" value="Ya" <?php if($dataUser['akseptorKB']=='Ya'){ ?> checked <?php } ?>>
											<label class="form-check-label" for="inlineRadio1">Ya</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="akseptorKB" id="akseptorKB" value="Tidak" <?php if($dataUser['akseptorKB']=='Tidak'){ ?> checked <?php } ?>>
											<label class="form-check-label" for="inlineRadio1">Tidak</label>
										</div>
									</div>
									<div class="col-10">
										<input type="text" class="form-control" placeholder="Jenis Akseptor KB" name="jenisAkseptor" aria-label="Last name" value="<?php echo $dataUser['jenisAkseptor'] ?>">
									</div>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Aktif dalam kegiatan Posyandu</label>
								<div class="row">
									<div class="col-2">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="aktifPosyandu" id="aktifPosyandu" value="Ya" <?php if($dataUser['aktifPosyandu']=='Ya'){ ?> checked <?php } ?>>
											<label class="form-check-label" for="inlineRadio1">Ya</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="aktifPosyandu" id="aktifPosyandu" value="Tidak" <?php if($dataUser['aktifPosyandu']=='Tidak'){ ?> checked <?php } ?>>
											<label class="form-check-label" for="inlineRadio1">Tidak</label>
										</div>
									</div>
									<div class="col-10">
										<input type="number" class="form-control" placeholder="Frekuensi/Volume" name="frekVolume" aria-label="Last name" value="<?php echo $dataUser['frekVolume'] ?>">
									</div>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Mengikuti Program Bina Keluarga Balita:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="binaKeluargaBalita" id="binaKeluargaBalita" value="Ya" <?php if($dataUser['binaKeluargaBalita']=='Ya'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="binaKeluargaBalita" id="binaKeluargaBalita" value="Tidak" <?php if($dataUser['binaKeluargaBalita']=='Tidak'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Memiliki Tabungan (diisi khusus KK):</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="memilikiTabungan" id="memilikiTabungan" value="Ya" <?php if($dataUser['memilikiTabungan']=='Ya'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="memilikiTabungan" id="memilikiTabungan" value="Tidak" <?php if($dataUser['memilikiTabungan']=='Tidak'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Mengikuti Kelompok Belajar:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kelompokBelajar" id="kelompokBelajar" value="Paket A" <?php if($dataUser['kelompokBelajar']=='Paket A'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Paket A</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kelompokBelajar" id="kelompokBelajar" value="Paket B" <?php if($dataUser['kelompokBelajar']=='Paket B'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Paket B</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kelompokBelajar" id="kelompokBelajar" value="Paket C" <?php if($dataUser['kelompokBelajar']=='Paket C'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Paket C</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kelompokBelajar" id="kelompokBelajar" value="Tidak Mengikuti Kelompok Belajar" <?php if($dataUser['kelompokBelajar']=='Tidak Mengikuti Kelompok Belajar'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Tidak Mengikuti Kelompok Belajar</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Mengikuti PAUD:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="mengikutiPaud" id="mengikutiPaud" value="Ya" <?php if($dataUser['mengikutiPaud']=='Ya'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="mengikutiPaud" id="mengikutiPaud" value="Tidak" <?php if($dataUser['mengikutiPaud']=='Tidak'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Ikut dalam kegiatan koperasi</label>
								<div class="row">
									<div class="col-2">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="kegiatanKoperasi" id="kegiatanKoperasi" value="Ya" <?php if($dataUser['kegiatanKoperasi']=='Ya'){ ?> checked <?php } ?>>
											<label class="form-check-label" for="inlineRadio1">Ya</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="kegiatanKoperasi" id="kegiatanKoperasi" value="Tidak" <?php if($dataUser['kegiatanKoperasi']=='Tidak'){ ?> checked <?php } ?>>
											<label class="form-check-label" for="inlineRadio1">Tidak</label>
										</div>
									</div>
									<div class="col-10">
										<input type="text" class="form-control" placeholder="Jenis Koperasi" name="jenisKoperasi" aria-label="Last name" value="<?php echo $dataUser['jenisKoperasi'] ?>">
									</div>
								</div>
							</div>
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Berkebutuhan Khusus:</label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kebutuhanKhusus" id="kebutuhanKhusus" value="Fisik" <?php if($dataUser['kebutuhanKhusus']=='Fisik'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Fisik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kebutuhanKhusus" id="kebutuhanKhusus" value="Non Fisik" <?php if($dataUser['kebutuhanKhusus']=='Non Fisik'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Non Fisik</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="kebutuhanKhusus" id="kebutuhanKhusus" value="Tidak" <?php if($dataUser['kebutuhanKhusus']=='Tidak'){ ?> checked <?php } ?>>
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</div>

							<div class="tombol text-end">
								<button class="btn btn-primary" type="submit" role="button" name="update" value="update"><img src="../../../assets/icon/icon-upload.png"> Simpan</button>
							</div>
						</form>
					</div>
					<?php
				}
				?>
			</div>
		</div>

	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	// $(document).ready(function(){
	// 	$('[data-bs-toggle="popover"]').popover();   
	// });
	// const popover = new bootstrap.Popover('.popover-dismiss', {
	// 	trigger: 'focus'
	// })
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

</html>