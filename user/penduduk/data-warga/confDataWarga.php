<?php
include("../../../config.php");
session_start();

if(isset($_POST['submit'])){
    $noRegist = $_POST['noRegist'];
    $idUser = $_SESSION['id'];
    $dasaWisma = $_POST['dasaWisma'];
    $kepalaRumahTangga = $_POST['kepalaRumahTangga'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $jabatan = $_POST['jabatan'];
    $rt = $_POST['rt'];
    $rw = $_POST['rw'];
    $jk = $_POST['jk'];
    $tempatLahir = $_POST['tempatLahir'];
    $tglLahir = $_POST['tglLahir'];
    $umur = $_POST['umur'];
    $statusKawin = $_POST['statusKawin'];
    $statusKeluarga = $_POST['statusKeluarga'];
    $agama = $_POST['agama'];
    $alamat = $_POST['alamat'];
    $pendidikan = $_POST['pendidikan'];
    $pekerjaan = $_POST['pekerjaan'];
    $akseptorKB = $_POST['akseptorKB'];
    $jenisAkseptor = $_POST['jenisAkseptor'];
    $aktifPosyandu = $_POST['aktifPosyandu'];
    $frekVolume = $_POST['frekVolume'];
    $binaKeluargaBalita = $_POST['binaKeluargaBalita'];
    $memilikiTabungan = $_POST['memilikiTabungan'];
    $kelompokBelajar = $_POST['kelompokBelajar'];
    $mengikutiPaud = $_POST['mengikutiPaud'];
    $kegiatanKoperasi = $_POST['kegiatanKoperasi'];
    $jenisKoperasi = $_POST['jenisKoperasi'];
    $kebutuhanKhusus = $_POST['kebutuhanKhusus'];

    $sql = "INSERT INTO tbl_data_warga (noRegist, idUser, dasaWisma, kepalaRumahTangga, nik, nama, jabatan, rt, rw, jk, 
                                        tempatLahir, tglLahir, umur, statusKawin, statusKeluarga, 
                                        agama, alamat, pendidikan, pekerjaan, akseptorKB, jenisAkseptor,
                                        aktifPosyandu, frekVolume, binaKeluargaBalita, memilikiTabungan,
                                        kelompokBelajar, mengikutiPaud, kegiatanKoperasi, jenisKoperasi,
                                        kebutuhanKhusus)
                                        VALUE ('$noRegist', '$idUser', '$dasaWisma', '$kepalaRumahTangga', '$nik',
                                        '$nama', '$jabatan', '$rt', '$rw', '$jk', '$tempatLahir', '$tglLahir', '$umur', 
                                        '$statusKawin', '$statusKeluarga', '$agama', '$alamat', '$pendidikan',
                                        '$pekerjaan', '$akseptorKB', '$jenisAkseptor', '$aktifPosyandu', 
                                        '$frekVolume', '$binaKeluargaBalita', '$memilikiTabungan',
                                        '$kelompokBelajar', '$mengikutiPaud', '$kegiatanKoperasi', 
                                        '$jenisKoperasi', '$kebutuhanKhusus')";
    $query = mysqli_query($db, $sql);

    if($query){
        echo "
            <script>
                alert('DATA BERHASIL DI SIMPAN');
                document.location.href = '../data-warga';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI SIMPAN');
                document.location.href = '../data-warga';
            </script>
        ";
    }
}

if(isset($_POST['update'])){
    $noRegist = $_POST['noRegist'];
    $dasaWisma = $_POST['dasaWisma'];
    $kepalaRumahTangga = $_POST['kepalaRumahTangga'];
    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $jabatan = $_POST['jabatan'];
    $rt = $_POST['rt'];
    $rw = $_POST['rw'];
    $jk = $_POST['jk'];
    $tempatLahir = $_POST['tempatLahir'];
    $tglLahir = $_POST['tglLahir'];
    $umur = $_POST['umur'];
    $statusKawin = $_POST['statusKawin'];
    $statusKeluarga = $_POST['statusKeluarga'];
    $agama = $_POST['agama'];
    $alamat = $_POST['alamat'];
    $pendidikan = $_POST['pendidikan'];
    $pekerjaan = $_POST['pekerjaan'];
    $akseptorKB = $_POST['akseptorKB'];
    $jenisAkseptor = $_POST['jenisAkseptor'];
    $aktifPosyandu = $_POST['aktifPosyandu'];
    $frekVolume = $_POST['frekVolume'];
    $binaKeluargaBalita = $_POST['binaKeluargaBalita'];
    $memilikiTabungan = $_POST['memilikiTabungan'];
    $kelompokBelajar = $_POST['kelompokBelajar'];
    $mengikutiPaud = $_POST['mengikutiPaud'];
    $kegiatanKoperasi = $_POST['kegiatanKoperasi'];
    $jenisKoperasi = $_POST['jenisKoperasi'];
    $kebutuhanKhusus = $_POST['kebutuhanKhusus'];

    $sql = "UPDATE tbl_data_warga SET dasaWisma='$dasaWisma', kepalaRumahTangga='$kepalaRumahTangga',
                                    nik='$nik', nama='$nama', jabatan='$jabatan', rt='$rt', rw='$rw', jk='$jk',
                                    tempatLahir='$tempatLahir', tglLahir='$tglLahir', umur='$umur',
                                    statusKawin='$statusKawin', statusKeluarga='$statusKeluarga',
                                    agama='$agama', alamat='$alamat', pendidikan='$pendidikan', 
                                    pekerjaan='$pekerjaan', akseptorKB='$akseptorKB', jenisAkseptor='$jenisAkseptor',
                                    aktifPosyandu='$aktifPosyandu', frekVolume='$frekVolume', binaKeluargaBalita='$binaKeluargaBalita',
                                    memilikiTabungan='$memilikiTabungan', kelompokBelajar='$kelompokBelajar',
                                    mengikutiPaud='$mengikutiPaud', kegiatanKoperasi='$kegiatanKoperasi', 
                                    jenisKoperasi='$jenisKoperasi', kebutuhanKhusus='$kebutuhanKhusus'
                                    WHERE noRegist='$noRegist'";
    $query = mysqli_query($db, $sql);

    if($query){
        echo "
            <script>
                alert('DATA BERHASIL DI UPDATE');
                document.location.href = '../data-warga';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI UPDATE');
                document.location.href = '../data-warga';
            </script>
        ";
    }
}


?>