<!DOCTYPE html>
<html lang="en">

<head>
	<link rel="preload" as="image" href="../../assets/image/pilih-user.jpg">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<!-- <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'> -->
	<link rel="stylesheet" href="../../css/sign-up.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../assets/image/logo.jpeg">
	<title>Create Account</title>
</head>

<body>
	<!-- star navbar -->
	<nav class="navbar navbar-expand-lg bg-light">
		<div class="container">
			<a class="navbar-brand" href="../../">Trivia Dkeb</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start body -->
	<div class="form_login">
		<div class="container">
			<h2>Create New Account</h2>
			<div class="punya-akun text-center mb-4">
				<a href="../" class="punya-akun">ALREADY REGISTERED? LOGIN</a>
			</div>
			<form action="confSignUp.php" method="POST">
			<div class="mb-3">
					<label for="exampleInputEmail1" class="form-label">NAMA</label>
					<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Nama" name="nama" required>
				</div>
				<div class="mb-3">
					<label for="exampleInputEmail1" class="form-label">EMAIL</label>
					<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Email" name="email" required>
				</div>
				<div class="mb-3">
					<label for="exampleInputEmail1" class="form-label">SELECT USER TYPE</label>
					<select class="form-select" aria-label="Default select example" name="user_type" required>
						<option selected disabled value="">Pilih Tipe User</option>
						<option value="admin">Admin</option>
						<option value="user">User</option>
					</select>
				</div>
				<div class="mb-3">
					<label for="exampleInputPassword1" class="form-label">PASSWORD</label>
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Masukkan Password" name="password" required>
				</div>
				<div class="mb-3">
					<label for="exampleInputPassword1" class="form-label">KONFIRMASI PASSWORD</label>
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Masukkan Konfirmasi Password" name="confirmPassword" required>
				</div>
				<div class="mb-3">
					<label for="exampleInputPassword1" class="form-label">DATE OF BIRTH</label>
					<input type="date" class="form-control" id="exampleInputPassword1" placeholder="Masukkan Konfirmasi Password" name="tglLahir" required>
				</div>
				<div class="tombol text-center mb-2">
					<button class="btn btn-primary" type="submit" name="signUp" value="signUp">SIGN UP</button>
				</div>
			</form>
		</div>
	</div>
	<!-- end body -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

</html>