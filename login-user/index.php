<!DOCTYPE html>
<html lang="en">

<head>
	<link rel="preload" as="image" href="../assets/image/pilih-user.jpg">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<!-- <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'> -->
	<link rel="stylesheet" href="../css/login.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../assets/image/logo.jpeg">
	<title>Login</title>
</head>

<body>
	<!-- star navbar -->
	<nav class="navbar navbar-expand-lg bg-light">
		<div class="container">
			<a class="navbar-brand" href="../">Trivia Dkeb</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="./">Login</a>
					</li>
					<!-- <li class="nav-item">
						<a class="nav-link" href="#">Informasi</a>
					</li> -->
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start body -->
	<div class="form_login">
		<div class="container">
			<h2>LOGIN USER</h2>
			<p>SIGN IN TO CONTINUE</p>
			<form action="confLogin.php" method="POST">
				<div class="mb-3">
					<label for="exampleInputEmail1" class="form-label">EMAIL</label>
					<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Username" name="email">
				</div>
				<div class="mb-3">
					<label for="exampleInputPassword1" class="form-label">PASSWORD</label>
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Masukkan Password" name="password">
				</div>
				<div class="mb-3 lupa_passwd">
					<a href="lupa-password/">FORGOT PASSWORD?</a>
				</div>
				<div class="form-check mb-3">
					<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
					<label class="form-check-label" for="flexCheckDefault">
						REMEMBER ME
					</label>
				</div>
				<div class="tombol text-center mb-3">
					<button class="btn btn-primary" type="submit" value="signIn" name="signIn">SIGN IN</button>
				</div>
				<div class="sign-up mb-3 text-center">
					<a href="sign-up/">Don't have an account? Sign Up</a>
				</div>
			</form>
		</div>
	</div>
	<!-- end body -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</html>