<?php
include("../config.php");
session_start();

if (isset($_POST['signIn'])) {
    $email = $_POST['email'];
    $password = md5($_POST['password']);

    $sql = "SELECT * FROM tbl_user WHERE email = '$email'";
    $query = mysqli_query($db, $sql);

    if (mysqli_num_rows($query) > 0) {
        if($query){
            $data = mysqli_fetch_array($query);
            if ($data['password'] == $password && $data['tipeUser'] == 'user' && $data['statusAkun'] == 'aktif'){
                $_SESSION['id'] = $data['id'];
                $_SESSION['tipeUser'] = $data['tipeUser'];
                echo "
                    <script>
                        alert('ANDA BERHASIL LOGIN SEBAGAI USER, KLIK OK UNTUK MELANJUTKAN');
                        document.location.href = '../user/';
                    </script>
                ";
            }else if($data['password'] == $password && $data['statusAkun'] != 'aktif' && $data['tipeUser'] == 'user'){
                echo "
                    <script>
                        alert('MAAF AKUN ANDA BELUM DAPAT DI GUNAKAN, MOHON TUNGGU HINGGA 1x24 JAM DAN LOGIN KEMBALI');
                        document.location.href = '../login-user/';
                    </script>
                ";
            }else{
                echo "
                    <script>
                        alert('EMAIL ATAU PASSWORD YANG ANDA MASUKKAN SALAH, SILAHKAN LOGIN ULANG');
                        document.location.href = '../login-user/';
                    </script>
                ";
            }
        }
    } else {
        echo "
                <script>
                    alert('EMAIL ATAU PASSWORD YANG ANDA MASUKKAN SALAH, SILAHKAN LOGIN ULANG');
                    document.location.href = '../login-user/';
                </script>
            ";
    }
}
