<?php
include('../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../login-admin");
	exit;
}
$id = $_SESSION['id'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../css/home-user.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../assets/image/logo.jpeg">
	<title>Home-Admin</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../assets/image/logo.jpeg" alt="">
		</div>
		<a class="active" href="./">Home</a>
		<a href="manajemen-akun/">Manajemen Akun</a>
		<a href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a href="penduduk-view/">View</a>
		</div>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a class="active" href="./">Home</a>
				<a href="manajemen-akun/">Manajemen Akun</a>
				<a href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a href="penduduk-view/">View</a>
				</div>
				<a href="#cetak" data-bs-toggle="collapse">Cetak</a>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul">
			<p>Profil Desa</p>
		</div>

		<div class="card mb-4">
			<div class="row">
				<div class="col-md-4">
					<img src="../assets/image/profile-desa.jpg" class="img-fluid rounded-start" alt="Profil Desa">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<h4 class="card-title">Kegiatan Validasi Data Desa Wisma</h4>
						<p class="card-text">
							Kegiatan Dasa Wisma memiliki tujuan untuk peningkatan kesejahteraan dan kesehatan keluarga, yang nantinya dapat membantu program pemerintah, kecamatan dan kelurahan terutama dibidang kesehatan. pada gambar di samping memperlihatkan para anggota Dasa Wisma melakukan kegiatan validasi data untuk memastikan bahwa data tersebut telah sesuai kriteria yang ditetapkan dengan tujuan untuk memastikan bahwa data yang akan dimasukkan ke dalam basis data telah diketahui dan dapat dijelaskan sumber dan kebenaran datanya.
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row row-cols-1 row-cols-md-2 g-4 mt-4 mb-4">
			<div class="col-md-4">
				<div class="card">
					<img src="../assets/image/1.jpg" class="card-img-top" alt="...">
					<div class="card-body">
						<p class="card-text">Kegiatan Pokja 1 Pola Asuh Anak dan Remaja</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<img src="../assets/image/2.jpg" class="card-img-top" alt="...">
					<div class="card-body">
						<p class="card-text">Kegiatan Pengambilan Sumpah dan Pelatihan</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<img src="../assets/image/3.jpg" class="card-img-top" alt="...">
					<div class="card-body">
						<p class="card-text">Kegiatan Rapat Koordinasi dengan TP-PKK Kecamatan</p>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	// $(document).ready(function(){
	// 	$('[data-bs-toggle="popover"]').popover();   
	// });
	// const popover = new bootstrap.Popover('.popover-dismiss', {
	// 	trigger: 'focus'
	// })
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

</html>