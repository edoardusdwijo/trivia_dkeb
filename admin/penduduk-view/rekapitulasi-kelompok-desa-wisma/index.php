<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../../login-admin");
	exit;
}
$id = $_SESSION['id'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/rekapitulasikelompokdesawisma-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../assets/image/logo.jpeg">
	<title>Rekapitulasi Kelompok Desa Wisma-Admin</title>
</head>
<?php
if (isset($_GET['dasaWisma'])) {
	$dasaWisma = $_GET['dasaWisma'];
	$cari = $_GET['dasaWisma'];
	$dataCari = mysqli_fetch_array(mysqli_query($db, "SELECT * FROM tbl_data_warga WHERE dasaWisma='$cari'"));
	$dataCari2 = mysqli_fetch_array(mysqli_query($db, "SELECT * FROM tbl_data_keluarga WHERE dasaWisma='$cari'"));
}
?>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../../">Home</a>
		<a href="../../manajemen-akun/">Manajemen Akun</a>
		<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a class="active" href="../">View</a>
		</div>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary hp" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../../">Home</a>
				<a href="../../manajemen-akun/">Manajemen Akun</a>
				<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a class="active" href="./">View</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul">
			<p class="text-center">REKAPITULASI<br>CATATAN DATA DAN DATA WARGA<br>KELOMPOK DESA WISMA</p>
		</div>

		<div class="detail mb-4">
			<!-- <p>RT <span>:</span></p>
			<p>RW <span>:</span></p>
			<p>DESA/KELURAHAN <span>:</span></p>
			<p>TAHUN <span>:</span></p> -->
			<table border="0" width="40%">
				<tr>
					<td>DASA WISMA</td>
					<td width="10%" class="titikdua">:</td>
					<td>
						<form action="" method="get" id="form_id">
							<select class="form-select" aria-label="Default select example" name="dasaWisma" onChange="document.getElementById('form_id').submit();">
								<option selected disabled>Pilih Dasa Wisma</option>
								<?php
								$sql = "SELECT dasaWisma FROM tbl_data_warga";
								$query = mysqli_query($db, $sql);
								$temp = "";
								while ($d = mysqli_fetch_array($query)) {
									if ($temp != $d['dasaWisma']) {
								?>
										<option <?php if (!empty($cari)) {
													echo $cari == $d['dasaWisma'] ? 'selected' : '';
												} ?> value="<?php echo $d['dasaWisma'] ?>"><?php echo $d['dasaWisma'] ?></option>
								<?php
									}
									$temp = $d['dasaWisma'];
								}
								?>
							</select>
						</form>
					</td>
				</tr>
				<tr>
					<td>RT</td>
					<td width="10%" class="titikdua">:</td>
					<?php
					if (isset($dataCari)) {
					?>
						<td><?php if ($dataCari['rt'] > 9) { ?>0<?php echo $dataCari['rt'];
											} else { ?>00<?php echo $dataCari['rt'];
														} ?></td>
					<?php
					} else {
					?>
						<td>-</td>
					<?php
					}
					?>
				</tr>
				<tr>
					<td>RW</td>
					<td width="10%" class="titikdua">:</td>
					<?php
					if (isset($dataCari)) {
					?>
						<td><?php if ($dataCari['rw'] > 9) { ?>0<?php echo $dataCari['rw'];
											} else { ?>00<?php echo $dataCari['rw'];
														} ?></td>
					<?php
					} else {
					?>
						<td>-</td>
					<?php
					}
					?>
				</tr>
				<tr>
					<td>DESA/KELURAHAN</td>
					<td width="10%" class="titikdua">:</td>
					<?php
					if (isset($dataCari2)) {
					?>
						<td><?php echo $dataCari2['desa'] ?></td>
					<?php
					} else {
					?>
						<td>-</td>
					<?php
					}
					?>
				</tr>
				<tr>
					<td>TAHUN</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo date('Y') ?></td>
				</tr>
			</table>
		</div>

		<div class="tabel table-responsive mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th rowspan="3" class="align-middle text-center">NO</th>
						<th rowspan="3" class="align-middle text-center">NAMA KEPALA RUMAH TANGGA</th>
						<th rowspan="3" class="align-middle text-center">JMH KK</th>
						<th colspan="11" class="align-middle text-center">JUMLAH ANGGOTA KELUARGA</th>
						<th colspan="5" class="align-middle text-center">KRITERIA RUMAH</th>
						<th rowspan="2" class="align-middle text-center">SUMBER AIR KELUARGA</th>
						<th rowspan="2" class="align-middle text-center">MAKANAN POKOK</th>
						<th colspan="4" class="align-middle text-center">WARGA MENGIKUTI KEGIATAN</th>
						<th rowspan="3" class="align-middle text-center">KET</th>
					</tr>
					<tr>
						<th colspan="2" class="align-middle text-center">TOTAL</th>
						<th colspan="2" class="align-middle text-center">BALITA</th>
						<th rowspan="2" class="align-middle text-center">PUS</th>
						<th rowspan="2" class="align-middle text-center">WUS</th>
						<th rowspan="2" class="align-middle text-center">IBU HAMIL</th>
						<th rowspan="2" class="align-middle text-center">IBU MENYUSUI</th>
						<th rowspan="2" class="align-middle text-center">LANSIA</th>
						<th rowspan="2" class="align-middle text-center">BUTA</th>
						<th rowspan="2" class="align-middle text-center">BERKEBUTUHAN KHUSUS</th>
						<th rowspan="2" class="align-middle text-center">KRITERIA</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI TMP. PEMB. SAMPAH</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI SPAL</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI JAMBAN KELUARGA</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI STIKER P4K</th>
						<th rowspan="2" class="align-middle text-center">UP2KPKK</th>
						<th rowspan="2" class="align-middle text-center">PEMANFAATAN TANAH PEKARANGAN</th>
						<th rowspan="2" class="align-middle text-center">INDUSTRI RUMAH TANGGA</th>
						<th rowspan="2" class="align-middle text-center">KERJA BAKTI</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-middle text-center">1</td>
						<td class="align-middle text-center">2</td>
						<td class="align-middle text-center">3</td>
						<td colspan="2" class="align-middle text-center">4</td>
						<td colspan="2" class="align-middle text-center">5</td>
						<td class="align-middle text-center">6</td>
						<td class="align-middle text-center">7</td>
						<td class="align-middle text-center">8</td>
						<td class="align-middle text-center">9</td>
						<td class="align-middle text-center">10</td>
						<td class="align-middle text-center">11</td>
						<td class="align-middle text-center">12</td>
						<td class="align-middle text-center">13</td>
						<td class="align-middle text-center">14</td>
						<td class="align-middle text-center">15</td>
						<td class="align-middle text-center">16</td>
						<td class="align-middle text-center">17</td>
						<td class="align-middle text-center">18</td>
						<td class="align-middle text-center">19</td>
						<td class="align-middle text-center">20</td>
						<td class="align-middle text-center">21</td>
						<td class="align-middle text-center">22</td>
						<td class="align-middle text-center">23</td>
						<td class="align-middle text-center">24</td>
					</tr>
					<?php
					if (!isset($cari)) {
					?>
						<tr>
							<td class="align-middle text-center" colspan="30">TIDAK ADA DATA</td>
						</tr>
						<?php
					} else {
						$sqlID = "SELECT idUser FROM tbl_data_warga WHERE dasaWisma = '$cari'";
						$queryID = mysqli_query($db, $sqlID);
						$no = 1;

						while ($data = mysqli_fetch_array($queryID)) {
							$sqlCariData1 = "SELECT * FROM tbl_data_warga WHERE idUser = '" . $data['idUser'] . "'";
							$queryCariData1 = mysqli_query($db, $sqlCariData1);
							$hasilData1 = mysqli_fetch_array($queryCariData1);
							$sqlCariData2 = "SELECT * FROM tbl_data_keluarga WHERE idUser = '" . $data['idUser'] . "'";
							$queryCariData2 = mysqli_query($db, $sqlCariData2);
							$hasilData2 = mysqli_fetch_array($queryCariData2);
							$sqlCariData3 = "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $data['idUser'] . "'";
							$queryCariData3 = mysqli_query($db, $sqlCariData3);
							$hasilData3 = mysqli_fetch_array($queryCariData3);
							$sqlCariData4 = "SELECT * FROM tbl_irt WHERE idKeluarga = '" . $data['idUser'] . "'";
							$queryCariData4 = mysqli_query($db, $sqlCariData4);
							$hasilData4 = mysqli_fetch_array($queryCariData4);

						?>
							<tr>
								<td class="align-middle text-center"><?php echo $no ?></td>
								<td class="align-middle text-center"><?php echo $hasilData1['kepalaRumahTangga'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['jumlahKK'] ?></td>
								<td colspan="2" class="align-middle text-center"><?php echo $hasilData2['jmlhAnggota'] ?></td>
								<td colspan="2" class="align-middle text-center"><?php echo $hasilData2['jumlahBalita'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['jumlahPus'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['jumlahWus'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['jumlahHamil'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['jumlahMenyusui'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['jumlahLansia'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['jumlahButa'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['berkebutuhanKhusus'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['kriteriaRumah'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['memilikiTPS'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['saluranLimbah'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['jambanKeluarga'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['stikerP4K'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['sumberAir'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['makananPokok'] ?></td>
								<td class="align-middle text-center"><?php echo $hasilData2['up2k'] ?></td>
								<?php if (mysqli_num_rows($queryCariData3) < 1) {
								?><td class="align-middle text-center">Tidak</td><?php
																				} else {
																					?><td class="align-middle text-center">Ya</td><?php
																																} ?>
								<?php if (mysqli_num_rows($queryCariData4) < 1) {
								?><td class="align-middle text-center">Tidak</td><?php
																				} else {
																					?><td class="align-middle text-center">Ya</td><?php
																																} ?>
								<td class="align-middle text-center">Ya</td>
								<td class="align-middle text-center"></td>
							</tr>
					<?php
						}
					}
					?>
					<tr>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center">Jumlah</td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="tombol mb-4 text-end">
			<a class="btn btn-light back" href="../" role="button"><img src="../../../assets/icon/icon-backward.png" alt=""></a>&nbsp;&nbsp;
			<a class="btn btn-light next" href="pemanfaatan-pekarangan/?dasaWisma=<?php echo $cari ?>" role="button"><img src="../../../assets/icon/icon-forward.png" alt=""></a>
		</div>
	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

</html>