<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../../login-admin");
	exit;
}
$id = $_SESSION['id'];
$rw = $_GET['rw'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/rekapitulasikelompokpkkrw-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../assets/image/logo.jpeg">
	<title>Rekapitulasi Kelompok PKK RW-Admin</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../../">Home</a>
		<a href="../../manajemen-akun/">Manajemen Akun</a>
		<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a class="active" href="../">View</a>
		</div>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary hp" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../../">Home</a>
				<a href="../../manajemen-akun/">Manajemen Akun</a>
				<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a class="active" href="./">View</a>
				</div>
				<a href="#cetak" data-bs-toggle="collapse">Cetak</a>
				<div class="collapse sub-menu-hp" id="cetak">
					<a href="../../cetak/rekap-kelompok-dasa-wisma/">Rekap Kelompok Dasa Wisma</a>
					<a href="../../cetak/rekap-kelompok-pkk-rt/">Rekap Kelompok PKK RT</a>
					<a href="../../cetak/rekap-kelompok-pkk-rw/">Rekap Kelompok PKK RW</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul">
			<p class="text-center">REKAPITULASI<br>CATATAN DATA DAN DATA WARGA<br>KELOMPOK PKK RW</p>
		</div>

		<div class="detail mb-4">
			<!-- <p>RT <span>:</span></p>
			<p>RW <span>:</span></p>
			<p>DESA/KELURAHAN <span>:</span></p>
			<p>TAHUN <span>:</span></p> -->
			<table border="0" width="40%">
				<tr>
					<td>RW</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php if ($rw > 9) { ?>0<?php echo $rw;
											} else { ?>00<?php echo $rw;
														} ?></td>
				</tr>
				<tr>
					<td>DESA/KELURAHAN</td>
					<td width="10%" class="titikdua">:</td>
					<?php
					$desa = mysqli_fetch_array(mysqli_query($db, "SELECT desa FROM tbl_data_keluarga WHERE rw='$rw'"));
					?>
					<td><?php echo $desa['desa'] ?></td>
				</tr>
				<tr>
					<td>TAHUN</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo date('Y') ?></td>
				</tr>
			</table>
		</div>

		<div class="tabel table-responsive mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th rowspan="3">NO</th>
						<th rowspan="3">NO RT</th>
						<th rowspan="3">JMH DAWIS</th>
						<th rowspan="3">JMH KRT</th>
						<th rowspan="3">JMH KK</th>
						<th colspan="11">JUMLAH ANGGOTA KELUARGA</th>
						<th colspan="6">KRITERIA RUMAH</th>
						<th colspan="3">SUMBER AIR KELUARGA</th>
						<th colspan="2">MAKANAN POKOK</th>
						<th colspan="4">WARGA MENGIKUTI KEGIATAN</th>
						<th rowspan="3">KET</th>
					</tr>
					<tr>
						<th colspan="2">TOTAL</th>
						<th colspan="2">BALITA</th>
						<th rowspan="2">PUS</th>
						<th rowspan="2">WUS</th>
						<th rowspan="2">IBU HAMIL</th>
						<th rowspan="2">IBU MENYUSUI</th>
						<th rowspan="2">LANSIA</th>
						<th rowspan="2">3 BUTA</th>
						<th rowspan="2">BERKEBUTUHAN KHUSUS</th>
						<th rowspan="2">SEHAT</th>
						<th rowspan="2">KURANG SEHAT</th>
						<th rowspan="2">MEMILIKI TEMP. PEMB. SAMPAH</th>
						<th rowspan="2">MEMILIKI SPAL</th>
						<th rowspan="2">MEMILIKI JAMBAN KELUARAGA</th>
						<th rowspan="2">MEMILIKI STIKER P4K</th>
						<th rowspan="2">PDAM</th>
						<th rowspan="2">SUMUR</th>
						<th rowspan="2">DLL</th>
						<th rowspan="2">BERAS</th>
						<th rowspan="2">NON BERAS</th>
						<th rowspan="2">UP2KPKK</th>
						<th rowspan="2">PEMANFAATAN TANAH PEKARANGAN</th>
						<th rowspan="2">INDUSTRI RUMAH TANGGA</th>
						<th rowspan="2">KESEHATAN LINGKUNGAN</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>2</td>
						<td>3</td>
						<td>4</td>
						<td>5</td>
						<td colspan="2">6</td>
						<td colspan="2">7</td>
						<td>10</td>
						<td>11</td>
						<td>12</td>
						<td>13</td>
						<td>14</td>
						<td>15</td>
						<td>16</td>
						<td>17</td>
						<td>18</td>
						<td>19</td>
						<td>20</td>
						<td>21</td>
						<td>22</td>
						<td>23</td>
						<td>24</td>
						<td>25</td>
						<td>26</td>
						<td>27</td>
						<td>28</td>
						<td>29</td>
						<td>30</td>
						<td>31</td>
						<td>32</td>
					</tr>
					<?php
					$sqlRT = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw'";
					$queryRT = mysqli_query($db, $sqlRT);
					$temp = "";
					$no = 1;
					$totalDawis = 0;
					$totalKepalaRT = 0;
					$totalKK = 0;
					$totalAnggota = 0;
					$totalBalita = 0;
					$totalPus = 0;
					$totalWus = 0;
					$totalHamil = 0;
					$totalMenyusui = 0;
					$totalLansia = 0;
					$totalButa = 0;
					$totalBerkebutuhan = 0;
					$totalSehat = 0;
					$totalKurangSehat = 0;
					$totalTPS = 0;
					$totalSaluran = 0;
					$totalJamban = 0;
					$totalStiker = 0;
					$totalSumur = 0;
					$totalPDAM = 0;
					$totalDll = 0;
					$totalBeras = 0;
					$totalNonBeras = 0;
					$totalUp2k = 0;
					$totalPTP = 0;
					$totalIRT = 0;
					$totalKesehatan = 0;
					while ($dataRT = mysqli_fetch_array($queryRT)) {
						if ($temp != $dataRT['rt']) {
					?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $dataRT['rt'];
									$rt = $dataRT['rt'] ?></td>

								<?php
								$sqlDawis = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryDawis = mysqli_query($db, $sqlDawis);
								$tempDawis = "";
								$jumlahDawis = 0;
								while ($dataDawis = mysqli_fetch_array($queryDawis)) {
									if ($tempDawis != $dataDawis['dasaWisma']) {
										$jumlahDawis++;
										$totalDawis++;
									}
								}
								?>
								<td><?php echo $jumlahDawis ?></td>

								<?php
								$sqlKepalaRT = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryKepalaRT = mysqli_query($db, $sqlKepalaRT);
								$tempKepalaRT = "";
								$jumlahKepalaRT = 0;
								while ($dataKepalaRT = mysqli_fetch_array($queryKepalaRT)) {
									if ($tempKepalaRT != $dataKepalaRT['namaKepalaK']) {
										$jumlahKepalaRT++;
										$totalKepalaRT++;
									}
								}
								?>
								<td><?php echo $jumlahKepalaRT ?></td>

								<?php
								$sqlKK = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryKK = mysqli_query($db, $sqlKK);
								$jumlahKK = 0;
								while ($dataKK = mysqli_fetch_array($queryKK)) {
									$jumlahKK += $dataKK['jumlahKK'];
									$totalKK += $dataKK['jumlahKK'];
								}
								?>
								<td><?php echo $jumlahKK ?></td>

								<?php
								$sqltotalAnggota = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryTotalAnggota = mysqli_query($db, $sqltotalAnggota);
								$jumlahAnggota = 0;
								while ($dataAnggota = mysqli_fetch_array($queryTotalAnggota)) {
									$jumlahAnggota += $dataAnggota['jmlhAnggota'];
									$totalAnggota += $dataAnggota['jmlhAnggota'];
								}
								?>
								<td colspan="2"><?php echo $jumlahAnggota ?></td>

								<?php
								$sqltotalBalita = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryTotalBalita = mysqli_query($db, $sqltotalBalita);
								$jumlahBalita = 0;
								while ($dataBalita = mysqli_fetch_array($queryTotalBalita)) {
									$jumlahBalita += $dataBalita['jumlahBalita'];
									$totalBalita += $dataBalita['jumlahBalita'];
								}
								?>
								<td colspan="2"><?php echo $jumlahBalita ?></td>

								<?php
								$sqlJumlahPus = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahPus = mysqli_query($db, $sqlJumlahPus);
								$jumlahPus = 0;
								while ($dataJumlahPus = mysqli_fetch_array($queryJumlahPus)) {
									$jumlahPus += $dataJumlahPus['jumlahPus'];
									$totalPus += $dataJumlahPus['jumlahPus'];
								}
								?>
								<td><?php echo $jumlahPus ?></td>

								<?php
								$sqlJumlahWus = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahWus = mysqli_query($db, $sqlJumlahWus);
								$jumlahWus = 0;
								while ($dataWus = mysqli_fetch_array($queryJumlahWus)) {
									$jumlahWus += $dataWus['jumlahWus'];
									$totalWus += $dataWus['jumlahWus'];
								}
								?>
								<td><?php echo $jumlahWus ?></td>

								<?php
								$sqlJumlahHamil = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahHamil = mysqli_query($db, $sqlJumlahHamil);
								$jumlahHamil = 0;
								while ($dataHamil = mysqli_fetch_array($queryJumlahHamil)) {
									$jumlahHamil += $dataHamil['jumlahHamil'];
									$totalHamil += $dataHamil['jumlahHamil'];
								}
								?>
								<td><?php echo $jumlahHamil ?></td>

								<?php
								$sqlJumlahMenyusui = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahMenyusui = mysqli_query($db, $sqlJumlahMenyusui);
								$jumlahMenyusui = 0;
								while ($dataMenyusui = mysqli_fetch_array($queryJumlahMenyusui)) {
									$jumlahMenyusui += $dataMenyusui['jumlahMenyusui'];
									$totalMenyusui += $dataMenyusui['jumlahMenyusui'];
								}
								?>
								<td><?php echo $jumlahMenyusui ?></td>

								<?php
								$sqlJumlahLansia = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahLansia = mysqli_query($db, $sqlJumlahLansia);
								$jumlahLansia = 0;
								while ($dataLansia = mysqli_fetch_array($queryJumlahLansia)) {
									$jumlahLansia += $dataLansia['jumlahLansia'];
									$totalLansia += $dataLansia['jumlahLansia'];
								}
								?>
								<td><?php echo $jumlahLansia ?></td>

								<?php
								$sqlJumlahButa = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahButa = mysqli_query($db, $sqlJumlahButa);
								$jumlahButa = 0;
								while ($dataButa = mysqli_fetch_array($queryJumlahButa)) {
									$jumlahButa += $dataButa['jumlahButa'];
									$totalButa += $dataButa['jumlahButa'];
								}
								?>
								<td><?php echo $jumlahButa ?></td>

								<?php
								$sqlJumlahBerkebutuhan = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahBerkebutuhan = mysqli_query($db, $sqlJumlahBerkebutuhan);
								$jumlahBerkebutuhan = 0;
								while ($dataBerkebutuhan = mysqli_fetch_array($queryJumlahBerkebutuhan)) {
									if ($dataBerkebutuhan['berkebutuhanKhusus'] == 'Fisik' || $dataBerkebutuhan['berkebutuhanKhusus'] == 'Non Fisik') {
										$jumlahBerkebutuhan++;
										$totalBerkebutuhan++;
									}
								}
								?>
								<td><?php echo $jumlahBerkebutuhan ?></td>

								<?php
								$sqlKriteriaRumah = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryKriteriaRumah = mysqli_query($db, $sqlKriteriaRumah);
								$jumlahSehat = 0;
								$jumlahKurangSehat = 0;
								$jumlahTPS = 0;
								$jumlahSaluran = 0;
								$jumlahJamban = 0;
								$jumlahStiker = 0;
								while ($dataKriteriaRumah = mysqli_fetch_array($queryKriteriaRumah)) {
									if($dataKriteriaRumah['kriteriaRumah'] == 'Sehat'){
										$jumlahSehat++;
										$totalSehat++;
									}else if($dataKriteriaRumah['kriteriaRumah'] == 'Kurang Sehat'){
										$jumlahKurangSehat++;
										$totalKurangSehat++;
									}

									if($dataKriteriaRumah['memilikiTPS'] == 'Ya'){
										$jumlahTPS++;
										$totalTPS++;
									}

									if($dataKriteriaRumah['saluranLimbah'] == 'Ya'){
										$jumlahSaluran++;
										$totalSaluran++;
									}

									if($dataKriteriaRumah['jambanKeluarga'] == 'Ya'){
										$jumlahJamban++;
										$totalJamban++;
									}

									if($dataKriteriaRumah['stikerP4K'] == 'Ya'){
										$jumlahStiker++;
										$totalStiker++;
									}

								}
								?>
								<td><?php echo $jumlahSehat ?></td>
								<td><?php echo $jumlahKurangSehat ?></td>
								<td><?php echo $jumlahTPS ?></td>
								<td><?php echo $jumlahSaluran ?></td>
								<td><?php echo $jumlahJamban ?></td>
								<td><?php echo $jumlahStiker ?></td>

								<?php
								$sqlSumberAir = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$querySumberAir = mysqli_query($db, $sqlSumberAir);
								$jumlahPDAM = 0;
								$jumlahSumur = 0;
								$jumlahDll = 0;
								while($dataSumberAir = mysqli_fetch_array($querySumberAir)){
									if($dataSumberAir['sumberAir'] == 'PDAM'){
										$jumlahPDAM++;
										$totalPDAM++;
									}else if($dataSumberAir['sumberAir'] == 'Sumur'){
										$jumlahSumur++;
										$totalSumur++;
									}else if($dataSumberAir['sumberAir'] == 'Dll'){
										$jumlahDll++;
										$totalDll++;
									}
								}
								?>
								<td><?php echo $jumlahPDAM ?></td>
								<td><?php echo $jumlahSumur ?></td>
								<td><?php echo $jumlahDll ?></td>

								<?php
								$sqlMakananPokok = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryMakananPokok = mysqli_query($db, $sqlMakananPokok);
								$jumlahBeras = 0;
								$jumlahNonBeras = 0;
								while($dataMakananPokok = mysqli_fetch_array($queryMakananPokok)){
									if($dataMakananPokok['makananPokok'] == 'Beras'){
										$jumlahBeras++;
										$totalBeras++;
									}else{
										$jumlahNonBeras++;
										$totalNonBeras++;
									}
								}
								?>
								<td><?php echo $jumlahBeras ?></td>
								<td><?php echo $jumlahNonBeras ?></td>

								<?php
								$sqlUp2k = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryUp2k = mysqli_query($db,$sqlUp2k);
								$jumlahUp2k = 0;
								while($dataUp2k = mysqli_fetch_array($queryUp2k)){
									if($dataUp2k['up2k'] == 'Ya'){
										$jumlahUp2k++;
										$totalUp2k++;
									}
								}
								?>
								<td><?php echo $jumlahUp2k ?></td>
								
								<?php
								$sqlPTP = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryPTP = mysqli_query($db,$sqlPTP);
								$jumlahPTP = 0;
								while($dataPTP = mysqli_fetch_array($queryPTP)){
									$querydiPTP = mysqli_query($db,"SELECT * FROM tbl_ptp WHERE idKeluarga = '".$dataPTP['idKeluarga']."'");
									$jumlahPTP += mysqli_num_rows($querydiPTP);
									$totalPTP += mysqli_num_rows($querydiPTP);
								}
								?>
								<td><?php echo $jumlahPTP ?></td>

								<?php
								$sqlIRT = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryIRT = mysqli_query($db,$sqlIRT);
								$jumlahIRT = 0;
								while($dataIRT = mysqli_fetch_array($queryIRT)){
									$querydiIRT = mysqli_query($db,"SELECT * FROM tbl_irt WHERE idKeluarga = '".$dataIRT['idKeluarga']."'");
									$jumlahIRT += mysqli_num_rows($querydiIRT);
									$totalIRT += mysqli_num_rows($querydiIRT);
								}
								?>
								<td><?php echo $jumlahIRT ?></td>

								<?php
								$sqlKesehatan = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryKesehatan = mysqli_query($db, $sqlKesehatan);
								$jumlahKesehatan = 0;
								$jumlahKesehatan += mysqli_num_rows($queryKesehatan);
								$totalKesehatan += mysqli_num_rows($queryKesehatan);
								?>
								<td><?php echo $jumlahKesehatan ?></td>
								<td></td>
							</tr>
					<?php
							$no++;
						}
					}
					?>
					<tr>
						<td></td>
						<td>JUMLAH</td>
						<td><?php echo $totalDawis ?></td>
						<td><?php echo $totalKepalaRT ?></td>
						<td><?php echo $totalKK ?></td>
						<td colspan="2"><?php echo $totalAnggota ?></td>
						<td colspan="2"><?php echo $totalBalita ?></td>
						<td><?php echo $totalPus ?></td>
						<td><?php echo $totalWus ?></td>
						<td><?php echo $totalHamil ?></td>
						<td><?php echo $totalMenyusui ?></td>
						<td><?php echo $totalLansia ?></td>
						<td><?php echo $totalButa ?></td>
						<td><?php echo $totalBerkebutuhan ?></td>
						<td><?php echo $totalSehat ?></td>
						<td><?php echo $totalKurangSehat ?></td>
						<td><?php echo $totalTPS ?></td>
						<td><?php echo $totalSaluran ?></td>
						<td><?php echo $totalJamban ?></td>
						<td><?php echo $totalStiker ?></td>
						<td><?php echo $totalPDAM ?></td>
						<td><?php echo $totalSumur ?></td>
						<td><?php echo $totalDll ?></td>
						<td><?php echo $totalBeras ?></td>
						<td><?php echo $totalNonBeras ?></td>
						<td><?php echo $totalUp2k ?></td>
						<td><?php echo $totalPTP ?></td>
						<td><?php echo $totalIRT ?></td>
						<td><?php echo $totalKesehatan ?></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="tombol mb-4 text-end">
			<a class="btn btn-light back" href="../" role="button"><img src="../../../assets/icon/icon-backward.png" alt=""></a>&nbsp;&nbsp;
			<a class="btn btn-light next" href="pemanfaatan-pekarangan/?rw=<?php echo $rw ?>" role="button"><img src="../../../assets/icon/icon-forward.png" alt=""></a>
		</div>
	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

</html>