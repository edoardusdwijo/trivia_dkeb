<?php
include('../../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../../../login-admin");
	exit;
}
$id = $_SESSION['id'];
$rw = $_GET['rw'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../../css/pemanfaatanpekarangan-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../../assets/image/logo.jpeg">
	<title>Pemanfaatan Tanah Pekarangan-Admin</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../../../">Home</a>
		<a href="../../../manajemen-akun/">Manajemen Akun</a>
		<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a class="active" href="../../">View</a>
		</div>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary hp" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../../../">Home</a>
				<a href="../../../manajemen-akun/">Manajemen Akun</a>
				<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a class="active" href="../../">View</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul">
			<p class="text-center">PEMANFAATAN TANAH PEKARANGAN / PTP HATINYA PKK</p>
		</div>

		<div class="tabel d-flex flex-sm-row flex-column mb-4">
			<div class="pemanfaatan">
				<p class="subjudul">PEMANFAATAN TANAH PEKARANGAN / PTP HATINYA PKK</p>
				<P class="subjudul1">KELOMPOK PKK RW : <?php if ($rw > 9) { ?>0<?php echo $rw;
																			} else { ?>00<?php echo $rw;
																						} ?></P>
				<P class="subjudul1">KELOMPOK PKK DESA/KEL : </P>
				<P class="subjudul1">TAHUN : <?php echo date('Y') ?></P>

				<div class="table-responsive mb-4">
					<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
						<thead class="table-warning">
							<tr>
								<th>NO</th>
								<th>NO RT</th>
								<th>JUMLAH DAWIS</th>
								<th>JUMLAH KRT</th>
								<th>PETERNAKAN</th>
								<th>PERIKANAN</th>
								<th>WARUNG HIDUP</th>
								<th>TOGA</th>
								<th>LUMBUNG HIDUP</th>
								<th>TANAMAN KERAS</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$sqlCariRw = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw'";
							$queryCariRw = mysqli_query($db, $sqlCariRw);
							$totalPeternakan = 0;
							$totalPerikanan = 0;
							$totalWarung = 0;
							$totalToga = 0;
							$totalLumbung = 0;
							$totalTanaman = 0;
							$totalDawis = 0;
							$totalKRT = 0;
							$no = 1;
							$tempRT = "";
							while($rt = mysqli_fetch_array($queryCariRw)){
								$jumlahPeternakan = 0;
								$jumlahPerikanan = 0;
								$jumlahWarung = 0;
								$jumlahToga = 0;
								$jumlahLumbung = 0;
								$jumlahTanaman = 0;
										if($tempRT != $rt['rt']){
											?><tr><?php
											$tempRT = $rt['rt'];
											$RT = $rt['rt'];
											?><td><?php echo $no ?></td><?php
											?><td><?php if($rt['rt']<10){?>00<?php echo $rt['rt'];}else{?>0<?php echo $rt['rt'];} ?></td><?php

											$sqlCariJumlahDawis = "SELECT * FROM tbl_data_keluarga WHERE rw=$rw AND rt=$RT";
											$queryCariJumlahDawis = mysqli_query($db,$sqlCariJumlahDawis);
											$tempDawis = "";
											$jumlahDawis = 0;
											while($dataDawis = mysqli_fetch_array($queryCariJumlahDawis)){
												if($tempDawis != $dataDawis['dasaWisma']){
													$jumlahDawis++;
													$totalDawis++;
													$dawis = $dataDawis['dasaWisma'];
													$sqlJumlah = "SELECT * FROM tbl_ptp WHERE dasaWisma = '$dawis'";
													$queryJumlah = mysqli_query($db,$sqlJumlah);

													while($dataPTP = mysqli_fetch_array($queryJumlah)){
														if($dataPTP['keterangan'] == "Peternakan"){
															$jumlahPeternakan++;
															$totalPeternakan++;
														}else if($dataPTP['keterangan'] == "Perikanan"){
															$jumlahPerikanan++;
															$totalPerikanan++;
														}else if($dataPTP['keterangan'] == "Warung Hidup"){
															$jumlahWarung++;
															$totalWarung++;
														}else if($dataPTP['keterangan'] == "Toga"){
															$jumlahToga++;
															$totalToga++;
														}else if($dataPTP['keterangan'] == "Lumbung Hidup"){
															$jumlahLumbung++;
															$totalLumbung++;
														}else if($dataPTP['keterangan'] == "Tanaman Keras"){
															$jumlahTanaman++;
															$totalTanaman++;
														}
													}
												}
											}
											?><td><?php echo $jumlahDawis ?></td><?php

											$sqlCariJumlahKRT = "SELECT * FROM tbl_data_keluarga WHERE rw=$rw AND rt=$RT";
											$queryCariJumlahKRT = mysqli_query($db,$sqlCariJumlahKRT);
											$tempKRT = "";
											$jumlahKRT = 0;
											while($dataKRT = mysqli_fetch_array($queryCariJumlahKRT)){
												if($tempKRT != $dataKRT['namaKepalaK']){
													$jumlahKRT++;
													$totalKRT++;
												}
											}
											?><td><?php echo $jumlahKRT ?></td><?php
											?>
											<td><?php echo $jumlahPeternakan ?></td>
											<td><?php echo $jumlahPerikanan ?></td>
											<td><?php echo $jumlahWarung ?></td>
											<td><?php echo $jumlahToga ?></td>
											<td><?php echo $jumlahLumbung ?></td>
											<td><?php echo $jumlahTanaman ?></td>
											</tr>
											<?php
											$no++;
										}
							}
							?>
							<tr>
								<td></td>
								<td>Total</td>
								<td><?php echo $totalDawis ?></td>
								<td><?php echo $totalKRT ?></td>
								<td><?php echo $totalPeternakan ?></td>
								<td><?php echo $totalPerikanan ?></td>
								<td><?php echo $totalWarung ?></td>
								<td><?php echo $totalToga ?></td>
								<td><?php echo $totalLumbung ?></td>
								<td><?php echo $totalTanaman ?></td>
							</tr>
							<?php
							?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="industri">
				<p class="subjudul">INDUSTRI RUMAH TANGGA</p>
				<P class="subjudul2">.</P>
				<P class="subjudul2">.</P>
				<P class="subjudul2">.</P>

				<div class="table-responsive mb-4">
					<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
						<thead class="table-warning">
							<tr>
								<th>PANGAN</th>
								<th>SANDANG</th>
								<th>JASA</th>
								<th>LAIN-LAIN</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$sqlCariIdBuatIRT = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw'";
							$queryCariIdBuatIRT = mysqli_query($db, $sqlCariIdBuatIRT);
							$tempDawis2 = "";
							while ($cariDataIRT = mysqli_fetch_array($queryCariIdBuatIRT)) {
								$dawis2 = $cariDataIRT['dasaWisma'];
								if($tempDawis2 != $dawis2){
									$jumlahPangan = 0;
									$jumlahSandang = 0;
									$jumlahJasa = 0;
									$jumlahLain = 0;
									$sqlDataIRT = "SELECT * FROM tbl_irt WHERE dasaWisma = '$dawis2'";
									$queryDataIRT = mysqli_query($db, $sqlDataIRT);
									while($ulangDawis = mysqli_fetch_array($queryDataIRT)){
										if($ulangDawis['keterangan'] == 'Pangan'){
											$jumlahPangan++;
										}else if($ulangDawis['keterangan'] == 'Sandang'){
											$jumlahSandang++;
										}else if($ulangDawis['keterangan'] == 'Jasa'){
											$jumlahJasa++;
										}else if($ulangDawis['keterangan'] == 'Lain-lain'){
											$jumlahLain++;
										}
									}
									?>
									<tr>
										<td><?php echo $jumlahPangan ?></td>
										<td><?php echo $jumlahSandang ?></td>
										<td><?php echo $jumlahJasa ?></td>
										<td><?php echo $jumlahLain ?></td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="tombol mb-4 text-end">
			<a class="btn btn-light back" href="../?rw=<?php echo $rw ?>" role="button"><img src="../../../../assets/icon/icon-backward.png" alt=""></a>&nbsp;&nbsp;
			<a class="btn btn-danger" href="../../../cetak/rekap-kelompok-pkk-rw/?rw=<?php echo $rw ?>" role="button" target="_BLANK">Print</a>
		</div>
	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

</html>