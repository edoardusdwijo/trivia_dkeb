<?php
include('../../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../../../login-admin");
	exit;
}
$id = $_SESSION['id'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../../css/pilihrw-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../../assets/image/logo.jpeg">
	<title>Pilih RW-Admin</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../../../">Home</a>
		<a href="../../../manajemen-akun/">Manajemen Akun</a>
		<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a class="active" href="./">View</a>
		</div>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary hp" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../../../">Home</a>
				<a href="../../../manajemen-akun/">Manajemen Akun</a>
				<a class="active" href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a class="active" href="./">View</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul mb-4">
			<p class="text-center">PILIH RW</p>
		</div>

		<div class="tabel table-responsive mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden" id="tbl_user">
				<thead class="table-warning">
					<th width="5%" class="text-center">No</th>
					<th class="text-center">RW</th>
					<th class="text-center">Aksi</th>
				</thead>
				<tbody>
					<?php
					$sqlRW = "SELECT rw FROM tbl_data_keluarga";
					$queryRW = mysqli_query($db, $sqlRW);
					$temp = "";
					$no = 1;
					while ($dataRW = mysqli_fetch_array($queryRW)) {
						if ($temp != $dataRW['rw']) {
					?>
							<tr>
								<td class="text-center"><?php echo $no ?></td>
								<td class="text-center"><?php if($dataRW['rw']>9){?>0<?php echo $dataRW['rw'];}else{?>00<?php echo $dataRW['rw'];} ?></td>
								<td class="align-midle">
									<div class="d-flex justify-content-center">
										<!-- <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="">Pilih</button> -->
										<a class="btn btn-primary btn-sm" href="../?rw=<?php echo $dataRW['rw'] ?>" role="button">Pilih</a>
									</div>
								</td>
							</tr>
					<?php
						$temp = $dataRW['rw'];
						}
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>

	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

</html>