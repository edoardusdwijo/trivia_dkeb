<?php
include('../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../login-admin");
	exit;
}


if (isset($_POST['edit'])){
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    $tglLahir = $_POST['tglLahir'];
    $statusAkun = $_POST['statusAkun'];
    $user_type = $_POST['user_type'];

    if($_POST['password'] != ""){
        $sql = "UPDATE tbl_user SET nama='$nama', email='$email', `password`='$password', tglLahir='$tglLahir', tipeUser='$user_type', statusAkun='$statusAkun' WHERE id='$id'";
        $query = mysqli_query($db, $sql);

        if ($query){
            echo "
	        <script>
	            alert('DATA BERHASIL DI PERBARUI');
				document.location.href = '../manajemen-akun';
	        </script>
	        ";
        }else{
            echo "
	        <script>
	            alert('GAGAL MEMPERBARUI DATA');
				document.location.href = '../manajemen-akun';
	        </script>
	        ";
        }
    }else{
        $sql = "UPDATE tbl_user SET nama='$nama', email='$email', tglLahir='$tglLahir', tipeUser='$user_type', statusAkun='$statusAkun' WHERE id='$id'";
        $query = mysqli_query($db, $sql);

        if ($query){
            echo "
	        <script>
	            alert('DATA BERHASIL DI PERBARUI');
				document.location.href = '../manajemen-akun';
	        </script>
	        ";
        }else{
            echo "
	        <script>
	            alert('GAGAL MEMPERBARUI DATA');
				document.location.href = '../manajemen-akun';
	        </script>
	        ";
        }
    }
}

if (isset($_POST['hapus'])){
    $id = $_POST['id'];

    $sql = "DELETE FROM tbl_user WHERE id=$id";
    $query = mysqli_query($db, $sql);

    if ($query){
        echo "
            <script>
                alert('DATA BERHASIL DI HAPUS');
                document.location.href = '../manajemen-akun';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('GAGAL MENGHAPUS DATA');
                document.location.href = '../manajemen-akun';
            </script>
        ";
    }
}