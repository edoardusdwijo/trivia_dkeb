<?php
include('../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../login-admin");
	exit;
}
$id = $_SESSION['id'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/manajemenakun-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../assets/image/logo.jpeg">
	<title>Manajemen Akun-Admin</title>
</head>

<body>
	<!-- start navbar -->
	<nav class="navbar navbar-expand bg-light">
		<div class="container">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" href="#"><img src="../../assets/icon/icon-profile.png" alt="Profile" class="profil"></a>
					</li>
					<li class="nav-item">
						<?php
						$sql = "SELECT * FROM tbl_user WHERE id='$id'";
						$query = mysqli_query($db, $sql);
						$data = mysqli_fetch_array($query);
						?>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<?php echo $data['nama'] ?>&nbsp;<img src="../../assets/icon/icon-dropdown.png" alt="">
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="../../logout.php">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start sidebar laptop -->
	<div class="sidebar-lp">
		<div class="logo mt-4 mb-4">
			<img src="../../assets/image/logo.jpeg" alt="">
		</div>
		<a href="../">Home</a>
		<a class="active" href="./">Manajemen Akun</a>
		<a href="#setting" data-bs-toggle="collapse">Penduduk</a>
		<div class="collapse sub-menu-lp" id="setting">
			<a href="../penduduk-view/">View</a>
		</div>
	</div>
	<!-- start sidebar laptop -->

	<!-- start sidebar hp -->
	<div class="sidebar-hp">
		<button class="btn btn-primary hp" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><img src="../../assets/icon/icon-menu.png" alt=""></button>

		<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
			<div class="offcanvas-header">
				<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="logo-hp mt-4 mb-4">
					<center>
						<img src="../../assets/image/logo.jpeg" alt="">
					</center>
				</div>
				<a href="../">Home</a>
				<a class="active" href="./">Manajemen Akun</a>
				<a href="#setting" data-bs-toggle="collapse">Penduduk</a>
				<div class="collapse sub-menu-hp" id="setting">
					<a href="../penduduk-view/">View</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end sidebar hp -->

	<!-- start konten -->
	<div class="content">
		<div class="judul">
			<p>Manajemen Akun</p>
		</div>

		<div class="tabel table-responsive mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th scope="col" class="text-center">No</th>
						<th scope="col" class="text-center">Nama</th>
						<th scope="col" class="text-center">Email</th>
						<th scope="col" class="text-center">Tipe User</th>
						<th scope="col" class="text-center">Tanggal Lahir</th>
						<th scope="col" class="text-center">Status</th>
						<th scope="col" class="text-center">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM tbl_user";
					$query = mysqli_query($db, $sql);
					$no = 1;
					while ($data = mysqli_fetch_array($query)) {
						?>
						<tr>
							<td><?php echo $no ?></td>
							<td><?php echo $data['nama'] ?></td>
							<td><?php echo $data['email'] ?></td>
							<td><?php echo $data['tipeUser'] ?></td>
							<td><?php echo $data['tglLahir'] ?></td>
							<td><?php echo $data['statusAkun'] ?></td>
							<td class="align-midle">
								<div class="d-flex justify-content-center">
									<button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#editData<?php echo $no ?>">Edit</button>&nbsp;
									<button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#hapusData<?php echo $no ?>">Hapus</button>
								</div>
							</td>
						</tr>
						<div class="modal fade" id="editData<?php echo $no ?>" tabindex="-1" aria-labelledby="edittambahModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="edittambahModalLabel">Edit Data</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>
									<form action="confManajemenAkun.php" method="POST">
										<div class="modal-body">
											<div class="mb-3">
												<label for="exampleFormControlInput1" class="form-label">Nama</label>
												<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $data['nama'] ?>" name="nama">
												<input type="hidden" class="form-control" id="exampleFormControlInput1" value="<?php echo $data['id'] ?>" name="id">
											</div>
											<div class="mb-3">
												<label for="exampleInputEmail1" class="form-label">EMAIL</label>
												<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Email" name="email" value="<?php echo $data['email'] ?>" required>
											</div>
											<div class="mb-3">
												<label for="exampleInputEmail1" class="form-label">SELECT USER TYPE</label>
												<select class="form-select" aria-label="Default select example" name="user_type" required>
													<?php
													if ($data['tipeUser'] == 'admin'){
														?>
														<option value="admin" selected>Admin</option>
														<option value="user">User</option>
														<?php
													}else{
														?>
														<option value="admin">Admin</option>
														<option value="user" selected>User</option>
														<?php
													}
													?>
												</select>
											</div>
											<div class="mb-3">
												<label for="exampleInputEmail1" class="form-label">STATUS AKUN</label>
												<select class="form-select" aria-label="Default select example" name="statusAkun" required>
													<?php
													if ($data['statusAkun'] == 'non-aktif'){
														?>
														<option value="non-aktif" selected>non-aktif</option>
														<option value="aktif">aktif</option>
														<?php
													}else{
														?>
														<option value="non-aktif">non-aktif</option>
														<option value="aktif" selected>aktif</option>
														<?php
													}
													?>
												</select>
											</div>
											<div class="mb-3">
												<label for="exampleInputPassword1" class="form-label">DATE OF BIRTH</label>
												<input type="date" class="form-control" id="exampleInputPassword1" placeholder="Masukkan Konfirmasi Password" name="tglLahir" value="<?php echo $data['tglLahir'] ?>" required>
											</div>
											<div class="mb-3">
												<label for="exampleInputPassword1" class="form-label">PASSWORD (kosongkan jika tidak ingin mengganti password)</label>
												<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Masukkan Password" name="password">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
											<button type="submit" class="btn btn-primary" value="edit" name="edit">Simpan Perubahan</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="modal fade" id="hapusData<?php echo $no ?>" tabindex="-1" aria-labelledby="hapusModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="hapusModalLabel">Hapus</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>
									<form action="confManajemenAkun.php" method="POST">
										<div class="modal-body">
											<input type="hidden" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $data['id'] ?>" name="id">
											Apakah anda yakin ingin menghapus data ini?
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" data-bs-dismiss="modal">Batal</button>
											<button type="submit" class="btn btn-danger" name="hapus" value="hapus">Hapus</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
</script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>