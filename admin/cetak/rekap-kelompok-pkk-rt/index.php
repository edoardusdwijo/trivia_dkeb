<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../../login-admin");
	exit;
}
$id = $_SESSION['id'];
$rt = $_GET['rt'];
$rw = $_GET['rw'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/rekapitulasikelompokpkkrtcetak-admin.css" media="screen">
	<link rel="stylesheet" href="../../../css/rekapitulasikelompokpkkrtview-admin.css" media="print">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../assets/image/logo.jpeg">
	<title>Cetak Rekapitulasi Kelompok PKK RT-Admin</title>
</head>

<body>
	<!-- start konten -->
	<div class="content">
		<div class="judul">
			<p class="text-center">REKAPITULASI<br>CATATAN DATA DAN DATA WARGA<br>KELOMPOK PKK RT</p>
		</div>

		<div class="detail mb-4">
			<!-- <p>RT <span>:</span></p>
			<p>RW <span>:</span></p>
			<p>DESA/KELURAHAN <span>:</span></p>
			<p>TAHUN <span>:</span></p> -->
			<table border="0" width="40%">
				<tr>
					<td width="40%">RT</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo $rt ?></td>
				</tr>
				<tr>
					<td>RW</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo $rt ?></td>
				</tr>
				<tr>
					<td>DESA/KELURAHAN</td>
					<td width="10%" class="titikdua">:</td>
					<?php
					$desa = mysqli_fetch_array(mysqli_query($db, "SELECT desa FROM tbl_data_keluarga"));
					?>
					<td><?php echo $desa['desa'] ?></td>
				</tr>
				<tr>
					<td>TAHUN</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo date('Y') ?></td>
				</tr>
			</table>
		</div>

		<div class="tabel table-responsive mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th rowspan="3" class="align-middle text-center">NO</th>
						<th rowspan="3" class="align-middle text-center">NAMA DASA WISMA</th>
						<th rowspan="3" class="align-middle text-center">JMH KRT</th>
						<th rowspan="3" class="align-middle text-center">JMH KK</th>
						<th colspan="11" class="align-middle text-center">JUMLAH ANGGOTA KELUARGA</th>
						<th colspan="6" class="align-middle text-center">KRITERIA RUMAH</th>
						<th colspan="3" class="align-middle text-center">SUMBER AIR KELUARGA</th>
						<th colspan="2" class="align-middle text-center">MAKANAN POKOK</th>
						<th colspan="4" class="align-middle text-center">WARGA MENGIKUTI KEGIATAN</th>
						<th rowspan="3" class="align-middle text-center">KET</th>
					</tr>
					<tr>
						<th colspan="2" rowspan="2" class="align-middle text-center">TOTAL</th>
						<th colspan="2" rowspan="2" class="align-middle text-center">BALITA</th>
						<th rowspan="2" class="align-middle text-center">PUS</th>
						<th rowspan="2" class="align-middle text-center">WUS</th>
						<th rowspan="2" class="align-middle text-center">IBU HAMIL</th>
						<th rowspan="2" class="align-middle text-center">IBU MENYUSUI</th>
						<th rowspan="2" class="align-middle text-center">LANSIA</th>
						<th rowspan="2" class="align-middle text-center">3 BUTA</th>
						<th rowspan="2" class="align-middle text-center">BERKEBUTUHAN KHUSUS</th>
						<th rowspan="2" class="align-middle text-center">SEHAT</th>
						<th rowspan="2" class="align-middle text-center">KURANG SEHAT</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI TEMP. PEMB. SAMPAH</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI SPAL</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI JAMBAN KELUARAGA</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI STIKER P4K</th>
						<th rowspan="2" class="align-middle text-center">PDAM</th>
						<th rowspan="2" class="align-middle text-center">SUMUR</th>
						<th rowspan="2" class="align-middle text-center">DLL</th>
						<th rowspan="2" class="align-middle text-center">BERAS</th>
						<th rowspan="2" class="align-middle text-center">NON BERAS</th>
						<th rowspan="2" class="align-middle text-center">UP2KPKK</th>
						<th rowspan="2" class="align-middle text-center">PEMANFAATAN TANAH PEKARANGAN</th>
						<th rowspan="2" class="align-middle text-center">INDUSTRI RUMAH TANGGA</th>
						<th rowspan="2" class="align-middle text-center">KESEHATAN LINGKUNGAN</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-middle text-center">1</td>
						<td class="align-middle text-center">2</td>
						<td class="align-middle text-center">3</td>
						<td class="align-middle text-center">4</td>
						<td colspan="2" class="align-middle text-center">5</td>
						<td colspan="2" class="align-middle text-center">6</td>
						<td class="align-middle text-center">7</td>
						<td class="align-middle text-center">8</td>
						<td class="align-middle text-center">9</td>
						<td class="align-middle text-center">10</td>
						<td class="align-middle text-center">11</td>
						<td class="align-middle text-center">12</td>
						<td class="align-middle text-center">13</td>
						<td class="align-middle text-center">14</td>
						<td class="align-middle text-center">15</td>
						<td class="align-middle text-center">16</td>
						<td class="align-middle text-center">17</td>
						<td class="align-middle text-center">18</td>
						<td class="align-middle text-center">19</td>
						<td class="align-middle text-center">20</td>
						<td class="align-middle text-center">21</td>
						<td class="align-middle text-center">22</td>
						<td class="align-middle text-center">23</td>
						<td class="align-middle text-center">24</td>
						<td class="align-middle text-center">25</td>
						<td class="align-middle text-center">26</td>
						<td class="align-middle text-center">27</td>
						<td class="align-middle text-center">28</td>
						<td class="align-middle text-center">29</td>
					</tr>
					<?php
					if (mysqli_num_rows(mysqli_query($db, "SELECT * FROM tbl_data_warga WHERE rt='$rt' AND rw='$rw'")) < 1) {
					?>
						<td class="align-middle text-center" colspan="31">TIDAK ADA DATA</td>
						<?php
					} else {
						$sqlDasaWisma = "SELECT dasaWisma FROM tbl_data_warga WHERE rt='$rt' AND rw='$rw'";
						$queryDasaWisma = mysqli_query($db, $sqlDasaWisma);
						$tempDasaWisma = "";

						$totalKRT = 0;
						$totalKK = 0;
						$totalAnggota = 0;
						$totalBalita = 0;
						$totalPus = 0;
						$totalWus = 0;
						$totalHamil = 0;
						$totalMenyusui = 0;
						$totalLansia = 0;
						$totalButa = 0;
						$totalBerkebutuhanKhusus = 0;
						$totalRumahSehat = 0;
						$totalRumahTidakSehat = 0;
						$totalTPS = 0;
						$totalSPAL = 0;
						$totalJamban = 0;
						$totalP4k = 0;
						$totalPDAM = 0;
						$totalSumur = 0;
						$totalDLL = 0;
						$totalBeras = 0;
						$totalNonBeras = 0;
						$totalUp2k = 0;
						$totalPTP = 0;
						$totalIRT = 0;
						$totalKerjaBakti = 0;
						$no = 1;
						while ($dataDasaWisma = mysqli_fetch_array($queryDasaWisma)) {
							if ($tempDasaWisma != $dataDasaWisma['dasaWisma']) {
								$jumlahKRT = 0;
								$jumlahKK = 0;
								$jumlahAnggota = 0;
								$jumlahBalita = 0;
								$jumlahPus = 0;
								$jumlahWus = 0;
								$jumlahHamil = 0;
								$jumlahMenyusui = 0;
								$jumlahLansia = 0;
								$jumlahButa = 0;
								$jumlahBerkebutuhanKhusus = 0;
								$jumlahRumahSehat = 0;
								$jumlahRumahTidakSehat = 0;
								$jumlahTPS = 0;
								$jumlahSPAL = 0;
								$jumlahJamban = 0;
								$jumlahP4k = 0;
								$jumlahPDAM = 0;
								$jumlahSumur = 0;
								$jumlahDLL = 0;
								$jumlahBeras = 0;
								$jumlahNonBeras = 0;
								$jumlahUp2k = 0;
								$jumlahPTP = 0;
								$jumlahIRT = 0;
								$jumlahKerjaBakti = 0;
						?>
								<tr>
									<td class="align-middle text-center"><?php echo $no ?></td>
									<td class="align-middle text-center"><?php echo $dataDasaWisma['dasaWisma'] ?></td>

									<?php
									$sqlJumlahKRT = "SELECT kepalaRumahTangga FROM tbl_data_warga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahKRT = mysqli_query($db, $sqlJumlahKRT);

									while ($dJumlahKRT = mysqli_fetch_array($queryJumlahKRT)) {
										$jumlahKRT++;
										$totalKRT++;
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahKRT ?></td>

									<?php
									$sqlJumlahKK = "SELECT jumlahKK FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahKK = mysqli_query($db, $sqlJumlahKK);

									while ($dJumlahKK = mysqli_fetch_array($queryJumlahKK)) {
										$jumlahKK += $dJumlahKK['jumlahKK'];
										$totalKK += $dJumlahKK['jumlahKK'];
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahKK ?></td>


									<?php
									$sqlJumlahAnggota = "SELECT jmlhAnggota FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahAnggota = mysqli_query($db, $sqlJumlahAnggota);

									while ($dJumlahAnggota = mysqli_fetch_array($queryJumlahAnggota)) {
										$jumlahAnggota += $dJumlahAnggota['jmlhAnggota'];
										$totalAnggota += $dJumlahAnggota['jmlhAnggota'];
									}
									?>
									<td colspan="2" class="align-middle text-center"><?php echo $jumlahAnggota ?></td>

									<?php
									$sqlJumlahBalita = "SELECT jumlahBalita FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahBalita = mysqli_query($db, $sqlJumlahBalita);

									while ($dJumlahBalita = mysqli_fetch_array($queryJumlahBalita)) {
										$jumlahBalita += $dJumlahBalita['jumlahBalita'];
										$totalBalita += $dJumlahBalita['jumlahBalita'];
									}
									?>
									<td colspan="2" class="align-middle text-center"><?php echo $jumlahBalita ?></td>

									<?php
									$sqlJumlahPus = "SELECT jumlahPus FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahPus = mysqli_query($db, $sqlJumlahPus);

									while ($dJumlahPus = mysqli_fetch_array($queryJumlahPus)) {
										$jumlahPus += $dJumlahPus['jumlahPus'];
										$totalPus += $dJumlahPus['jumlahPus'];
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahPus ?></td>

									<?php
									$sqlJumlahWus = "SELECT jumlahWus FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahWus = mysqli_query($db, $sqlJumlahWus);

									while ($dJumlahWus = mysqli_fetch_array($queryJumlahWus)) {
										$jumlahWus += $dJumlahWus['jumlahWus'];
										$totalWus += $dJumlahWus['jumlahWus'];
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahWus ?></td>

									<?php
									$sqlJumlahHamil = "SELECT jumlahHamil FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahHamil = mysqli_query($db, $sqlJumlahHamil);

									while ($dJumlahHamil = mysqli_fetch_array($queryJumlahHamil)) {
										$jumlahHamil += $dJumlahHamil['jumlahHamil'];
										$totalHamil += $dJumlahHamil['jumlahHamil'];
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahHamil ?></td>

									<?php
									$sqlJumlahMenyusui = "SELECT jumlahMenyusui FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahMenyusui = mysqli_query($db, $sqlJumlahMenyusui);

									while ($dJumlahMenyusui = mysqli_fetch_array($queryJumlahMenyusui)) {
										$jumlahMenyusui += $dJumlahMenyusui['jumlahMenyusui'];
										$totalMenyusui += $dJumlahMenyusui['jumlahMenyusui'];
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahMenyusui ?></td>

									<?php
									$sqlJumlahLansia = "SELECT jumlahLansia FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahLansia = mysqli_query($db, $sqlJumlahLansia);

									while ($dJumlahLansia = mysqli_fetch_array($queryJumlahLansia)) {
										$jumlahLansia += $dJumlahLansia['jumlahLansia'];
										$totalLansia += $dJumlahLansia['jumlahLansia'];
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahLansia ?></td>

									<?php
									$sqlJumlahButa = "SELECT jumlahButa FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJumlahButa = mysqli_query($db, $sqlJumlahButa);

									while ($dJumlahButa = mysqli_fetch_array($queryJumlahButa)) {
										$jumlahButa += $dJumlahButa['jumlahButa'];
										$totalButa += $dJumlahButa['jumlahButa'];
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahButa ?></td>

									<?php
									$sqlBerkebutuhan = "SELECT berkebutuhanKhusus FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryBerkebutuhan = mysqli_query($db, $sqlBerkebutuhan);

									while ($dBerkebutuhanKhusus = mysqli_fetch_array($queryBerkebutuhan)) {
										if ($dBerkebutuhanKhusus == 'Fisik' || $dBerkebutuhanKhusus == 'Non Fisik') {
											$jumlahBerkebutuhanKhusus++;
											$totalBerkebutuhanKhusus++;
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahBerkebutuhanKhusus ?></td>

									<?php
									$sqlKriteriaSehat = "SELECT kriteriaRumah FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND kriteriaRumah='Sehat' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryKriteriaSehat = mysqli_query($db, $sqlKriteriaSehat);

									while ($dKriteriaSehat = mysqli_fetch_array($queryKriteriaSehat)) {
										$jumlahRumahSehat++;
										$totalRumahSehat++;
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahRumahSehat ?></td>

									<?php
									$sqlKriteriaTidakSehat = "SELECT kriteriaRumah FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND kriteriaRumah='Tidak Sehat' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryKriteriaTidakSehat = mysqli_query($db, $sqlKriteriaTidakSehat);

									while ($dKriteriaTidakSehat = mysqli_fetch_array($queryKriteriaTidakSehat)) {
										$jumlahRumahTidakSehat++;
										$totalRumahTidakSehat++;
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahRumahTidakSehat ?></td>

									<?php
									$sqlTPS = "SELECT memilikiTPS FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryTPS = mysqli_query($db, $sqlTPS);

									while ($dTPS = mysqli_fetch_array($queryTPS)) {
										if ($dTPS['memilikiTPS'] == 'Ya') {
											$jumlahTPS++;
											$totalTPS++;
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahTPS ?></td>

									<?php
									$sqlSPAL = "SELECT saluranLimbah FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$querySPAL = mysqli_query($db, $sqlSPAL);

									while ($dSPAL = mysqli_fetch_array($querySPAL)) {
										if ($dSPAL['saluranLimbah'] == 'Ya') {
											$jumlahSPAL++;
											$totalSPAL++;
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahSPAL ?></td>

									<?php
									$sqlJamban = "SELECT jambanKeluarga FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryJamban = mysqli_query($db, $sqlJamban);

									while ($dJamban = mysqli_fetch_array($queryJamban)) {
										if ($dJamban['jambanKeluarga'] == 'Ya') {
											$jumlahJamban++;
											$totalJamban++;
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahJamban ?></td>

									<?php
									$sqlP4k = "SELECT stikerP4K FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryP4k = mysqli_query($db, $sqlP4k);

									while ($dP4k = mysqli_fetch_array($queryP4k)) {
										if ($dP4k['stikerP4K'] == 'Ya') {
											$jumlahP4k++;
											$totalP4k++;
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahP4k ?></td>

									<?php
									$sqlSumberAir = "SELECT sumberAir FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$querySumberAir = mysqli_query($db, $sqlSumberAir);

									while ($dSumberAir = mysqli_fetch_array($querySumberAir)) {
										if ($dSumberAir['sumberAir'] == 'PDAM') {
											$jumlahPDAM++;
											$totalPDAM++;
										} else if ($dSumberAir['sumberAir'] == 'Sumur') {
											$jumlahSumur++;
											$totalSumur++;
										} else {
											$jumlahDLL++;
											$totalDLL++;
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahPDAM ?></td>
									<td class="align-middle text-center"><?php echo $jumlahSumur ?></td>
									<td class="align-middle text-center"><?php echo $jumlahDLL ?></td>

									<?php
									$sqlMakananPokok = "SELECT makananPokok FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryMakananPokok = mysqli_query($db, $sqlMakananPokok);

									while ($dMakananPokok = mysqli_fetch_array($queryMakananPokok)) {
										if ($dMakananPokok['makananPokok'] == 'Beras') {
											$jumlahBeras++;
											$totalBeras++;
										} else if ($dMakananPokok['makananPokok'] == 'Non Beras') {
											$jumlahNonBeras++;
											$totalNonBeras++;
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahBeras ?></td>
									<td class="align-middle text-center"><?php echo $jumlahNonBeras ?></td>

									<?php
									$sqlUp2k = "SELECT up2k FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryUp2k = mysqli_query($db, $sqlUp2k);

									while ($dUp2k = mysqli_fetch_array($queryUp2k)) {
										if ($dUp2k['up2k'] == 'Ya') {
											$jumlahUp2k++;
											$totalUp2k++;
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahUp2k ?></td>

									<?php
									$sqlPTP = "SELECT idKeluarga FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryPTP = mysqli_query($db, $sqlPTP);

									while ($dPTP = mysqli_fetch_array($queryPTP)) {
										$sqlPTP1 = "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $dPTP['idKeluarga'] . "'";
										$queryPTP1 = mysqli_query($db, $sqlPTP1);
										if (mysqli_num_rows($queryPTP1) != 0) {
											while ($dPTP1 = mysqli_fetch_array($queryPTP1)) {
												$jumlahPTP++;
												$totalPTP++;
											}
										}
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahPTP ?></td>

									<?php
									$sqlIRT = "SELECT idKeluarga FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryIRT = mysqli_query($db, $sqlIRT);

									while ($dIRT = mysqli_fetch_array($queryIRT)) {
										$sqlIRT1 = "SELECT * FROM tbl_irt WHERE idKeluarga = '" . $dIRT['idKeluarga'] . "'";
										$queryIRT1 = mysqli_query($db, $sqlIRT1);
										if (mysqli_num_rows($queryIRT1) != 0) {
											while ($dIRT1 = mysqli_fetch_array($queryIRT1)) {
												$jumlahIRT++;
												$totalIRT++;
											}
										}
									}
									?>

									<td class="align-middle text-center"><?php echo $jumlahIRT ?></td>

									<?php
									$sqlKerjaBakti = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt' AND dasaWisma='" . $dataDasaWisma['dasaWisma'] . "'";
									$queryKerjaBakti = mysqli_query($db, $sqlKerjaBakti);

									while ($dKerjaBakti = mysqli_fetch_array($queryKerjaBakti)) {
										$jumlahKerjaBakti++;
										$totalKerjaBakti++;
									}
									?>
									<td class="align-middle text-center"><?php echo $jumlahKerjaBakti ?></td>

									<td class="align-middle text-center"></td>


								</tr>
						<?php
								$no++;
							}
						}
					}
					if ($no < 0) {
						?>
						<tr>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center">Jumlah</td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td colspan="2" class="align-middle text-center"></td>
							<td colspan="2" class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center"></td>
						</tr>
					<?php
					} else {
					?>
						<tr>
							<td class="align-middle text-center"></td>
							<td class="align-middle text-center">Jumlah</td>
							<td class="align-middle text-center"><?php echo $totalKRT ?></td>
							<td class="align-middle text-center"><?php echo $totalKK ?></td>
							<td colspan="2" class="align-middle text-center"><?php echo $totalAnggota ?></td>
							<td colspan="2" class="align-middle text-center"><?php echo $totalBalita ?></td>
							<td class="align-middle text-center"><?php echo $totalPus ?></td>
							<td class="align-middle text-center"><?php echo $totalWus ?></td>
							<td class="align-middle text-center"><?php echo $totalHamil ?></td>
							<td class="align-middle text-center"><?php echo $totalMenyusui ?></td>
							<td class="align-middle text-center"><?php echo $totalLansia ?></td>
							<td class="align-middle text-center"><?php echo $totalButa ?></td>
							<td class="align-middle text-center"><?php echo $totalBerkebutuhanKhusus ?></td>
							<td class="align-middle text-center"><?php echo $totalRumahSehat ?></td>
							<td class="align-middle text-center"><?php echo $totalRumahTidakSehat ?></td>
							<td class="align-middle text-center"><?php echo $totalTPS ?></td>
							<td class="align-middle text-center"><?php echo $totalSPAL ?></td>
							<td class="align-middle text-center"><?php echo $totalJamban ?></td>
							<td class="align-middle text-center"><?php echo $totalP4k ?></td>
							<td class="align-middle text-center"><?php echo $totalPDAM ?></td>
							<td class="align-middle text-center"><?php echo $totalSumur ?></td>
							<td class="align-middle text-center"><?php echo $totalDLL ?></td>
							<td class="align-middle text-center"><?php echo $totalBeras ?></td>
							<td class="align-middle text-center"><?php echo $totalNonBeras ?></td>
							<td class="align-middle text-center"><?php echo $totalUp2k ?></td>
							<td class="align-middle text-center"><?php echo $totalPTP ?></td>
							<td class="align-middle text-center"><?php echo $totalIRT ?></td>
							<td class="align-middle text-center"><?php echo $totalKerjaBakti ?></td>
							<td class="align-middle text-center"></td>
						</tr>
					<?php
					}
					?>

				</tbody>
			</table>
		</div>

		<div class="judul1">
			<p class="text-center">PEMANFAATAN TANAH PEKARANGAN / PTP HATINYA PKK</p>
		</div>

		<div class="tabel d-flex flex-sm-row flex-column mb-4">
			<div class="pemanfaatan">
				<p class="subjudul">PEMANFAATAN TANAH PEKARANGAN / PTP HATINYA PKK</p>
				<P class="subjudul1">Kelompok PKK RT : <?php if ($rt > 9) { ?>0<?php echo $rt;
																			} else { ?>00<?php echo $rt;
																			} ?></P>
				<P class="subjudul1">Kelompok PKK RW : <?php if ($rw > 9) { ?>0<?php echo $rw;
																			} else { ?>00<?php echo $rw;
																			} ?></P>
				<P class="subjudul1">Tahun : <?php echo date('Y') ?></P>

				<div class="table-responsive mb-4">
					<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
						<thead class="table-warning">
							<tr>
								<th>NO</th>
								<th>NAMA DASA WISMA</th>
								<th>JMH KRT</th>
								<th>PETERNAKAN</th>
								<th>PERIKANAN</th>
								<th>WARUNG HIDUP</th>
								<th>TOGA</th>
								<th>LUMBUNG HIDUP</th>
								<th>TANAMAN KERAS</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$sql = "SELECT * FROM tbl_data_keluarga WHERE rt='$rt' AND rw='$rw'";
							$query = mysqli_query($db, $sql);
							$no = 1;
							$totalPeternakan = 0;
							$totalPerikanan = 0;
							$totalWarung = 0;
							$totalToga = 0;
							$totalLumbung = 0;
							$totalTanaman = 0;
							$totalKRT = 0;
							while ($d = mysqli_fetch_array($query)) {
								?>
								<tr>
									<td><?php echo $no ?></td>
									<td><?php echo $d['dasaWisma'] ?></td>

									<?php
									$sqlJmlhKRT = "SELECT * FROM tbl_ptp WHERE dasaWisma = '" . $d['dasaWisma'] . "' AND keterangan = 'Peternakan'";
									$periksaJmlhKRT = mysqli_num_rows(mysqli_query($db, $sqlJmlhKRT));
									$jumlahKRT = $periksaJmlhKRT;
									?><td><?php echo $jumlahKRT ?></td><?php
									$totalKRT += $jumlahKRT;
									?>

									<?php
									$sqlPeternakan = "SELECT * FROM tbl_ptp WHERE dasaWisma = '" . $d['dasaWisma'] . "' AND keterangan = 'Peternakan'";
									$periksaPeternakan = mysqli_num_rows(mysqli_query($db, $sqlPeternakan));
									?><td><?php echo $periksaPeternakan ?></td><?php
									$totalPeternakan =+ $periksaPeternakan;
									?>

									<?php
									$sqlPerikanan = "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $d['idKeluarga'] . "' AND keterangan = 'Perikanan'";
									$periksaPerikanan = mysqli_num_rows(mysqli_query($db, $sqlPerikanan));
									?><td><?php echo $periksaPerikanan ?></td><?php
									$totalPerikanan =+ $periksaPerikanan;
									?>

									<?php
									$sqlWarung = "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $d['idKeluarga'] . "' AND keterangan = 'Warung Hidup'";
									$periksaWarung = mysqli_num_rows(mysqli_query($db, $sqlWarung));
									?><td><?php echo $periksaWarung ?></td><?php
									$totalWarung =+ $periksaWarung;
									?>

									<?php
									$sqlToga = "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $d['idKeluarga'] . "' AND keterangan = 'Toga'";
									$periksaToga = mysqli_num_rows(mysqli_query($db, $sqlToga));
									?><td><?php echo $periksaToga ?></td><?php
									$totalToga =+ $periksaToga;
									?>

									<?php
									$sqlLumbung = "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $d['idKeluarga'] . "' AND keterangan = 'Lumbung Hidup'";
									$periksaLumbung = mysqli_num_rows(mysqli_query($db, $sqlLumbung));
									?><td><?php echo $periksaLumbung ?></td><?php
									$totalLumbung =+ $periksaLumbung;
									?>

									<?php
									$sqlTanaman = "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $d['idKeluarga'] . "' AND keterangan = 'Tanaman Keras'";
									$periksaTanaman = mysqli_num_rows(mysqli_query($db, $sqlTanaman));
									?><td><?php echo $periksaTanaman ?></td><?php
									$totalTanaman =+ $periksaTanaman;
									?>
								</tr>
								<?php
								$no++;
							}
							?>
							<tr>
								<td></td>
								<td>Jumlah</td>
								<td><?php echo $totalKRT ?></td>
								<td><?php echo $totalPeternakan ?></td>
								<td><?php echo $totalPerikanan ?></td>
								<td><?php echo $totalWarung ?></td>
								<td><?php echo $totalToga ?></td>
								<td><?php echo $totalLumbung ?></td>
								<td><?php echo $totalTanaman ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="industri">
				<p class="subjudul">INDUSTRI RUMAH TANGGA</p>
				<P class="subjudul2">.</P>
				<P class="subjudul2">.</P>
				<P class="subjudul2">.</P>
				<div class="table-responsive mb-4">
					<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered tabel2">
						<thead class="table-warning">
							<tr>
								<th>PANGAN</th>
								<th>SANDANG</th>
								<th>JASA</th>
								<th>LAIN-LAIN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$sql2 = "SELECT * FROM tbl_data_keluarga WHERE rt='$rt' AND rw='$rw'";
							$query2 = mysqli_query($db, $sql2);
							while ($d2 = mysqli_fetch_array($query2)) {
								?>
								<tr>
									<?php
									$sqlPangan = "SELECT * FROM tbl_irt WHERE keterangan='Pangan' AND dasaWisma = '" . $d2['dasaWisma'] . "'";
									$queryPangan = mysqli_query($db, $sqlPangan);
									$jumlahPangan = mysqli_num_rows($queryPangan);
									?><td><?php echo $jumlahPangan ?></td><?php
									?>

									<?php
									$sqlSandang = "SELECT * FROM tbl_irt WHERE keterangan='Sandang' AND dasaWisma = '" . $d2['dasaWisma'] . "'";
									$querySandang = mysqli_query($db, $sqlSandang);
									$jumlahSandang = mysqli_num_rows($querySandang);
									?><td><?php echo $jumlahSandang ?></td><?php
									?>

									<?php
									$sqlJasa = "SELECT * FROM tbl_irt WHERE keterangan='Jasa' AND dasaWisma = '" . $d2['dasaWisma'] . "'";
									$queryJasa = mysqli_query($db, $sqlJasa);
									$jumlahJasa = mysqli_num_rows($queryJasa);
									?><td><?php echo $jumlahJasa ?></td><?php
									?>

									<?php
									$sqlLain = "SELECT * FROM tbl_irt WHERE keterangan='Lain-lain' AND dasaWisma = '" . $d2['dasaWisma'] . "'";
									$queryLain = mysqli_query($db, $sqlLain);
									$jumlahLain = mysqli_num_rows($queryLain);
									?><td><?php echo $jumlahLain ?></td><?php
									?>

								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
	window.print();
</script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

</html>