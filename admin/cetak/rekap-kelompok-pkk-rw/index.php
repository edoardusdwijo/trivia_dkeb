<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../../login-admin");
	exit;
}
$id = $_SESSION['id'];
$rw = $_GET['rw'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/rekapitulasikelompokpkkrwcetak-admin.css" media="screen">
	<link rel="stylesheet" href="../../../css/rekapitulasikelompokpkkrwview-admin.css" media="print">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../assets/image/logo.jpeg">
	<title>Cetak Rekapitulasi Kelompok PKK RW-Admin</title>
</head>

<body>
	<!-- start konten -->
	<div class="content">
		<div class="judul">
			<p class="text-center">REKAPITULASI<br>CATATAN DATA DAN DATA WARGA<br>KELOMPOK PKK RT</p>
		</div>

		<div class="detail mb-4">
			<!-- <p>RT <span>:</span></p>
			<p>RW <span>:</span></p>
			<p>DESA/KELURAHAN <span>:</span></p>
			<p>TAHUN <span>:</span></p> -->
			<table border="0" width="40%">
				<tr>
					<td width="40%">RW</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php if ($rw > 9) { ?>0<?php echo $rw;
											} else { ?>00<?php echo $rw;
														} ?></td>
				</tr>
				<tr>
					<td>DESA/KELURAHAN</td>
					<td width="10%" class="titikdua">:</td>
					<?php
					$desa = mysqli_fetch_array(mysqli_query($db, "SELECT desa FROM tbl_data_keluarga WHERE rw='$rw'"));
					?>
					<td><?php echo $desa['desa'] ?></td>
				</tr>
				<tr>
					<td>TAHUN</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo date('Y') ?></td>
				</tr>
			</table>
		</div>

		<div class="tabel table-responsive mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
				<thead class="table-warning atas">
					<tr>
						<th rowspan="3">NO</th>
						<th rowspan="3">NO RT</th>
						<th rowspan="3">JMH DAWIS</th>
						<th rowspan="3">JMH KRT</th>
						<th rowspan="3">JMH KK</th>
						<th colspan="11">JUMLAH ANGGOTA KELUARGA</th>
						<th colspan="6">KRITERIA RUMAH</th>
						<th colspan="3">SUMBER AIR KELUARGA</th>
						<th colspan="2">MAKANAN POKOK</th>
						<th colspan="4">WARGA MENGIKUTI KEGIATAN</th>
						<th rowspan="3">KET</th>
					</tr>
					<tr>
						<th colspan="2">TOTAL</th>
						<th colspan="2">BALITA</th>
						<th rowspan="2">PUS</th>
						<th rowspan="2">WUS</th>
						<th rowspan="2">IBU HAMIL</th>
						<th rowspan="2">IBU MENYUSUI</th>
						<th rowspan="2">LANSIA</th>
						<th rowspan="2">3 BUTA</th>
						<th rowspan="2">BERKEBUTUHAN KHUSUS</th>
						<th rowspan="2">SEHAT</th>
						<th rowspan="2">KURANG SEHAT</th>
						<th rowspan="2">MEMILIKI TEMP. PEMB. SAMPAH</th>
						<th rowspan="2">MEMILIKI SPAL</th>
						<th rowspan="2">MEMILIKI JAMBAN KELUARAGA</th>
						<th rowspan="2">MEMILIKI STIKER P4K</th>
						<th rowspan="2">PDAM</th>
						<th rowspan="2">SUMUR</th>
						<th rowspan="2">DLL</th>
						<th rowspan="2">BERAS</th>
						<th rowspan="2">NON BERAS</th>
						<th rowspan="2">UP2KPKK</th>
						<th rowspan="2">PEMANFAATAN TANAH PEKARANGAN</th>
						<th rowspan="2">INDUSTRI RUMAH TANGGA</th>
						<th rowspan="2">KESEHATAN LINGKUNGAN</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>2</td>
						<td>3</td>
						<td>4</td>
						<td>5</td>
						<td colspan="2">6</td>
						<td colspan="2">7</td>
						<td>10</td>
						<td>11</td>
						<td>12</td>
						<td>13</td>
						<td>14</td>
						<td>15</td>
						<td>16</td>
						<td>17</td>
						<td>18</td>
						<td>19</td>
						<td>20</td>
						<td>21</td>
						<td>22</td>
						<td>23</td>
						<td>24</td>
						<td>25</td>
						<td>26</td>
						<td>27</td>
						<td>28</td>
						<td>29</td>
						<td>30</td>
						<td>31</td>
						<td>32</td>
					</tr>
					<?php
					$sqlRT = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw'";
					$queryRT = mysqli_query($db, $sqlRT);
					$temp = "";
					$no = 1;
					$totalDawis = 0;
					$totalKepalaRT = 0;
					$totalKK = 0;
					$totalAnggota = 0;
					$totalBalita = 0;
					$totalPus = 0;
					$totalWus = 0;
					$totalHamil = 0;
					$totalMenyusui = 0;
					$totalLansia = 0;
					$totalButa = 0;
					$totalBerkebutuhan = 0;
					$totalSehat = 0;
					$totalKurangSehat = 0;
					$totalTPS = 0;
					$totalSaluran = 0;
					$totalJamban = 0;
					$totalStiker = 0;
					$totalSumur = 0;
					$totalPDAM = 0;
					$totalDll = 0;
					$totalBeras = 0;
					$totalNonBeras = 0;
					$totalUp2k = 0;
					$totalPTP = 0;
					$totalIRT = 0;
					$totalKesehatan = 0;
					while ($dataRT = mysqli_fetch_array($queryRT)) {
						if ($temp != $dataRT['rt']) {
					?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $dataRT['rt'];
									$rt = $dataRT['rt'] ?></td>

								<?php
								$sqlDawis = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryDawis = mysqli_query($db, $sqlDawis);
								$tempDawis = "";
								$jumlahDawis = 0;
								while ($dataDawis = mysqli_fetch_array($queryDawis)) {
									if ($tempDawis != $dataDawis['dasaWisma']) {
										$jumlahDawis++;
										$totalDawis++;
									}
								}
								?>
								<td><?php echo $jumlahDawis ?></td>

								<?php
								$sqlKepalaRT = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryKepalaRT = mysqli_query($db, $sqlKepalaRT);
								$tempKepalaRT = "";
								$jumlahKepalaRT = 0;
								while ($dataKepalaRT = mysqli_fetch_array($queryKepalaRT)) {
									if ($tempKepalaRT != $dataKepalaRT['namaKepalaK']) {
										$jumlahKepalaRT++;
										$totalKepalaRT++;
									}
								}
								?>
								<td><?php echo $jumlahKepalaRT ?></td>

								<?php
								$sqlKK = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryKK = mysqli_query($db, $sqlKK);
								$jumlahKK = 0;
								while ($dataKK = mysqli_fetch_array($queryKK)) {
									$jumlahKK += $dataKK['jumlahKK'];
									$totalKK += $dataKK['jumlahKK'];
								}
								?>
								<td><?php echo $jumlahKK ?></td>

								<?php
								$sqltotalAnggota = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryTotalAnggota = mysqli_query($db, $sqltotalAnggota);
								$jumlahAnggota = 0;
								while ($dataAnggota = mysqli_fetch_array($queryTotalAnggota)) {
									$jumlahAnggota += $dataAnggota['jmlhAnggota'];
									$totalAnggota += $dataAnggota['jmlhAnggota'];
								}
								?>
								<td colspan="2"><?php echo $jumlahAnggota ?></td>

								<?php
								$sqltotalBalita = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryTotalBalita = mysqli_query($db, $sqltotalBalita);
								$jumlahBalita = 0;
								while ($dataBalita = mysqli_fetch_array($queryTotalBalita)) {
									$jumlahBalita += $dataBalita['jumlahBalita'];
									$totalBalita += $dataBalita['jumlahBalita'];
								}
								?>
								<td colspan="2"><?php echo $jumlahBalita ?></td>

								<?php
								$sqlJumlahPus = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahPus = mysqli_query($db, $sqlJumlahPus);
								$jumlahPus = 0;
								while ($dataJumlahPus = mysqli_fetch_array($queryJumlahPus)) {
									$jumlahPus += $dataJumlahPus['jumlahPus'];
									$totalPus += $dataJumlahPus['jumlahPus'];
								}
								?>
								<td><?php echo $jumlahPus ?></td>

								<?php
								$sqlJumlahWus = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahWus = mysqli_query($db, $sqlJumlahWus);
								$jumlahWus = 0;
								while ($dataWus = mysqli_fetch_array($queryJumlahWus)) {
									$jumlahWus += $dataWus['jumlahWus'];
									$totalWus += $dataWus['jumlahWus'];
								}
								?>
								<td><?php echo $jumlahWus ?></td>

								<?php
								$sqlJumlahHamil = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahHamil = mysqli_query($db, $sqlJumlahHamil);
								$jumlahHamil = 0;
								while ($dataHamil = mysqli_fetch_array($queryJumlahHamil)) {
									$jumlahHamil += $dataHamil['jumlahHamil'];
									$totalHamil += $dataHamil['jumlahHamil'];
								}
								?>
								<td><?php echo $jumlahHamil ?></td>

								<?php
								$sqlJumlahMenyusui = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahMenyusui = mysqli_query($db, $sqlJumlahMenyusui);
								$jumlahMenyusui = 0;
								while ($dataMenyusui = mysqli_fetch_array($queryJumlahMenyusui)) {
									$jumlahMenyusui += $dataMenyusui['jumlahMenyusui'];
									$totalMenyusui += $dataMenyusui['jumlahMenyusui'];
								}
								?>
								<td><?php echo $jumlahMenyusui ?></td>

								<?php
								$sqlJumlahLansia = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahLansia = mysqli_query($db, $sqlJumlahLansia);
								$jumlahLansia = 0;
								while ($dataLansia = mysqli_fetch_array($queryJumlahLansia)) {
									$jumlahLansia += $dataLansia['jumlahLansia'];
									$totalLansia += $dataLansia['jumlahLansia'];
								}
								?>
								<td><?php echo $jumlahLansia ?></td>

								<?php
								$sqlJumlahButa = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahButa = mysqli_query($db, $sqlJumlahButa);
								$jumlahButa = 0;
								while ($dataButa = mysqli_fetch_array($queryJumlahButa)) {
									$jumlahButa += $dataButa['jumlahButa'];
									$totalButa += $dataButa['jumlahButa'];
								}
								?>
								<td><?php echo $jumlahButa ?></td>

								<?php
								$sqlJumlahBerkebutuhan = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryJumlahBerkebutuhan = mysqli_query($db, $sqlJumlahBerkebutuhan);
								$jumlahBerkebutuhan = 0;
								while ($dataBerkebutuhan = mysqli_fetch_array($queryJumlahBerkebutuhan)) {
									if ($dataBerkebutuhan['berkebutuhanKhusus'] == 'Fisik' || $dataBerkebutuhan['berkebutuhanKhusus'] == 'Non Fisik') {
										$jumlahBerkebutuhan++;
										$totalBerkebutuhan++;
									}
								}
								?>
								<td><?php echo $jumlahBerkebutuhan ?></td>

								<?php
								$sqlKriteriaRumah = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryKriteriaRumah = mysqli_query($db, $sqlKriteriaRumah);
								$jumlahSehat = 0;
								$jumlahKurangSehat = 0;
								$jumlahTPS = 0;
								$jumlahSaluran = 0;
								$jumlahJamban = 0;
								$jumlahStiker = 0;
								while ($dataKriteriaRumah = mysqli_fetch_array($queryKriteriaRumah)) {
									if ($dataKriteriaRumah['kriteriaRumah'] == 'Sehat') {
										$jumlahSehat++;
										$totalSehat++;
									} else if ($dataKriteriaRumah['kriteriaRumah'] == 'Kurang Sehat') {
										$jumlahKurangSehat++;
										$totalKurangSehat++;
									}

									if ($dataKriteriaRumah['memilikiTPS'] == 'Ya') {
										$jumlahTPS++;
										$totalTPS++;
									}

									if ($dataKriteriaRumah['saluranLimbah'] == 'Ya') {
										$jumlahSaluran++;
										$totalSaluran++;
									}

									if ($dataKriteriaRumah['jambanKeluarga'] == 'Ya') {
										$jumlahJamban++;
										$totalJamban++;
									}

									if ($dataKriteriaRumah['stikerP4K'] == 'Ya') {
										$jumlahStiker++;
										$totalStiker++;
									}
								}
								?>
								<td><?php echo $jumlahSehat ?></td>
								<td><?php echo $jumlahKurangSehat ?></td>
								<td><?php echo $jumlahTPS ?></td>
								<td><?php echo $jumlahSaluran ?></td>
								<td><?php echo $jumlahJamban ?></td>
								<td><?php echo $jumlahStiker ?></td>

								<?php
								$sqlSumberAir = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$querySumberAir = mysqli_query($db, $sqlSumberAir);
								$jumlahPDAM = 0;
								$jumlahSumur = 0;
								$jumlahDll = 0;
								while ($dataSumberAir = mysqli_fetch_array($querySumberAir)) {
									if ($dataSumberAir['sumberAir'] == 'PDAM') {
										$jumlahPDAM++;
										$totalPDAM++;
									} else if ($dataSumberAir['sumberAir'] == 'Sumur') {
										$jumlahSumur++;
										$totalSumur++;
									} else if ($dataSumberAir['sumberAir'] == 'Dll') {
										$jumlahDll++;
										$totalDll++;
									}
								}
								?>
								<td><?php echo $jumlahPDAM ?></td>
								<td><?php echo $jumlahSumur ?></td>
								<td><?php echo $jumlahDll ?></td>

								<?php
								$sqlMakananPokok = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryMakananPokok = mysqli_query($db, $sqlMakananPokok);
								$jumlahBeras = 0;
								$jumlahNonBeras = 0;
								while ($dataMakananPokok = mysqli_fetch_array($queryMakananPokok)) {
									if ($dataMakananPokok['makananPokok'] == 'Beras') {
										$jumlahBeras++;
										$totalBeras++;
									} else {
										$jumlahNonBeras++;
										$totalNonBeras++;
									}
								}
								?>
								<td><?php echo $jumlahBeras ?></td>
								<td><?php echo $jumlahNonBeras ?></td>

								<?php
								$sqlUp2k = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryUp2k = mysqli_query($db, $sqlUp2k);
								$jumlahUp2k = 0;
								while ($dataUp2k = mysqli_fetch_array($queryUp2k)) {
									if ($dataUp2k['up2k'] == 'Ya') {
										$jumlahUp2k++;
										$totalUp2k++;
									}
								}
								?>
								<td><?php echo $jumlahUp2k ?></td>

								<?php
								$sqlPTP = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryPTP = mysqli_query($db, $sqlPTP);
								$jumlahPTP = 0;
								while ($dataPTP = mysqli_fetch_array($queryPTP)) {
									$querydiPTP = mysqli_query($db, "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $dataPTP['idKeluarga'] . "'");
									$jumlahPTP += mysqli_num_rows($querydiPTP);
									$totalPTP += mysqli_num_rows($querydiPTP);
								}
								?>
								<td><?php echo $jumlahPTP ?></td>

								<?php
								$sqlIRT = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryIRT = mysqli_query($db, $sqlIRT);
								$jumlahIRT = 0;
								while ($dataIRT = mysqli_fetch_array($queryIRT)) {
									$querydiIRT = mysqli_query($db, "SELECT * FROM tbl_irt WHERE idKeluarga = '" . $dataIRT['idKeluarga'] . "'");
									$jumlahIRT += mysqli_num_rows($querydiIRT);
									$totalIRT += mysqli_num_rows($querydiIRT);
								}
								?>
								<td><?php echo $jumlahIRT ?></td>

								<?php
								$sqlKesehatan = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw' AND rt='$rt'";
								$queryKesehatan = mysqli_query($db, $sqlKesehatan);
								$jumlahKesehatan = 0;
								$jumlahKesehatan += mysqli_num_rows($queryKesehatan);
								$totalKesehatan += mysqli_num_rows($queryKesehatan);
								?>
								<td><?php echo $jumlahKesehatan ?></td>
								<td></td>
							</tr>
					<?php
							$no++;
						}
					}
					?>
					<tr>
						<td></td>
						<td>JUMLAH</td>
						<td><?php echo $totalDawis ?></td>
						<td><?php echo $totalKepalaRT ?></td>
						<td><?php echo $totalKK ?></td>
						<td colspan="2"><?php echo $totalAnggota ?></td>
						<td colspan="2"><?php echo $totalBalita ?></td>
						<td><?php echo $totalPus ?></td>
						<td><?php echo $totalWus ?></td>
						<td><?php echo $totalHamil ?></td>
						<td><?php echo $totalMenyusui ?></td>
						<td><?php echo $totalLansia ?></td>
						<td><?php echo $totalButa ?></td>
						<td><?php echo $totalBerkebutuhan ?></td>
						<td><?php echo $totalSehat ?></td>
						<td><?php echo $totalKurangSehat ?></td>
						<td><?php echo $totalTPS ?></td>
						<td><?php echo $totalSaluran ?></td>
						<td><?php echo $totalJamban ?></td>
						<td><?php echo $totalStiker ?></td>
						<td><?php echo $totalPDAM ?></td>
						<td><?php echo $totalSumur ?></td>
						<td><?php echo $totalDll ?></td>
						<td><?php echo $totalBeras ?></td>
						<td><?php echo $totalNonBeras ?></td>
						<td><?php echo $totalUp2k ?></td>
						<td><?php echo $totalPTP ?></td>
						<td><?php echo $totalIRT ?></td>
						<td><?php echo $totalKesehatan ?></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="judul1">
			<p class="text-center">PEMANFAATAN TANAH PEKARANGAN / PTP HATINYA PKK</p>
		</div>

		<div class="tabel d-flex flex-sm-row flex-column mb-4">
			<div class="pemanfaatan">
				<p class="subjudul">PEMANFAATAN TANAH PEKARANGAN / PTP HATINYA PKK</p>
				<P class="subjudul1">Kelompok PKK RW : <?php if ($rw > 9) { ?>0<?php echo $rw;
																			} else { ?>00<?php echo $rw;
																						} ?></P>
				<?php
				$desa = mysqli_fetch_array(mysqli_query($db, "SELECT desa FROM tbl_data_keluarga WHERE rw='$rw'"));
				?>
				<P class="subjudul1">Kelompok PKK Desa/Kel : <?php echo $desa['desa'] ?></P>
				<P class="subjudul1">Tahun : <?php echo date('Y') ?></P>

				<div class="table-responsive mb-4">
					<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
						<thead class="table-warning bawah">
							<tr>
								<th>NO</th>
								<th>NO KRT</th>
								<th>JUMLAH DAWIS</th>
								<th>JUMLAH KRT</th>
								<th>PETERNAKAN</th>
								<th>PERIKANAN</th>
								<th>WARUNG HIDUP</th>
								<th>TOGA</th>
								<th>LUMBUNG HIDUP</th>
								<th>TANAMAN KERAS</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$sqlCariRw = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw'";
							$queryCariRw = mysqli_query($db, $sqlCariRw);
							$totalPeternakan = 0;
							$totalPerikanan = 0;
							$totalWarung = 0;
							$totalToga = 0;
							$totalLumbung = 0;
							$totalTanaman = 0;
							$totalDawis = 0;
							$totalKRT = 0;
							$no = 1;
							$tempRT = "";
							while($rt = mysqli_fetch_array($queryCariRw)){
								$jumlahPeternakan = 0;
								$jumlahPerikanan = 0;
								$jumlahWarung = 0;
								$jumlahToga = 0;
								$jumlahLumbung = 0;
								$jumlahTanaman = 0;
										if($tempRT != $rt['rt']){
											?><tr><?php
											$tempRT = $rt['rt'];
											$RT = $rt['rt'];
											?><td><?php echo $no ?></td><?php
											?><td><?php if($rt['rt']<10){?>00<?php echo $rt['rt'];}else{?>0<?php echo $rt['rt'];} ?></td><?php

											$sqlCariJumlahDawis = "SELECT * FROM tbl_data_keluarga WHERE rw=$rw AND rt=$RT";
											$queryCariJumlahDawis = mysqli_query($db,$sqlCariJumlahDawis);
											$tempDawis = "";
											$jumlahDawis = 0;
											while($dataDawis = mysqli_fetch_array($queryCariJumlahDawis)){
												if($tempDawis != $dataDawis['dasaWisma']){
													$jumlahDawis++;
													$totalDawis++;
													$dawis = $dataDawis['dasaWisma'];
													$sqlJumlah = "SELECT * FROM tbl_ptp WHERE dasaWisma = '$dawis'";
													$queryJumlah = mysqli_query($db,$sqlJumlah);

													while($dataPTP = mysqli_fetch_array($queryJumlah)){
														if($dataPTP['keterangan'] == "Peternakan"){
															$jumlahPeternakan++;
															$totalPeternakan++;
														}else if($dataPTP['keterangan'] == "Perikanan"){
															$jumlahPerikanan++;
															$totalPerikanan++;
														}else if($dataPTP['keterangan'] == "Warung Hidup"){
															$jumlahWarung++;
															$totalWarung++;
														}else if($dataPTP['keterangan'] == "Toga"){
															$jumlahToga++;
															$totalToga++;
														}else if($dataPTP['keterangan'] == "Lumbung Hidup"){
															$jumlahLumbung++;
															$totalLumbung++;
														}else if($dataPTP['keterangan'] == "Tanaman Keras"){
															$jumlahTanaman++;
															$totalTanaman++;
														}
													}
												}
											}
											?><td><?php echo $jumlahDawis ?></td><?php

											$sqlCariJumlahKRT = "SELECT * FROM tbl_data_keluarga WHERE rw=$rw AND rt=$RT";
											$queryCariJumlahKRT = mysqli_query($db,$sqlCariJumlahKRT);
											$tempKRT = "";
											$jumlahKRT = 0;
											while($dataKRT = mysqli_fetch_array($queryCariJumlahKRT)){
												if($tempKRT != $dataKRT['namaKepalaK']){
													$jumlahKRT++;
													$totalKRT++;
												}
											}
											?><td><?php echo $jumlahKRT ?></td><?php
											?>
											<td><?php echo $jumlahPeternakan ?></td>
											<td><?php echo $jumlahPerikanan ?></td>
											<td><?php echo $jumlahWarung ?></td>
											<td><?php echo $jumlahToga ?></td>
											<td><?php echo $jumlahLumbung ?></td>
											<td><?php echo $jumlahTanaman ?></td>
											</tr>
											<?php
											$no++;
										}
							}
							?>
							<tr>
								<td></td>
								<td>Total</td>
								<td><?php echo $totalDawis ?></td>
								<td><?php echo $totalKRT ?></td>
								<td><?php echo $totalPeternakan ?></td>
								<td><?php echo $totalPerikanan ?></td>
								<td><?php echo $totalWarung ?></td>
								<td><?php echo $totalToga ?></td>
								<td><?php echo $totalLumbung ?></td>
								<td><?php echo $totalTanaman ?></td>
							</tr>
							<?php
							?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="industri">
				<p class="subjudul">INDUSTRI RUMAH TANGGA</p>
				<P class="subjudul2">.</P>
				<P class="subjudul2">.</P>
				<P class="subjudul2">.</P>
				<div class="table-responsive mb-4">
					<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
						<thead class="table-warning bawah">
							<tr>
								<th>PANGAN</th>
								<th>SANDANG</th>
								<th>JASA</th>
								<th>LAIN-LAIN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$sqlCariIdBuatIRT = "SELECT * FROM tbl_data_keluarga WHERE rw='$rw'";
							$queryCariIdBuatIRT = mysqli_query($db, $sqlCariIdBuatIRT);
							$tempDawis2 = "";
							while ($cariDataIRT = mysqli_fetch_array($queryCariIdBuatIRT)) {
								$dawis2 = $cariDataIRT['dasaWisma'];
								if($tempDawis2 != $dawis2){
									$jumlahPangan = 0;
									$jumlahSandang = 0;
									$jumlahJasa = 0;
									$jumlahLain = 0;
									$sqlDataIRT = "SELECT * FROM tbl_irt WHERE dasaWisma = '$dawis2'";
									$queryDataIRT = mysqli_query($db, $sqlDataIRT);
									while($ulangDawis = mysqli_fetch_array($queryDataIRT)){
										if($ulangDawis['keterangan'] == 'Pangan'){
											$jumlahPangan++;
										}else if($ulangDawis['keterangan'] == 'Sandang'){
											$jumlahSandang++;
										}else if($ulangDawis['keterangan'] == 'Jasa'){
											$jumlahJasa++;
										}else if($ulangDawis['keterangan'] == 'Lain-lain'){
											$jumlahLain++;
										}
									}
									?>
									<tr>
										<td><?php echo $jumlahPangan ?></td>
										<td><?php echo $jumlahSandang ?></td>
										<td><?php echo $jumlahJasa ?></td>
										<td><?php echo $jumlahLain ?></td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
	window.print();
</script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

</html>