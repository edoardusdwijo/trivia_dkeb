<?php
include('../../../config.php');
session_start();

if ($_SESSION['tipeUser'] != 'admin') {
	header("location:../../../login-admin");
	exit;
}
$id = $_SESSION['id'];
$dasaWisma = $_GET['dasaWisma'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/rekapitulasikelompokdesawismacetak-admin.css" media="screen">
	<link rel="stylesheet" href="../../../css/rekapitulasikelompokdesawismaview-admin.css" media="print">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../../assets/image/logo.jpeg">
	<title>Cetak Rekapitulasi Kelompok Dasa Wisma-Admin</title>
</head>

<body>
	<!-- start konten -->
	<div class="content">
		<div class="judul">
			<p class="text-center">REKAPITULASI<br>CATATAN DATA DAN DATA WARGA<br>KELOMPOK DASA WISMA</p>
		</div>

		<div class="detail mb-4">
			<!-- <p>RT <span>:</span></p>
			<p>RW <span>:</span></p>
			<p>DESA/KELURAHAN <span>:</span></p>
			<p>TAHUN <span>:</span></p> -->
			<table border="0" width="40%">
				<tr>
					<td width="40%">Dasa Wisma</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo $dasaWisma ?></td>
				</tr>
				<?php
				$sqlJudul = "SELECT * FROM tbl_data_keluarga WHERE `dasaWisma`='$dasaWisma'";
				$queryJudul = mysqli_query($db, $sqlJudul);
				$dataJudul = mysqli_fetch_array($queryJudul);
				?>
				<tr>
					<td>RT</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo $dataJudul['rt'] ?></td>
				</tr>
				<tr>
					<td>RW</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo $dataJudul['rw'] ?></td>
				</tr>
				<tr>
					<td>DESA/KELURAHAN</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo $dataJudul['desa'] ?></td>
				</tr>
				<tr>
					<td>TAHUN</td>
					<td width="10%" class="titikdua">:</td>
					<td><?php echo date('Y') ?></td>
				</tr>
			</table>
		</div>

		<div class="tabel table-responsive mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th rowspan="3" class="align-middle text-center">NO</th>
						<th rowspan="3" class="align-middle text-center">NAMA KEPALA RUMAH TANGGA</th>
						<th rowspan="3" class="align-middle text-center">JMH KK</th>
						<th colspan="11" class="align-middle text-center">JUMLAH ANGGOTA KELUARGA</th>
						<th colspan="5" class="align-middle text-center">KRITERIA RUMAH</th>
						<th rowspan="2" class="align-middle text-center">SUMBER AIR KELUARGA</th>
						<th rowspan="2" class="align-middle text-center">MAKANAN POKOK</th>
						<th colspan="4" class="align-middle text-center">WARGA MENGIKUTI KEGIATAN</th>
						<th rowspan="3" class="align-middle text-center">KET</th>
					</tr>
					<tr>
						<th colspan="2" class="align-middle text-center">TOTAL</th>
						<th colspan="2" class="align-middle text-center">BALITA</th>
						<th rowspan="2" class="align-middle text-center">PUS</th>
						<th rowspan="2" class="align-middle text-center">WUS</th>
						<th rowspan="2" class="align-middle text-center">IBU HAMIL</th>
						<th rowspan="2" class="align-middle text-center">IBU MENYUSUI</th>
						<th rowspan="2" class="align-middle text-center">LANSIA</th>
						<th rowspan="2" class="align-middle text-center">BUTA</th>
						<th rowspan="2" class="align-middle text-center">BERKEBUTUHAN KHUSUS</th>
						<th rowspan="2" class="align-middle text-center">KRITERIA</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI TMP. PEMB. SAMPAH</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI SPAL</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI JAMBAN KELUARGA</th>
						<th rowspan="2" class="align-middle text-center">MEMILIKI STIKER P4K</th>
						<th rowspan="2" class="align-middle text-center">UP2KPKK</th>
						<th rowspan="2" class="align-middle text-center">PEMANFAATAN TANAH PEKARANGAN</th>
						<th rowspan="2" class="align-middle text-center">INDUSTRI RUMAH TANGGA</th>
						<th rowspan="2" class="align-middle text-center">KERJA BAKTI</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-middle text-center">1</td>
						<td class="align-middle text-center">2</td>
						<td class="align-middle text-center">3</td>
						<td colspan="2" class="align-middle text-center">4</td>
						<td colspan="2" class="align-middle text-center">5</td>
						<td class="align-middle text-center">6</td>
						<td class="align-middle text-center">7</td>
						<td class="align-middle text-center">8</td>
						<td class="align-middle text-center">9</td>
						<td class="align-middle text-center">10</td>
						<td class="align-middle text-center">11</td>
						<td class="align-middle text-center">12</td>
						<td class="align-middle text-center">13</td>
						<td class="align-middle text-center">14</td>
						<td class="align-middle text-center">15</td>
						<td class="align-middle text-center">16</td>
						<td class="align-middle text-center">17</td>
						<td class="align-middle text-center">18</td>
						<td class="align-middle text-center">19</td>
						<td class="align-middle text-center">20</td>
						<td class="align-middle text-center">21</td>
						<td class="align-middle text-center">22</td>
						<td class="align-middle text-center">23</td>
						<td class="align-middle text-center">24</td>
					</tr>
					<?php
					$sqlID = "SELECT idUser FROM tbl_data_warga WHERE dasaWisma = '$dasaWisma'";
					$queryID = mysqli_query($db, $sqlID);
					$no = 1;

					while ($data = mysqli_fetch_array($queryID)) {
						$sqlCariData1 = "SELECT * FROM tbl_data_warga WHERE idUser = '" . $data['idUser'] . "'";
						$queryCariData1 = mysqli_query($db, $sqlCariData1);
						$hasilData1 = mysqli_fetch_array($queryCariData1);
						$sqlCariData2 = "SELECT * FROM tbl_data_keluarga WHERE idUser = '" . $data['idUser'] . "'";
						$queryCariData2 = mysqli_query($db, $sqlCariData2);
						$hasilData2 = mysqli_fetch_array($queryCariData2);
						$sqlCariData3 = "SELECT * FROM tbl_ptp WHERE idKeluarga = '" . $data['idUser'] . "'";
						$queryCariData3 = mysqli_query($db, $sqlCariData3);
						$hasilData3 = mysqli_fetch_array($queryCariData3);
						$sqlCariData4 = "SELECT * FROM tbl_irt WHERE idKeluarga = '" . $data['idUser'] . "'";
						$queryCariData4 = mysqli_query($db, $sqlCariData4);
						$hasilData4 = mysqli_fetch_array($queryCariData4);

					?>
						<tr>
							<td class="align-middle text-center"><?php echo $no ?></td>
							<td class="align-middle text-center"><?php echo $hasilData1['kepalaRumahTangga'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['jumlahKK'] ?></td>
							<td colspan="2" class="align-middle text-center"><?php echo $hasilData2['jmlhAnggota'] ?></td>
							<td colspan="2" class="align-middle text-center"><?php echo $hasilData2['jumlahBalita'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['jumlahPus'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['jumlahWus'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['jumlahHamil'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['jumlahMenyusui'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['jumlahLansia'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['jumlahButa'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['berkebutuhanKhusus'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['kriteriaRumah'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['memilikiTPS'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['saluranLimbah'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['jambanKeluarga'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['stikerP4K'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['sumberAir'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['makananPokok'] ?></td>
							<td class="align-middle text-center"><?php echo $hasilData2['up2k'] ?></td>
							<?php if (mysqli_num_rows($queryCariData3) < 1) {
							?><td class="align-middle text-center">Tidak</td><?php
																		} else {
																			?><td class="align-middle text-center">Ya</td><?php
																														} ?>
							<?php if (mysqli_num_rows($queryCariData4) < 1) {
							?><td class="align-middle text-center">Tidak</td><?php
																		} else {
																			?><td class="align-middle text-center">Ya</td><?php
																														} ?>
							<td class="align-middle text-center">Ya</td>
							<td class="align-middle text-center"></td>
						</tr>
					<?php
					}
					?>
					<tr>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center">Jumlah</td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
						<td class="align-middle text-center"></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="judul1 mt-4">
			<p class="text-center">PEMANFAATAN TANAH PEKARANGAN / PTP HATINYA PKK</p>
		</div>

		<div class="tabel1 d-flex flex-sm-row flex-column mb-4">
			<div class="pemanfaatan">
				<p class="subjudul">PEMANFAATAN TANAH PEKARANGAN / PTP HATINYA PKK</p>
				<P class="subjudul1">KELOMPOK DAWIS : <?php echo $dasaWisma ?></P>

				<div class="table-responsive mb-4">
					<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
						<thead class="table-warning">
							<tr>
								<th>No</th>
								<th>NAMA KRT</th>
								<th>PETERNAKAN</th>
								<th>PERIKANAN</th>
								<th>WARUNG HIDUP</th>
								<th>TOGA</th>
								<th>LUMBUNG HIDUP</th>
								<th>TANAMAN KERAS</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$sqlPTP = "SELECT * FROM tbl_ptp WHERE dasaWisma = '$dasaWisma'";
							$queryPTP = mysqli_query($db, $sqlPTP);
							$no = 1;
							$nama = "";
							$jumlahPeternakan = 0;
							$jumlahPerikanan = 0;
							$jumlahWarung = 0;
							$jumlahToga = 0;
							$jumlahLumbung = 0;
							$jumlahTanaman = 0;
							while ($dataPTP = mysqli_fetch_array($queryPTP)) {
								if ($nama != $dataPTP['namaKRT']) {
							?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo $dataPTP['namaKRT'] ?></td>
										<?php
										$sqlPeternakan = "SELECT * FROM tbl_ptp WHERE namaKRT = '" . $dataPTP['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Peternakan'";
										$periksaPeternakan = mysqli_num_rows(mysqli_query($db, $sqlPeternakan));
										if ($periksaPeternakan < 1) {
										?> <td>Tidak</td> <?php
														} else {
															?> <td>Ya</td> <?php
																			$jumlahPeternakan++;
																		}
																			?>
										<?php
										$sqlPerikanan = "SELECT * FROM tbl_ptp WHERE namaKRT = '" . $dataPTP['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Perikanan'";
										$periksaPerikanan = mysqli_num_rows(mysqli_query($db, $sqlPerikanan));
										if ($periksaPerikanan < 1) {
										?> <td>Tidak</td> <?php
														} else {
															?> <td>Ya</td> <?php
																			$jumlahPerikanan++;
																		}
																			?>
										<?php
										$sqlWarung = "SELECT * FROM tbl_ptp WHERE namaKRT = '" . $dataPTP['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Warung Hidup'";
										$periksaWarung = mysqli_num_rows(mysqli_query($db, $sqlWarung));
										if ($periksaWarung < 1) {
										?> <td>Tidak</td> <?php
														} else {
															?> <td>Ya</td> <?php
																			$jumlahWarung++;
																		}
																			?>
										<?php
										$sqlToga = "SELECT * FROM tbl_ptp WHERE namaKRT = '" . $dataPTP['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Toga'";
										$periksaToga = mysqli_num_rows(mysqli_query($db, $sqlToga));
										if ($periksaToga < 1) {
										?> <td>Tidak</td> <?php
														} else {
															?> <td>Ya</td> <?php
																			$jumlahToga++;
																		}
																			?>
										<?php
										$sqlLumbung = "SELECT * FROM tbl_ptp WHERE namaKRT = '" . $dataPTP['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Lumbung Hidup'";
										$periksaLumbung = mysqli_num_rows(mysqli_query($db, $sqlLumbung));
										if ($periksaLumbung < 1) {
										?> <td>Tidak</td> <?php
														} else {
															?> <td>Ya</td> <?php
																			$jumlahLumbung++;
																		}
																			?>
										<?php
										$sqlTanaman = "SELECT * FROM tbl_ptp WHERE namaKRT = '" . $dataPTP['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Tanaman Keras'";
										$periksaTanaman = mysqli_num_rows(mysqli_query($db, $sqlTanaman));
										if ($periksaTanaman < 1) {
										?> <td>Tidak</td> <?php
														} else {
															?> <td>Ya</td> <?php
																			$jumlahTanaman++;
																		}
																			?>
									</tr>
							<?php
								}
								$nama = $dataPTP['namaKRT'];
								$no++;
							}
							?>
							<tr>
								<td></td>
								<td>Jumlah</td>
								<td><?php echo $jumlahPeternakan ?></td>
								<td><?php echo $jumlahPerikanan ?></td>
								<td><?php echo $jumlahWarung ?></td>
								<td><?php echo $jumlahToga ?></td>
								<td><?php echo $jumlahLumbung ?></td>
								<td><?php echo $jumlahTanaman ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="industri">
				<p class="subjudul">INDUSTRI RUMAH TANGGA</p>
				<P class="subjudul2">.</P>
				<div class="table-responsive mb-4">
					<table class="table table-hover table-light rounded-3 overflow-hidden table-bordered">
						<thead class="table-warning">
							<tr>
								<th>PANGAN</th>
								<th>SANDANG</th>
								<th>JASA</th>
								<th>LAIN-LAIN</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$sqlIRT = "SELECT * FROM tbl_irt WHERE dasaWisma = '$dasaWisma'";
							$queryIRT = mysqli_query($db, $sqlIRT);
							$no = 1;
							$nama = "";
							while ($dataIRT = mysqli_fetch_array($queryIRT)) {
								?>
								<tr>
								<?php
								if ($nama != $dataIRT['namaKRT']) {
									$sqlPangan = "SELECT * FROM tbl_irt WHERE namaKRT = '" . $dataIRT['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Pangan'";
									$periksaPangan = mysqli_num_rows(mysqli_query($db, $sqlPangan));
									if ($periksaPangan < 1) {
							?> <td>Tidak</td> <?php
											} else {
												?> <td>Ya</td> <?php
															}
															$sqlSandang = "SELECT * FROM tbl_irt WHERE namaKRT = '" . $dataIRT['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Sandang'";
															$periksaSandang = mysqli_num_rows(mysqli_query($db, $sqlSandang));
															if ($periksaSandang < 1) {
																?> <td>Tidak</td> <?php
																								} else {
																									?> <td>Ya</td> <?php
																								}
																								$sqlJasa = "SELECT * FROM tbl_irt WHERE namaKRT = '" . $dataIRT['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Jasa'";
																								$periksaJasa = mysqli_num_rows(mysqli_query($db, $sqlJasa));
																								if ($periksaJasa < 1) {
																								?> <td>Tidak</td> <?php
																								} else {
																									?> <td>Ya</td> <?php
																								}
																								$sqlLain = "SELECT * FROM tbl_irt WHERE namaKRT = '" . $dataIRT['namaKRT'] . "' AND dasaWisma = '$dasaWisma' AND keterangan = 'Lain-lain'";
																								$periksaLain = mysqli_num_rows(mysqli_query($db, $sqlLain));
																								if ($periksaLain < 1) {
																								?> <td>Tidak</td> <?php
																								} else {
																									?> <td>Ya</td> <?php
																								}
																								?>
																									</tr>
																								<?php
																							}
																							$nama = $dataIRT['namaKRT'];
																						}
																								?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
	<!-- end konten -->
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
	window.print();
</script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

</html>