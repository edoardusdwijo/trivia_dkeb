<!DOCTYPE html>
<html lang="en">

<head>
	<link rel="preload" as="image" href="../../assets/image/pilih-user.jpg">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<!-- <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'> -->
	<link rel="stylesheet" href="../../css/lupa-password.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../../assets/image/logo.jpeg">
	<title>Forgot Password</title>
</head>

<body>
	<!-- star navbar -->
	<nav class="navbar navbar-expand-lg bg-light">
		<div class="container">
			<a class="navbar-brand" href="../../">Trivia Dkeb</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start body -->
	<div class="form_login">
		<div class="container">
			<h1>Password Recovery</h1>
			<p>Please till in the email you've used to create a Trivia DKEB account and we'll send you a reset link</p>
			<form>
				<div class="mb-4">
					<label for="exampleInputEmail1" class="form-label">EMAIL</label>
					<center>
						<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Email" name="email">
					</center>
				</div>
				<div class="tombol text-center mb-3">
					<button class="btn btn-primary" type="submit">Reset my password</button>
				</div>
				<div class="sign-up mb-3 text-center">
					<a href="../">Back to login</a>
				</div>
			</form>
		</div>
	</div>
	<!-- end body -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</html>