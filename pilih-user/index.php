<!DOCTYPE html>
<html lang="en">

<head>
	<link rel="preload" as="image" href="../assets/image/pilih-user.jpg">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../css/pilih-user.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<link rel="icon" href="../assets/image/logo.jpeg">
	<title>Pilih User</title>
</head>

<body>
	<!-- star navbar -->
	<nav class="navbar navbar-expand-lg bg-light">
		<div class="container">
			<a class="navbar-brand" href="../">Trivia Dkeb</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
		</div>
	</nav>
	<!-- end navbar -->

	<!-- start body -->
	<div class="container icon">
		<div class="row d-flex justify-align-center text-center">
			<div class="col-md-6 mb-4">
				<a href="../login-admin/" class="admin text-center">
					<img src="../assets/icon/icon-admin.png" alt="">
					<center>
						<p>ADMIN</p>
					</center>
				</a>
			</div>
			<div class="col-md-6 mb-4">
				<a href="../login-user/" class="user">
					<img src="../assets/icon/icon-user.png" alt="">
					<center>
						<P>USER</P>
					</center>
				</a>
			</div>
		</div>
	</div>
	<!-- end body -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</html>