-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Jan 2023 pada 18.21
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trivia_dkeb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_anggota_kel`
--

CREATE TABLE `tbl_anggota_kel` (
  `id` int(20) NOT NULL,
  `idKeluarga` int(50) NOT NULL,
  `idUser` int(20) NOT NULL,
  `noRegAnggota` varchar(50) NOT NULL,
  `namaAnggota` varchar(244) NOT NULL,
  `statusKeluargaAnggota` varchar(50) NOT NULL,
  `statusPerkawinanAnggota` varchar(50) NOT NULL,
  `jenisKelaminAnggota` varchar(50) NOT NULL,
  `tglLahirAnggota` date NOT NULL,
  `pendidikanAnggota` varchar(20) NOT NULL,
  `pekerjaanAnggota` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_catatan_keluarga`
--

CREATE TABLE `tbl_catatan_keluarga` (
  `idCatatanKeluarga` int(20) NOT NULL,
  `kepalaRumahTangga` varchar(255) NOT NULL,
  `dasaWisma` varchar(255) NOT NULL,
  `tahun` year(4) NOT NULL,
  `kriteriaRumah` varchar(100) NOT NULL,
  `jambanKeluarga` varchar(100) NOT NULL,
  `memilikiTPS` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_data_keluarga`
--

CREATE TABLE `tbl_data_keluarga` (
  `idKeluarga` int(20) NOT NULL,
  `idUser` int(20) NOT NULL,
  `dasaWisma` varchar(100) NOT NULL,
  `rt` int(20) NOT NULL,
  `rw` int(20) NOT NULL,
  `dusun` varchar(100) NOT NULL,
  `desa` varchar(100) NOT NULL,
  `kec` varchar(100) NOT NULL,
  `kab` varchar(100) NOT NULL,
  `prov` varchar(100) NOT NULL,
  `namaKepalaK` varchar(255) NOT NULL,
  `jmlhAnggota` int(20) NOT NULL,
  `jumlahKK` int(20) NOT NULL,
  `jumlahBalita` int(20) NOT NULL,
  `jumlahPus` int(20) NOT NULL,
  `jumlahWus` int(20) NOT NULL,
  `jumlahButa` int(20) NOT NULL,
  `jumlahHamil` int(20) NOT NULL,
  `jumlahMenyusui` int(20) NOT NULL,
  `jumlahLansia` int(20) NOT NULL,
  `berkebutuhanKhusus` varchar(30) NOT NULL,
  `makananPokok` varchar(30) NOT NULL,
  `jambanKeluarga` varchar(20) NOT NULL,
  `sumberAir` varchar(20) NOT NULL,
  `memilikiTPS` varchar(20) NOT NULL,
  `saluranLimbah` varchar(20) NOT NULL,
  `stikerP4K` varchar(20) NOT NULL,
  `kriteriaRumah` varchar(30) NOT NULL,
  `up2k` varchar(20) NOT NULL,
  `jenisUsaha` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_data_warga`
--

CREATE TABLE `tbl_data_warga` (
  `noRegist` varchar(50) NOT NULL,
  `idUser` varchar(50) NOT NULL,
  `dasaWisma` varchar(100) NOT NULL,
  `kepalaRumahTangga` varchar(100) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `rt` int(20) NOT NULL,
  `rw` int(20) NOT NULL,
  `jk` varchar(50) NOT NULL,
  `tempatLahir` varchar(50) NOT NULL,
  `tglLahir` date NOT NULL,
  `umur` int(5) NOT NULL,
  `statusKawin` varchar(50) NOT NULL,
  `statusKeluarga` varchar(50) NOT NULL,
  `agama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pendidikan` varchar(30) NOT NULL,
  `pekerjaan` varchar(30) NOT NULL,
  `akseptorKB` varchar(10) NOT NULL,
  `jenisAkseptor` varchar(50) NOT NULL,
  `aktifPosyandu` varchar(10) NOT NULL,
  `frekVolume` varchar(50) NOT NULL,
  `binaKeluargaBalita` varchar(10) NOT NULL,
  `memilikiTabungan` varchar(10) NOT NULL,
  `kelompokBelajar` varchar(100) NOT NULL,
  `mengikutiPaud` varchar(100) NOT NULL,
  `kegiatanKoperasi` varchar(100) NOT NULL,
  `jenisKoperasi` varchar(100) NOT NULL,
  `kebutuhanKhusus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_irt`
--

CREATE TABLE `tbl_irt` (
  `id` int(20) NOT NULL,
  `idKeluarga` int(50) NOT NULL,
  `dasaWisma` varchar(200) NOT NULL,
  `namaKRT` varchar(100) NOT NULL,
  `keterangan` varchar(244) NOT NULL,
  `komoditi` varchar(255) NOT NULL,
  `volume` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_list_catatan_keluarga`
--

CREATE TABLE `tbl_list_catatan_keluarga` (
  `id` int(20) NOT NULL,
  `idCatatanKeluarga` int(20) NOT NULL,
  `anggotaKeluarga` varchar(255) NOT NULL,
  `statusKawinAnggota` varchar(20) NOT NULL,
  `jenisKelaminAnggota` varchar(20) NOT NULL,
  `tempatLahirAnggota` varchar(100) NOT NULL,
  `tanggalLahirAnggota` date NOT NULL,
  `agamaAnggota` varchar(100) NOT NULL,
  `pendidikanAnggota` varchar(100) NOT NULL,
  `pekerjaanAnggota` varchar(100) NOT NULL,
  `berkebutuhanKhususAnggota` varchar(100) NOT NULL,
  `pengamalanPancasila` varchar(100) NOT NULL,
  `gotongRoyong` varchar(100) NOT NULL,
  `pendidikanKeterampilan` varchar(100) NOT NULL,
  `kehidupanBerkoperasi` varchar(100) NOT NULL DEFAULT '-',
  `pangan` varchar(100) NOT NULL,
  `sandang` varchar(100) NOT NULL,
  `kesehatan` varchar(100) NOT NULL,
  `perencanaanKesehatan` varchar(100) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ptp`
--

CREATE TABLE `tbl_ptp` (
  `id` int(50) NOT NULL,
  `idKeluarga` int(50) NOT NULL,
  `dasaWisma` varchar(200) NOT NULL,
  `namaKRT` varchar(100) NOT NULL,
  `keterangan` varchar(244) NOT NULL,
  `komoditi` varchar(255) NOT NULL,
  `volume` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(50) NOT NULL,
  `nama` varchar(244) NOT NULL,
  `email` varchar(244) NOT NULL,
  `tipeUser` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `tglLahir` date NOT NULL,
  `statusAkun` varchar(50) NOT NULL DEFAULT 'non-aktif'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nama`, `email`, `tipeUser`, `password`, `tglLahir`, `statusAkun`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2001-03-10', 'aktif');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_anggota_kel`
--
ALTER TABLE `tbl_anggota_kel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_catatan_keluarga`
--
ALTER TABLE `tbl_catatan_keluarga`
  ADD PRIMARY KEY (`idCatatanKeluarga`);

--
-- Indeks untuk tabel `tbl_data_keluarga`
--
ALTER TABLE `tbl_data_keluarga`
  ADD PRIMARY KEY (`idKeluarga`);

--
-- Indeks untuk tabel `tbl_data_warga`
--
ALTER TABLE `tbl_data_warga`
  ADD PRIMARY KEY (`noRegist`);

--
-- Indeks untuk tabel `tbl_irt`
--
ALTER TABLE `tbl_irt`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_list_catatan_keluarga`
--
ALTER TABLE `tbl_list_catatan_keluarga`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_ptp`
--
ALTER TABLE `tbl_ptp`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_anggota_kel`
--
ALTER TABLE `tbl_anggota_kel`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_irt`
--
ALTER TABLE `tbl_irt`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_list_catatan_keluarga`
--
ALTER TABLE `tbl_list_catatan_keluarga`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_ptp`
--
ALTER TABLE `tbl_ptp`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
